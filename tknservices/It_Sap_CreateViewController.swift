//
//  It_Sap_CreateViewController.swift
//  tknservices
//
//  Created by lifetofree on 8/22/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Sap_CreateViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    var user_selected = UserRequest_detail()
    var emp_detail = employee_detail()
    var arr_emp_detail = [employee_detail]()
    
    var remote_detail = UserRequest_detail()
    var arr_remote_detail = [UserRequest_detail]()
    
    var doccode_detail = UserRequest_detail()
    var arr_doccode_detail = [UserRequest_detail]()
    
    var dataLocation: [String] = []
    var dataRemote: [String] = []
    var dataEmployee: [String] = []
    var dataEmployee_empidx: [String] = []
    
    var dataSystemClose = ["ด่วน","ไม่ด่วน","ปานกลาง"]
    
    var emp_name: String = ""
    var _emp_name: String = ""
    var emp_code: String = ""
    var emp_name_th: String = ""
    var rsec_idx: Int = 0
    var emp_idx_select: Int = 0
    var remote_idx: Int = 0
    var CheckRemote: Int = 0
    var remote_id: String = ""
    var remote_pass: String = ""
    var PIDX_Add: Int = 0
    
    var DocCode: String = ""
    var return_msg_: String = ""
    var return_code_: String = ""
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfCreateName: UITextField!
    @IBOutlet weak var tfTel: UITextField!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var tfUserLogon: UITextField!
    @IBOutlet weak var tfTcode: UITextField!
    @IBOutlet weak var tfpriolity: UITextField!
    
    @IBOutlet weak var btnAttachFile: UIButton!
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var tfComment: UITextField!
    
    var pvLocation = UIPickerView()
    var pvPriolity = UIPickerView()
    var pvEmployee = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        getMyProfile();
        getProfile_Defult();
        
        ddlLocation();
        ddlEmployee();
        
        // set picker view
        pvLocation.delegate = self
        pvLocation.dataSource = self
        pvLocation.tag = 1
        tfLocation.inputView = pvLocation
        
        pvPriolity.delegate = self
        pvPriolity.dataSource = self
        pvPriolity.tag = 2
        tfpriolity.text = dataSystemClose[0]
        tfpriolity.inputView = pvPriolity
        
        pvEmployee.delegate = self
        pvEmployee.dataSource = self
        pvEmployee.tag = 3
        tfCreateName.inputView = pvEmployee
        
        // create tool bar for date picker
        let toolBarTel = UIToolbar()
        toolBarTel.barStyle = .default
        toolBarTel.isTranslucent = true
        toolBarTel.sizeToFit()
        
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        let toolBarUserLogon = UIToolbar()
        toolBarUserLogon.barStyle = .default
        toolBarUserLogon.isTranslucent = true
        toolBarUserLogon.sizeToFit()
        
        let toolBarTcode = UIToolbar()
        toolBarTcode.barStyle = .default
        toolBarTcode.isTranslucent = true
        toolBarTcode.sizeToFit()
        
        let doneButtonTel = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerTel))
        let spaceButtonTel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonTel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerTel))
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        let doneButtonUserLogon = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerUserLogon))
        let spaceButtonUserLogon = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonUserLogon = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerUserLogon))
        
        let doneButtonTcode = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerTcode))
        let spaceButtonTcode = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonTcode = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerTcode))
        
        toolBarTel.setItems([cancelButtonTel,spaceButtonTel,doneButtonTel], animated: false)
        toolBarTel.isUserInteractionEnabled = true
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        toolBarUserLogon.setItems([cancelButtonUserLogon,spaceButtonUserLogon,doneButtonUserLogon], animated: false)
        toolBarUserLogon.isUserInteractionEnabled = true
        
        toolBarTcode.setItems([cancelButtonTcode,spaceButtonTcode, doneButtonTcode], animated: false)
        toolBarTcode.isUserInteractionEnabled = true
        
        tfTel.inputAccessoryView = toolBarTel
        tfComment.inputAccessoryView = toolBarComment
        tfUserLogon.inputAccessoryView = toolBarUserLogon
        tfTcode.inputAccessoryView = toolBarTcode
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // disable btn
        // check iOS version
        let os = ProcessInfo().operatingSystemVersion
        switch (os.majorVersion, os.minorVersion, os.patchVersion) {
        case (9, _, _):
            btnAttachFile.isEnabled = true
        case (10, _, _):
            btnAttachFile.isEnabled = true
        default:
            // this code will have already crashed on iOS 7, so >= iOS 10.0
            btnAttachFile.isEnabled = false
            btnAttachFile.setTitle("--- coming soon ---", for: .normal)
        }
    }
    
    @IBAction func buttonPopup(_ sender: UITextField) {
        //showInputDialog()
    }
    
    @IBAction func selectPhotoButtonTapped(_ sender: Any) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            myImageView.image = image
            //print(image)
        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickerTel() {
        tfTel.resignFirstResponder()
    }
    
    func CancelPickerTel() {
        tfTel.text = ""
        tfTel.resignFirstResponder()
    }
    
    func donePickerComment() {
        tfComment.resignFirstResponder()
    }
    
    func CancelPickerComment() {
        tfComment.text = ""
        tfComment.resignFirstResponder()
    }
    
    func donePickerUserLogon() {
        tfUserLogon.resignFirstResponder()
    }
    
    func CancelPickerUserLogon() {
        tfUserLogon.text = ""
        tfUserLogon.resignFirstResponder()
    }
    
    func donePickerTcode() {
        tfTcode.resignFirstResponder()
    }
    
    func CancelPickerTcode() {
        tfTcode.text = ""
        tfTcode.resignFirstResponder()
    }
    
    func getProfile_Defult() {
        
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        if let emp_code_temp = defaults.value(forKey: "emp_code") {
            emp_code = emp_code_temp as! String
        }
        if let emp_name_th_temp = defaults.value(forKey: "emp_name_th") {
            emp_name_th = emp_name_th_temp as! String
        }
        if let rsec_idx_temp = defaults.value(forKey: "rsec_idx") {
            rsec_idx = rsec_idx_temp as! Int
        }
        
    }
    
    // get my profile
    func getMyProfile() {
        
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        
        let tempUrl = String(urlGetMyProfile + sEmpIDX)
        let sendUrl = URL(string: tempUrl!)!
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    // get value
                    if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                            if(key == "emp_idx") {
                                self.emp_detail.emp_idx = Int(item.stringValue)!
                            }
                            if(key == "emp_code") {
                                self.emp_detail.emp_code = item.stringValue
                            }
                            if(key == "emp_name_th") {
                                self.emp_detail.emp_name_th = item.stringValue
                            }
                            if(key == "rsec_idx") {
                                self.emp_detail.rsec_idx = Int(item.stringValue)!
                            }
                            
                        }
                        self.arr_emp_detail += [self.emp_detail]
                    }
                    else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                            self.emp_detail.emp_idx = Int(item["emp_idx"].stringValue)!
                            self.emp_detail.emp_code = item["emp_code"].stringValue
                            self.emp_detail.emp_name_th = item["emp_name_th"].stringValue
                            self.emp_detail.rsec_idx = Int(item["rsec_idx"].stringValue)!
                            
                            self.arr_emp_detail += [self.emp_detail]
                        }
                    }
                    
                    // set value
                    //defaults.set(self.arr_emp_detail[0].emp_idx, forKey: "emp_idx")
                    //defaults.set(self.arr_emp_detail[0].emp_code, forKey: "emp_code")
                    
                    if let emp_name_th_temp = defaults.value(forKey: "emp_name_th") {
                        self._emp_name = emp_name_th_temp as! String
                    }
                    
                    //                        print(self._emp_name)
                    self.tfCreateName.text = self._emp_name
                    
                    //}
                }
        }
        
    }
    
    // Set Insert Repair
    func Insert_Repair() {
        
        if(tfpriolity.text! == "ด่วน"){
            self.PIDX_Add = 1
        }
        else if(tfpriolity.text! == "ไม่ด่วน"){
            self.PIDX_Add = 2
        }
        else{
            self.PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        "EmpIDX_add" : sEmpIDX,
                        "LocName" : tfLocation.text!, //Location
                        
                        //"OrgIDX" : iRsecIDX, ***
                        "SysIDX_add" : 2,
                        "ISO_User" : 2, //แจ้งซ่อม Sap
                        "PIDX_Add" : self.PIDX_Add, //ลำดับความสำคัญ
                        "TelETC" : tfTel.text!,
                        "EmailETC" : "",
                        "CostIDX" : 0,
                        "NCEmpIDX" : sEmpIDX, //EmpIDX ของผูที่ถูกแจ้งแทน
                        
                        "UserLogonName" : self.tfUserLogon.text!, //
                        "TransactionCode" : self.tfTcode.text!, //
                        
                        "CheckRemote" : 0,
                        "RemoteIDX" : 0,
                        "UserIDRemote" : "",
                        "PasswordRemote" : "",
                        
                        //complete
                        "AdminIDX" : 0,
                        "AdminDoingIDX" : 0,
                        "CommentAMDoing" : "",
                        "FileAMDoing" : 0,
                        "CCAIDX" : 0,
                        "StaIDX" : 1,
                        
                        "EmailIDX" : 1, //Sap
                        
                        "detailUser" : tfComment.text!,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlInsertItrepair + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
        
        //myImageUploadRequest();
    }
    
    // get employee
    func ddlEmployee() {
        
        tfCreateName.text = self.emp_name
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "RSecID" : rsec_idx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetChooseOther + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "EmpIDX") {
                                self.remote_detail.EmpIDX = Int(item.stringValue)!
                            }
                            if(key == "FullNameTH") {
                                self.remote_detail.FullNameTH = item.stringValue
                            }
                        }
                        self.arr_remote_detail += [self.remote_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.remote_detail.EmpIDX = Int(item["EmpIDX"].stringValue)!
                            self.remote_detail.FullNameTH = item["FullNameTH"].stringValue
                            
                            self.arr_remote_detail += [self.remote_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_remote_detail.count {
                        if(self.arr_remote_detail[i].FullNameTH != ""){
                            self.dataEmployee += [self.arr_remote_detail[i].FullNameTH]
                        }
                    }
                    
                }
        }
    }
    
    // get Location
    func ddlLocation() {
        
        let value =
            [
                "data_employee" : [
                    "employee_list" : [
                        "org_idx" : 1,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetPlace + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                            if(key == "r0idx") {
                                self.emp_detail.r0idx = Int(item.stringValue)!
                            }
                            if(key == "LocName") {
                                self.emp_detail.LocName = item.stringValue
                            }
                        }
                        self.arr_emp_detail += [self.emp_detail]
                    }
                    else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                            self.emp_detail.r0idx = Int(item["r0idx"].stringValue)!
                            self.emp_detail.LocName = item["LocName"].stringValue
                            
                            self.arr_emp_detail += [self.emp_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_emp_detail.count {
                        if(self.arr_emp_detail[i].LocName != ""){
                            self.dataLocation += [self.arr_emp_detail[i].LocName]
                        }
                    }
                }
                
        }
    }
    
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    
    //--- required --//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLocation.count
        }
        else if(pickerView.tag == 2) {
            return self.dataSystemClose.count
        }
        else {
            return self.dataEmployee.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLocation[row]
        }
        else if(pickerView.tag == 2) {
            return self.dataSystemClose[row]
        }
        else {
            return self.dataEmployee[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLocation.text = self.dataLocation[row]
        }
        else if(pickerView.tag == 2) {
            tfpriolity.text = self.dataSystemClose[row]
        }
        else{
            tfCreateName.text = self.dataEmployee[row]
        }
        
        self.view.endEditing(true)
    }
    
    func Check_Alert(){
        let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณากรอกข้อมูลให้ครบถ้วน", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        if(tfLocation.text! == "" || tfComment.text! == "" || tfUserLogon.text! == "" || tfTcode.text! == "" || tfpriolity.text! == ""){
            Check_Alert()
        }
        else{
            
            Insert_Repair();
            myImageUploadRequest();
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itSapTabBarController
            itSapTabBarController.selectedIndex = 0 // It_Sap_TabBarController
            
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 // It_Sap_TabBarController
    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 // It_Sap_ListViewController
    }
    //----- Navigation Bar Button -----//
    
    func myImageUploadRequest()  {
        
        print ("Start upload photo")
        // ================================
        // Upload the photo to the server
        // ================================
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "SysIDX_add" : 2, // Sap
                        "EmpIDX_add" : sEmpIDX, //tfCreateName.text!,
                    ]
                ]
        ]
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_DocCode + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "DocCode") {
                                self.doccode_detail.DocCode = item.stringValue
                            }
                            if(key == "URQIDX") {
                                self.doccode_detail.URQIDX = Int(item.stringValue)!
                            }
                        }
                        self.arr_doccode_detail += [self.doccode_detail]
                    }
                        
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.doccode_detail.DocCode = item["DocCode"].stringValue
                            self.doccode_detail.URQIDX = Int(item["URQIDX"].stringValue)!
                            self.arr_doccode_detail += [self.doccode_detail]
                        }
                    }
                    
                    self.return_code_ = self.arr_doccode_detail[0].DocCode
                    //return_msg = self.arr_doccode_detail[0].DocCode
                    
                    
                    let uploadURL = sUploadUrl + "/ITRepair/mobile_upload/uploads.php"
                    let myUrl = NSURL(string: uploadURL);
                    
                    let request = NSMutableURLRequest(url:myUrl! as URL);
                    request.httpMethod = "POST";
                    let param = [
                        "firstName"  : self.return_code_,
                        "lastName"    : "ITRepair",
                        "userId"    : "1",
                        ]
                    
                    print(param)
                    
                    let boundary = self.generateBoundaryString()
                    
                    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                    
                    
                    if(self.myImageView.image != nil){
                        
                        let imageData = UIImageJPEGRepresentation(self.myImageView.image!, 1)
                        
                        
                        if(imageData==nil)  { return; }
                        
                        
                        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary, Doccode: self.return_code_) as Data
                        
                        //myActivityIndicator.startAnimating();
                        
                        let task = URLSession.shared.dataTask(with: request as URLRequest) {
                            data, response, error in
                            
                            if error != nil {
                                print("error=\(String(describing: error))")
                                return
                            }
                            
                            // You can print out response object
                            print("******* response = \(String(describing: response))")
                            
                            // Print out reponse body
                            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                            print("****** response data = \(responseString!)")
                            
                            do {
                                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                                
                                print(json!)
                                
                                DispatchQueue.main.async(execute: {
                                    //self.myActivityIndicator.stopAnimating()
                                    self.myImageView.image = nil;
                                });
                                
                            }catch
                            {
                                print(error)
                            }
                            
                        }
                        
                        
                        task.resume()
                    }
                    
                    
                    
                }
        }
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, Doccode: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString_sap(string: "--\(boundary)\r\n")
                body.appendString_sap(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString_sap(string: "\(value)\r\n")
            }
        }
        
        let filename = Doccode + "0.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString_sap(string: "--\(boundary)\r\n")
        body.appendString_sap(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString_sap(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString_sap(string: "\r\n")
        
        body.appendString_sap(string: "--\(boundary)--\r\n")
        
        return body
        
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
}

extension NSMutableData {
    
    func appendString_sap(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
