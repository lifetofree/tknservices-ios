//
//  CenPerGeneralViewController02.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CenPerGeneralViewController02: UIViewController {
    
    @IBOutlet weak var lblOrgNameTh: UILabel!
    @IBOutlet weak var lblDeptNameTh: UILabel!
    @IBOutlet weak var lblSecNameTh: UILabel!
    @IBOutlet weak var lblPosNameTh: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_org_name_th = defaults.string(forKey: "org_name_th") {
            lblOrgNameTh.text = ud_org_name_th
        }
        if let ud_dept_name_th = defaults.string(forKey: "dept_name_th") {
            lblDeptNameTh.text = ud_dept_name_th
        }
        if let ud_sec_name_th = defaults.string(forKey: "sec_name_th") {
            lblSecNameTh.text = ud_sec_name_th
        }
        if let ud_pos_name_th = defaults.string(forKey: "pos_name_th") {
            lblPosNameTh.text = ud_pos_name_th
        }
    }
}

