//
//  RegisterViewController.swift
//  tknservices
//
//  Created by MaI on 7/11/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var tfuser: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        
        tfuser.delegate = self as? UITextFieldDelegate
        tfpassword.delegate = self as? UITextFieldDelegate

        // set placeholder text and color
        tfuser.attributedPlaceholder = NSAttributedString(string:"email", attributes:[NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.2)])
        tfpassword.attributedPlaceholder = NSAttributedString(string:"password", attributes:[NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.2)])

        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        
        
        // create tool bar for date picker
        let toolBaruser = UIToolbar()
        toolBaruser.barStyle = .default
        toolBaruser.isTranslucent = true
        toolBaruser.sizeToFit()
        
        let toolBarpassword = UIToolbar()
        toolBarpassword.barStyle = .default
        toolBarpassword.isTranslucent = true
        toolBarpassword.sizeToFit()
        
        
        let doneButtonuser = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickeruser))
        let spaceButtonuser = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonuser = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickeruser))
        
        let doneButtonpassword = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerpassword))
        let spaceButtonpassword = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonpassword = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerpassword))
        
        toolBaruser.setItems([cancelButtonuser,spaceButtonuser,doneButtonuser], animated: false)
        toolBaruser.isUserInteractionEnabled = true
        
        toolBarpassword.setItems([cancelButtonpassword,spaceButtonpassword, doneButtonpassword], animated: false)
        toolBarpassword.isUserInteractionEnabled = true

        tfuser.inputAccessoryView = toolBaruser
        tfpassword.inputAccessoryView = toolBarpassword
        
        
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickeruser()
    {
        tfuser.resignFirstResponder()
    }
    func CancelPickeruser()
    {
        tfuser.text = ""
        tfuser.resignFirstResponder()
    }
    
    func donePickerpassword()
    {
        tfpassword.resignFirstResponder()
    }
    func CancelPickerpassword()
    {
        tfpassword.text = ""
        tfpassword.resignFirstResponder()
    }
    
    
    
    // Set Insert
    func Insert() {
        
        let value =
            [
                "data_employee_member" : [
                    "BoxEmp_member" : [
                        "Member_EmpUser" : tfuser.text!,
                        "Member_EmpPassword" : tfpassword.text!,
                        "Member_EmpEmail" : tfuser.text!,
                        
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetCenMember + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                    let swiftyJsonVar_1 = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar_1["data_employee_member"]["ReturnMsg"].stringValue
                    
                    print(responseData.result.value!)
                }
        }
        
        
        
    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
        
        if(tfuser.text! != "" && tfpassword.text! != ""){
        
            let providedEmailAddress = tfuser.text
        
            let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
            if isEmailAddressValid
            {
            
                Insert()
            
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let registerViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
                appDelegate.window?.rootViewController = registerViewController
            
            } else {
                print("Email address is not valid")
                displayAlertMessage(messageToDisplay: "กรุณาตรวจสอบ Email ให้ถูกต้อง")
            }
            
        }
        else{
            displayAlertMessage(messageToDisplay: "กรุณากรอกข้อมูลให้ครบถ้วน")
        }
        
    }
    
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "แจ้งเตือน", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    

    
    //----- Navigation Bar Button -----//
    @IBAction func btnCancel(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginViewController
    }
    //----- Navigation Bar Button -----//
}

