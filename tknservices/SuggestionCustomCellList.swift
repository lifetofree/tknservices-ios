//
//  SuggestionCustomCellList.swift
//  TKNServices
//
//  Created by lifetofree on 6/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class SuggestionCustomCellList: UITableViewCell {
    
    @IBOutlet weak var lblReqNo: UILabel!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblDeptName: UILabel!
    @IBOutlet weak var lblReqDate: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(req_no: String, emp_name: String, dept_name: String, req_date: String, comment: String) {
        lblReqNo.text = req_no
        lblEmpName.text = emp_name
        lblDeptName.text = dept_name
        lblReqDate.text = req_date
        lblComment.text = comment
    }
    
}
