//
//  CentralOtherViewController.swift
//  tknservices
//
//  Created by lifetofree on 3/13/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CentralOtherViewController: UIViewController {
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblEmpPos: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            lblEmpName.text = ud_emp_name_th
        }
        if let ud_pos_name_th = defaults.string(forKey: "pos_name_th") {
            lblEmpPos.text = ud_pos_name_th
        }
    }
    // get user data
    
    //----- function for event -----//
    @IBAction func btnSuggestion(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let suggestionTabBarController = storyBoard.instantiateViewController(withIdentifier: "SuggestionTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = suggestionTabBarController
    }
    
    @IBAction func btnCheckVersion(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralOtherViewControllerVersion = storyBoard.instantiateViewController(withIdentifier: "CentralOtherViewControllerVersion") as! CentralOtherViewControllerVersion
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralOtherViewControllerVersion
    }
    
    // btnLogout
    @IBAction func btnLogout(_ sender: UIButton) {
        
        defaults.removeObject(forKey: "emp_idx")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = dataViewController
    }
    
    @IBAction func btnHoliday(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralOtherViewControllerHoliday = storyBoard.instantiateViewController(withIdentifier: "CentralOtherViewControllerHoliday") as! CentralOtherViewControllerHoliday
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralOtherViewControllerHoliday
    }
    
    @IBAction func btnPhone(_ sender: UIButton) {
        //if(sEmpIDX == "172" || sEmpIDX == "24047") {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let centralOtherViewControllerTelephone = storyBoard.instantiateViewController(withIdentifier: "CentralOtherViewControllerTelephone") as! CentralOtherViewControllerTelephone
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = centralOtherViewControllerTelephone
        //} else {
            //alertComingSoon(return_msg: "")
        //}
    }
    //----- function for event -----//
    
    // alertComingSoon
    func alertComingSoon(return_msg: String) {
        let comingSoonAlert = UIAlertController(title: "Coming soon", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "Coming soon"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        comingSoonAlert.addAction(okAction)
        
        self.present(comingSoonAlert, animated: true, completion: nil)
    }
}
