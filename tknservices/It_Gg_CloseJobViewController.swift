//
//  It_Gg_CloseJobViewController.swift
//  tknservices
//
//  Created by lifetofree on 7/25/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Gg_CloseJobViewController: UIViewController {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfComment: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        //custom navbar background
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "bg_title_it_google")!.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        tfComment.inputAccessoryView = toolBarComment

        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_Gg_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_Gg_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func donePickerComment()
    {
        tfComment.resignFirstResponder()
    }
    func CancelPickerComment()
    {
        tfComment.text = ""
        tfComment.resignFirstResponder()
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 4 && acidx == 3){
            staidx = 22
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_GMGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get urlCloseJob_Admin
    func urlCloseJob_Admin() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "CIT1IDX" : 0,
                        "CIT2IDX" : 0,
                        "CIT3IDX" : 0,
                        "CIT4IDX" : 0,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "CommentAMDoing" : tfComment.text!,
                        "CCAIDX" : 0,
                        "RecieveIDX" : 0,
                        "EmpIDX_add" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "StaIDX" : 1
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGM_CloseJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func Check_Alert(){
        let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณากรอกข้อมูลให้ครบถ้วน", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        if(tfComment.text! == ""){
            Check_Alert()
        }
        else{
            
            urlUpdate()
            urlCloseJob_Admin()
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itGgTabBarController
            itGgTabBarController.selectedIndex = 0
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0 //
    }
    //----- Navigation Bar Button -----//
}
