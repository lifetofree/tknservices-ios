//
//  CentralOtherViewControllerVersion.swift
//  TKNServices
//
//  Created by lifetofree on 6/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CentralOtherViewControllerVersion: UIViewController {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lblVersionInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set button
        btnUpdate.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        
        // get version
        lblVersionInfo.text = appVersion + "(" + appBuildNo + ")"
        getApplicationVersion()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- function for event -----//
    // get application update status
    func getApplicationVersion() {
        // set data and url
        let value =
            [
                "data_employee" : [
                    "version_mobile_list" : [
                        "VersionCode" : appVersion,
                        "VersionName" : appBuildNo
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetAppVersion + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    
                    // check return_code
                    if (return_code == 1) { // update version
                        //                        print(appLink)
                        self.btnUpdate.isHidden = false
                    } else { // lastest version
                        self.btnUpdate.isHidden = true
                    }
//                    print(responseData.result.value!)
                }
        }
    }
    
    @IBAction func btnUpdate(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string: appLink)!)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 3 // CentralOtherViewController
    }
    //----- function for event -----//
}
