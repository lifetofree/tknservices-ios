//
//  It_Chr_CustomCellList.swift
//  TKNServices
//
//  Created by lifetofree on 4/23/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class It_Chr_CustomCellList: UITableViewCell {
    
    @IBOutlet weak var lblChrNo: UILabel!
    @IBOutlet weak var lblChrSystem: UILabel!
    @IBOutlet weak var lblChrCreateDate: UILabel!
    @IBOutlet weak var lblChrEmpName: UILabel!
    @IBOutlet weak var lblChrStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(chr_no: String, chr_system: String, chr_create_date: String, chr_emp_name: String, chr_status: String) {
        lblChrNo.text = chr_no
        lblChrSystem.text = chr_system
        lblChrCreateDate.text = chr_create_date
        lblChrEmpName.text = chr_emp_name
        lblChrStatus.text = chr_status
    }
    
}
