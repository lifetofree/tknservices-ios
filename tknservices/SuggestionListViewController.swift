//
//  SuggestionListViewController.swift
//  TKNServices
//
//  Created by lifetofree on 6/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SuggestionListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var refreshControl = UIRefreshControl()
    
    var u0_doc = data_requestlist()
    var arr_u0_doc = [data_requestlist]()
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var tbvList: UITableView!
    let textCellIdentifier = "tvcSuggestionList"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set table view w/ transparent background
        self.tbvList.backgroundColor = UIColor.clear

        tbvList.delegate = self
        tbvList.dataSource = self
        
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(SuggestionListViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tbvList.addSubview(refreshControl)

        self.getAllList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_doc.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SuggestionCustomCellList = tbvList.dequeueReusableCell(withIdentifier: textCellIdentifier) as! SuggestionCustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _req_no = arr_u0_doc[row].RCode
        let _emp_name = arr_u0_doc[row].fullname
        let _dept_name = arr_u0_doc[row].deptname
        let _req_date = arr_u0_doc[row].CreateDate
        let _comment = arr_u0_doc[row].Comment
        
        // load item
        cell.loadItem(req_no: _req_no, emp_name: _emp_name, dept_name: _dept_name, req_date: _req_date, comment: _comment)
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    
        //        tableView.deselectRow(at: indexPath, animated: true)
        //
        //        let row = indexPath.row
        //        data_selected = arr_u0_doc[row]
//    }
    
    //--- connected to other view ---//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        //        let iIndexPath : IndexPath = self.tbvList.indexPathForSelectedRow! as IndexPath
        //
        //        let detailView = segue.destination as! It_Chr_ListViewControllerSelected
        
        //        data_selected = arr_u0_doc[iIndexPath.row]
        //        detailView.val_selected = data_selected
//    }
    
    // function for event
    // get all approve
    func getAllList() {
        self.arr_u0_doc = [data_requestlist]()
        
        // set data and url
        let value =
            [
                "DataSupportIT" : [
                    "request_list" : [
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetSuggestionList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = 0 //Int(swiftyJsonVar["DataSupportIT"]["return_code"].stringValue)!
                    
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["DataSupportIT"]["request_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["DataSupportIT"]["request_list"].dictionaryValue {
                                if(key == "RCode") {
                                    self.u0_doc.RCode = item.stringValue
                                }
                                if(key == "fullname") {
                                    self.u0_doc.fullname = item.stringValue
                                }
                                if(key == "deptname") {
                                    self.u0_doc.deptname = item.stringValue
                                }
                                if(key == "CreateDate") {
                                    self.u0_doc.CreateDate = item.stringValue
                                }
                                if(key == "Comment") {
                                    self.u0_doc.Comment = item.stringValue
                                }
                            }
                            self.arr_u0_doc += [self.u0_doc]
                        } else if swiftyJsonVar["DataSupportIT"]["request_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["DataSupportIT"]["request_list"].arrayValue {
                                self.u0_doc.RCode = item["RCode"].stringValue
                                self.u0_doc.fullname = item["fullname"].stringValue
                                self.u0_doc.deptname = item["deptname"].stringValue
                                self.u0_doc.CreateDate = item["CreateDate"].stringValue
                                self.u0_doc.Comment = item["Comment"].stringValue

                                self.arr_u0_doc += [self.u0_doc]
                            }
                        }
//                        sEmpIDX = String(self.arr_emp_detail[0].emp_idx)
//                        defaults.set(sEmpIDX, forKey: "emp_idx")
//                        defaults.set(self.arr_emp_detail[0].emp_code, forKey: "emp_code")
//                        defaults.set(self.arr_emp_detail[0].emp_name_th, forKey: "emp_name_th")
//                        defaults.set(self.arr_emp_detail[0].rsec_idx, forKey: "rsec_idx")
                        
//                        self.loginSuccess()
                    } else {
//                        self.loginFail()
                    }
//                    print(responseData.result.value!)
                    
                    // count data and reload
                    if (self.arr_u0_doc.count > 0) {
                        // set back to label view
                        self.tbvList.backgroundView = nil;
                        
                        self.tbvList.reloadData()
                    } else {
                        self.tbvList.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvList.bounds.size.width, height: self.tbvList.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray
                        
                        // set back to label view
                        self.tbvList.backgroundView = lblEmpty;
                        // no separator
                        self.tbvList.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
                
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    // function for event
    
    // button event
    @IBAction func btnGoToHome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 3 // CentralTabBarController
    }
    // button event
}
