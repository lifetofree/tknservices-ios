//
//  It_Bi_CloseJobUViewController.swift
//  TKNServices
//
//  Created by lifetofree on 7/10/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bi_CloseJobUViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tfStatus: UITextField!

    var dataSystem = ["สามารถใช้งานได้ตามปกติ","ยังพบอาการตามที่แจ้ง"]
    var pvSystem = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- PickerView Requried -----//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSystem.count
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSystem[row]
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        tfStatus.text = dataSystem[row]
        self.view.endEditing(true)
    }
    //----- PickerView Requried -----//
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        setDataDetail()
        goBack()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    //----- Event Button -----//
    
    //----- Reuse -----//
    func initView() {
        view_content.layer.cornerRadius = 10

        // set picker view
        pvSystem.delegate = self
        pvSystem.dataSource = self
        pvSystem.tag = 1
        tfStatus.text = dataSystem[0]
        tfStatus.inputView = pvSystem

        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
    }
    
    // setDataDetail
    func setDataDetail() {
        if(unidx == 2 && acidx == 1){
            if(tfStatus.text! == "สามารถใช้งานได้ตามปกติ"){
                staidx = 4
            }
            else if(tfStatus.text! == "ยังพบอาการตามที่แจ้ง"){
                staidx = 5
            }
            else{
                staidx = 0
            }
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "PIDX_Add" : 0,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_SapGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBiTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bi_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBiTabBarController
        itBiTabBarController.selectedIndex = 0 //
    }
    //----- Reuse -----//
}
