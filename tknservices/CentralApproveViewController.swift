//
//  CentralApproveViewController.swift
//  TKNServices
//
//  Created by lifetofree on 4/18/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CentralApproveViewController: UIViewController {
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblEmpPos: UILabel!
    
    var rsec_idx: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
        
        // check rsec_idx
        if let ud_rsec_idx = defaults.string(forKey: "rsec_idx") {
            rsec_idx = Int(ud_rsec_idx)!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            lblEmpName.text = ud_emp_name_th
        }
        if let ud_pos_name_th = defaults.string(forKey: "pos_name_th") {
            lblEmpPos.text = ud_pos_name_th
        }
    }
    // get user data
    
    // button event
    @IBAction func btnItAd(_ sender: UIButton) {
        alertComingSoon(return_msg: "")
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let itAdTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Ad_TabBarController") as! UITabBarController
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        appDelegate.window?.rootViewController = itAdTabBarController
    }
    
    @IBAction func btnItChr(_ sender: UIButton) {
//        alertComingSoon(return_msg: "")
        if(rsec_idx == 149 || rsec_idx == 175) { // CEO & SC
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itChrListViewController = storyBoard.instantiateViewController(withIdentifier: "It_Chr_ListViewController") as! It_Chr_ListViewController

            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            appDelegate.window?.rootViewController = itChrListViewController
        } else {
            alertError(return_msg: "คุณไม่มีสิทธิ์ในการเข้าใช้งานระบบนี้ค่ะ")
        }
    }
    // button event
    
    // alertComingSoon
    func alertComingSoon(return_msg: String) {
        let comingSoonAlert = UIAlertController(title: "Coming soon", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "Coming soon"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        comingSoonAlert.addAction(okAction)
        
        self.present(comingSoonAlert, animated: true, completion: nil)
    }
    
    // alertError
    func alertError(return_msg: String) {
        let comingSoonAlert = UIAlertController(title: "Error", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "Coming soon"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        comingSoonAlert.addAction(okAction)
        
        self.present(comingSoonAlert, animated: true, completion: nil)
    }
}
