//
//  It_Pos_ListViewControllerSelected.swift
//  tknservices
//
//  Created by lifetofree on 8/18/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class It_Pos_ListViewControllerSelected: UIViewController {
    
    @IBOutlet weak var view_segment: UIView!
    @IBOutlet weak var view_content: UIView!
    
    var pos_container: ContainerViewController!
    var user_selected = UserRequest_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_segment.layer.cornerRadius = 5
        view_content.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            pos_container.segueIdentifierReceivedFromParent("pos_detail")
        } else if sender.selectedSegmentIndex == 1 {
            pos_container.segueIdentifierReceivedFromParent("pos_comment")
        } else {
            pos_container.segueIdentifierReceivedFromParent("pos_administrator")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pos_container" {
            self.pos_container = segue.destination as! ContainerViewController
            setDetail()
        }
    }
    
    func setDetail() {
        
        DocCode = user_selected.DocCode
        CreateDateUser = user_selected.CreateDateUser + " " + user_selected.TimeCreateJob
        EmpName = user_selected.EmpName
        TelETC = user_selected.TelETC
        LocName = user_selected.LocName
        RemoteName = user_selected.RemoteName
        UserIDRemote = user_selected.UserIDRemote + "," + user_selected.PasswordRemote
        Comment = user_selected.detailUser
        
        URQIDX = user_selected.URQIDX
        unidx = user_selected.unidx
        acidx = user_selected.acidx
        RDeptIDX = user_selected.RDeptIDX
        SysIDX = user_selected.SysIDX
        EmpIDX_Create = user_selected.EmpIDX
        OrgIDX = user_selected.OrgIDX
    }
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
        itPosTabBarController.selectedIndex = 0 // It_Pos_ListViewController
    }
    //----- Navigation Bar Button -----//
}
