//
//  SuggestionCreateViewController.swift
//  TKNServices
//
//  Created by lifetofree on 6/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SuggestionCreateViewController: UIViewController {

    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var view_suggestion: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var tvSuggestion: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        view_suggestion.layer.cornerRadius = 2
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: 0xFF372F, borderColor: 0x7F0308, borderWidth: 1.0)
        // set button
        
        getMyProfile()
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
//        // move textfield up when keyboard appears
//        NotificationCenter.default.addObserver(self, selector: #selector(SuggestionCreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(SuggestionCreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            lblEmpName.text = ud_emp_name_th
        }
    }
    // get user data
    
    @IBAction func btnSave(_ sender: UIButton) {
        saveData()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    
    func saveData() {
        guard let suggestionText = tvSuggestion.text , !suggestionText.isEmpty else {
            alertEmptyValue()
            return
        }
        
        // set data and url
        let value =
            [
                "DataSupportIT" : [
                    "request_list" : [
                        "EmpIDX" : Int(sEmpIDX)!,
                        "Comment" : tvSuggestion.text!,
                        "Request_type" : 1
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSetSuggestionList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    self.goBack()
                }
        }
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let suggestionTabBarController = storyBoard.instantiateViewController(withIdentifier: "SuggestionTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = suggestionTabBarController
        suggestionTabBarController.selectedIndex = 0 // SuggestionListViewController
    }
    
    // alert when UITextField empty
    func alertEmptyValue() {
        let emptyAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: "กรุณากรอกข้อมูลด้วยค่ะ", preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // set focus
//            _tbName.becomeFirstResponder()
        }
        
        emptyAlert.addAction(okAction)
        
        self.present(emptyAlert, animated: true, completion: nil)
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
}
