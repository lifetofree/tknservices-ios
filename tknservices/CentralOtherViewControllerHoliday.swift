//
//  CentralOtherViewControllerHoliday.swift
//  TKNServices
//
//  Created by lifetofree on 6/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CentralOtherViewControllerHoliday: UIViewController {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var imgHoliday: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        showImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- function for event -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 3 // CentralOtherViewController
    }
    
    func showImage() {
        if let httpUrl = String(sUploadUrl + String("tkn_holiday.png")),
            let imgUrl = URL(string: httpUrl),
            let imgData = NSData(contentsOf: imgUrl) {
            // use image value
            let imgObj = UIImage(data: imgData as Data)
            imgHoliday.image = imgObj
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped))
            imgHoliday.isUserInteractionEnabled = true
            imgHoliday.addGestureRecognizer(tapGestureRecognizer)
            
            return
        }
    }
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        // tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        newImageView.addGestureRecognizer(tap)
        // pinch
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        newImageView.addGestureRecognizer(pinchRecognizer)
        self.view.addSubview(newImageView)
        //        // pan
        //        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        //        newImageView.addGestureRecognizer(panRecognizer)
    }
    
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func handlePinchGesture(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            if CGFloat(view.transform.a) > 5.0 {
                view.transform.a = 5.0 // this is x coordinate
                view.transform.d = 5.0 // this is x coordinate
            }
            if CGFloat(view.transform.d) < 1.0 {
                view.transform.a = 1.0 // this is x coordinate
                view.transform.d = 1.0 // this is x coordinate
            }
            sender.scale = 1
            
            // pan
            let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            if view.transform.a > 1.0 {
                view.addGestureRecognizer(panRecognizer)
                //                print("aaa")
            } else {
                view.removeGestureRecognizer(panRecognizer)
                //                print("bbb")
            }
            //            print(view.transform.a)
        }
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        // get translation
        let translation = sender.translation(in: view)
        sender.setTranslation(CGPoint.zero, in: view)
        //        print(translation)
        
        //create a new Label and give it the parameters of the old one
        let label = sender.view! as UIView
        label.center = CGPoint(x: label.center.x + translation.x, y: label.center.y + translation.y)
        label.isMultipleTouchEnabled = true
        label.isUserInteractionEnabled = true
        
        //        if sender.state == UIGestureRecognizerState.began {
        //            //add something you want to happen when the Label Panning has started
        //            print("started")
        //        }
        //
        //        if sender.state == UIGestureRecognizerState.ended {
        //            //add something you want to happen when the Label Panning has ended
        //            print("ended")
        //        }
        //
        //
        //        if sender.state == UIGestureRecognizerState.changed {
        //            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
        //            print("changed")
        //        }
        //        else {
        //            // or something when its not moving
        //            print("not moving")
        //        }
    }
    //----- function for event -----//
}
