//
//  Vac_CustomCellList.swift
//  tknservices
//
//  Created by lifetofree on 4/20/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class Vac_CustomCellList: UITableViewCell {
    
    @IBOutlet weak var lblVacType: UILabel!
    @IBOutlet weak var lblVacStart: UILabel!
    @IBOutlet weak var lblVacEnd: UILabel!
    @IBOutlet weak var lblVacStatus: UILabel!
    @IBOutlet weak var ivVacStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(vac_type: String, vac_start: String, vac_end: String, vac_status: String) {
        lblVacType.text = vac_type
        lblVacStart.text = vac_start
        lblVacEnd.text = vac_end
        lblVacStatus.text = vac_status
        
        switch vac_status {
        case "อนุมัติ":
            ivVacStatus.image = UIImage(named: "icon_status_completed")
        case "ไม่อนุมัติ(แก้ไขเอกสาร)":
            ivVacStatus.image = UIImage(named: "icon_status_warning")
        case "ดำเนินการ", "แก้ไข":
            ivVacStatus.image = UIImage(named: "icon_status_waiting")
        case "ไม่อนุมัติ(ปิดเอกสาร)", "ยกเลิก":
            ivVacStatus.image = UIImage(named: "icon_status_repair")
        default:
            ivVacStatus.image = UIImage(named: "icon_status_completed")
        }
    }
    
}
