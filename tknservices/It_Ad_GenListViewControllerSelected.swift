//
//  It_Ad_GenListViewControllerSelected.swift
//  TKNServices
//
//  Created by lifetofree on 4/18/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class It_Ad_GenListViewControllerSelected: UIViewController {
    
    @IBOutlet weak var view_content: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // button event
    @IBAction func btnBack(_ sender: UIButton) {
        goToList()
    }
    // button event
    
    // function for event
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itAdTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Ad_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itAdTabBarController
        itAdTabBarController.selectedIndex = 0 // It_Ad_GenListViewController
    }
    // function for event
}
