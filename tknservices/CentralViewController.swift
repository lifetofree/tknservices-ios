//
//  CentralViewController.swift
//  tknservices
//
//  Created by lifetofree on 4/3/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CentralViewController: UIViewController {
    
    var emp_detail = employee_detail()
    var arr_emp_detail = [employee_detail]()
    
    @IBOutlet weak var lbl_emp_name: UILabel!
    @IBOutlet weak var lbl_pos_name: UILabel!
    @IBOutlet weak var lbl_loc_name: UILabel!
    
    @IBOutlet weak var btn_app_update: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
        getApplicationVersion()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- MIS -----//
    @IBAction func btnItIt(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = viewController
    }
    
    @IBAction func btnItSap(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
    }
    
    @IBAction func btnItGoogle(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
    }
    
    @IBAction func btnItPos(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
    }
    
    @IBAction func btnItBi(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBiTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bi_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBiTabBarController
    }
    
    @IBAction func btnItBp(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpTabBarController
    }
    //----- MIS -----//
    
    //----- HR -----//
    @IBAction func btnHrVac(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vacTabBarController = storyBoard.instantiateViewController(withIdentifier: "Vac_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = vacTabBarController
    }
    
    @IBAction func btnHrTime(_ sender: UIButton) {
        alertComingSoon(return_msg: "")
    }
    //----- HR -----//
    
    //----- EN -----//
    @IBAction func btnEnRepair(_ sender: UIButton) {
        alertComingSoon(return_msg: "")
    }
    //----- EN -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIBarButtonItem) {
        /*let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
         
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         
         appDelegate.window?.rootViewController = loginController*/
        
        defaults.removeObject(forKey: "emp_idx")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = dataViewController
    }
    //----- Navigation Bar Button -----//
    
    //----- function for event -----//
    // get application update status
    func getApplicationVersion() {
        // set data and url
        let value =
            [
                "data_employee" : [
                    "version_mobile_list" : [
                        "VersionCode" : appVersion,
                        "VersionName" : appBuildNo
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetAppVersion + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    
                    // check return_code
                    if (return_code == 1) { // update version
//                        print(appLink)
                        self.btn_app_update.isHidden = false
                    } else { // lastest version
                        self.btn_app_update.isHidden = true
                    }
                    // print(responseData.result.value!)
                }
        }
    }
    
    @IBAction func btn_app_update_click(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string: appLink)!)
    }
    
    // alertError
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    // alertComingSoon
    func alertComingSoon(return_msg: String) {
        let comingSoonAlert = UIAlertController(title: "Coming soon", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "Coming soon"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        comingSoonAlert.addAction(okAction)
        
        self.present(comingSoonAlert, animated: true, completion: nil)
    }
    
    // get my profile
    func getMyProfile() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        //        print(sEmpIDX)
        
        let tempUrl = String(urlGetMyProfileMobile + sEmpIDX)
        let sendUrl = URL(string: tempUrl!)!
        // print(sendUrl)
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    
                    // check return_code
                    if (return_code == 0) {
                        // get value
                        if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                                if(key == "emp_idx") {
                                    self.emp_detail.emp_idx = Int(item.stringValue)!
                                }
                                if(key == "emp_code") {
                                    self.emp_detail.emp_code = item.stringValue
                                }
                                if(key == "emp_type_name") {
                                    self.emp_detail.emp_type_name = item.stringValue
                                }
                                if(key == "pos_name_th") {
                                    self.emp_detail.pos_name_th = item.stringValue
                                }
                                if(key == "sec_name_th") {
                                    self.emp_detail.sec_name_th = item.stringValue
                                }
                                if(key == "dept_name_th") {
                                    self.emp_detail.dept_name_th = item.stringValue
                                }
                                if(key == "org_name_th") {
                                    self.emp_detail.org_name_th = item.stringValue
                                }
                                if(key == "costcenter_no") {
                                    self.emp_detail.costcenter_no = item.stringValue
                                }
                                if(key == "sex_name_th") {
                                    self.emp_detail.sex_name_th = item.stringValue
                                }
                                if(key == "emp_nickname_th") {
                                    self.emp_detail.emp_nickname_th = item.stringValue
                                }
                                if(key == "emp_name_th") {
                                    self.emp_detail.emp_name_th = item.stringValue
                                }
                                if(key == "emp_phone_no") {
                                    self.emp_detail.emp_phone_no = item.stringValue
                                }
                                if(key == "emp_mobile_no") {
                                    self.emp_detail.emp_mobile_no = item.stringValue
                                }
                                if(key == "emp_email") {
                                    self.emp_detail.emp_email = item.stringValue
                                }
                                if(key == "emp_birthday") {
                                    self.emp_detail.emp_birthday = item.stringValue
                                }
                                if(key == "nat_name") {
                                    self.emp_detail.nat_name = item.stringValue
                                }
                                if(key == "race_name") {
                                    self.emp_detail.race_name = item.stringValue
                                }
                                if(key == "rel_name_th") {
                                    self.emp_detail.rel_name_th = item.stringValue
                                }
                                if(key == "emp_address") {
                                    self.emp_detail.emp_address = item.stringValue
                                }
                                if(key == "dist_name") {
                                    self.emp_detail.dist_name = item.stringValue
                                }
                                if(key == "amp_name") {
                                    self.emp_detail.amp_name = item.stringValue
                                }
                                if(key == "prov_name") {
                                    self.emp_detail.prov_name = item.stringValue
                                }
                                if(key == "post_code") {
                                    self.emp_detail.post_code = item.stringValue
                                }
                                if(key == "country_name") {
                                    self.emp_detail.country_name = item.stringValue
                                }
                                if(key == "emp_start_date") {
                                    self.emp_detail.emp_start_date = item.stringValue
                                }
                                if(key == "emp_probation_date") {
                                    self.emp_detail.emp_probation_date = item.stringValue
                                }
                                if(key == "emp_approve1") {
                                    self.emp_detail.emp_approve1 = item.stringValue
                                }
                                if(key == "emp_approve2") {
                                    self.emp_detail.emp_approve2 = item.stringValue
                                }
                                if(key == "IdentityCard") {
                                    self.emp_detail.IdentityCard = item.stringValue
                                }
                                if(key == "Height") {
                                    self.emp_detail.Height = Double(item.stringValue)!
                                }
                                if(key == "Weight") {
                                    self.emp_detail.Weight = Double(item.stringValue)!
                                }
                                if(key == "BGName") {
                                    self.emp_detail.BGName = item.stringValue
                                }
                                if(key == "Scar") {
                                    self.emp_detail.Scar = item.stringValue
                                }
                                if(key == "SocNo") {
                                    self.emp_detail.SocNo = item.stringValue
                                }
                                if(key == "SocHosName") {
                                    self.emp_detail.SocHosName = item.stringValue
                                }
                                if(key == "SocDateExpired") {
                                    self.emp_detail.SocDateExpired = item.stringValue
                                }
                            }
                            self.arr_emp_detail += [self.emp_detail]
                        }
                        
                        // set value
                        self.lbl_emp_name.text = self.arr_emp_detail[0].emp_name_th
                        self.lbl_pos_name.text = self.arr_emp_detail[0].pos_name_th
                        self.lbl_loc_name.text = self.arr_emp_detail[0].org_name_th
                        
                        // set to user defaults
                        defaults.set(self.arr_emp_detail[0].emp_code, forKey: "emp_code")
                        defaults.set(self.arr_emp_detail[0].emp_name_th, forKey: "emp_name_th")
                        defaults.set(self.arr_emp_detail[0].pos_name_th, forKey: "pos_name_th")
                        defaults.set(self.arr_emp_detail[0].emp_type_name, forKey: "emp_type_name")
                        defaults.set(self.arr_emp_detail[0].emp_start_date, forKey: "emp_start_date")
                        defaults.set(self.arr_emp_detail[0].emp_probation_date, forKey: "emp_probation_date")
                        defaults.set(self.arr_emp_detail[0].costcenter_no, forKey: "costcenter_no")
                        defaults.set(self.arr_emp_detail[0].rel_name_th, forKey: "rel_name_th")
                        defaults.set(self.arr_emp_detail[0].emp_nickname_th, forKey: "emp_nickname_th")
                        defaults.set(self.arr_emp_detail[0].sex_name_th, forKey: "sex_name_th")
                        defaults.set(self.arr_emp_detail[0].emp_birthday, forKey: "emp_birthday")
                        defaults.set(self.arr_emp_detail[0].IdentityCard, forKey: "IdentityCard")
                        
                        defaults.set(self.arr_emp_detail[0].org_name_th, forKey: "org_name_th")
                        defaults.set(self.arr_emp_detail[0].dept_name_th, forKey: "dept_name_th")
                        defaults.set(self.arr_emp_detail[0].sec_name_th, forKey: "sec_name_th")
                        
                        defaults.set(self.arr_emp_detail[0].emp_address, forKey: "emp_address")
                        defaults.set(self.arr_emp_detail[0].country_name, forKey: "country_name")
                        defaults.set(self.arr_emp_detail[0].prov_name, forKey: "prov_name")
                        defaults.set(self.arr_emp_detail[0].amp_name, forKey: "amp_name")
                        defaults.set(self.arr_emp_detail[0].dist_name, forKey: "dist_name")
                        defaults.set(self.arr_emp_detail[0].post_code, forKey: "post_code")
                        defaults.set(self.arr_emp_detail[0].emp_mobile_no, forKey: "emp_mobile_no")
                        defaults.set(self.arr_emp_detail[0].emp_phone_no, forKey: "emp_phone_no")
                        defaults.set(self.arr_emp_detail[0].emp_email, forKey: "emp_email")
                        
                        defaults.set(self.arr_emp_detail[0].Height, forKey: "Height")
                        defaults.set(self.arr_emp_detail[0].Weight, forKey: "Weight")
                        defaults.set(self.arr_emp_detail[0].BGName, forKey: "BGName")
                        defaults.set(self.arr_emp_detail[0].Scar, forKey: "Scar")
                        defaults.set(self.arr_emp_detail[0].SocHosName, forKey: "SocHosName")
                        defaults.set(self.arr_emp_detail[0].SocNo, forKey: "SocNo")
                        defaults.set(self.arr_emp_detail[0].SocDateExpired, forKey: "SocDateExpired")
                        
                        defaults.set(self.arr_emp_detail[0].emp_approve1, forKey: "emp_approve1")
                        defaults.set(self.arr_emp_detail[0].emp_approve2, forKey: "emp_approve2")
                        
                    } else {
                        self.alertError(return_msg: return_msg)
                    }
                    // print(responseData.result.value!)
                }
        }
    }
    //----- function for event -----//
}
