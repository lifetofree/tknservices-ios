//
//  It_It_ReceiveJobViewController.swift
//  tknservices
//
//  Created by MaI on 8/18/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_It_ReceiveJobViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_controller: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblCloseReceive: UITextField!
    
    var dataSystemClose = ["ดำเนินการเรียบร้อย","รออะไหล่","ส่งเคลม","ติดต่อซัพภายนอก"]
    var pvClose = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_controller.layer.cornerRadius = 10
        
        // set picker view
        pvClose.delegate = self
        pvClose.dataSource = self
        pvClose.tag = 1
        lblCloseReceive.text = dataSystemClose[0]
        lblCloseReceive.inputView = pvClose
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //if(pickerView.tag == 1) {
        return dataSystemClose.count
        //}
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //if(pickerView.tag == 1) {
        return dataSystemClose[row]
        //}
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //if(pickerView.tag == 1) {
        lblCloseReceive.text = dataSystemClose[row]
        //TempCloseReceive = lblCloseReceive.text!
        //}
        self.view.endEditing(true)
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        //if(unidx == 4 && acidx == 3){
            if(lblCloseReceive.text! == "รออะไหล่"){
                staidx = 43
            }
            else if(lblCloseReceive.text! == "ส่งเครม"){
                staidx = 44
            }
            else if(lblCloseReceive.text! == "ติดต่อซัพภายนอก"){
                staidx = 45
            }
        //}
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_ITGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        //print(lblCloseReceive.text!)
        if(lblCloseReceive.text! == "ดำเนินการเรียบร้อย"){
            let It_It_CloseJob = storyBoard.instantiateViewController(withIdentifier: "It_It_CloseJobViewController") as! It_It_CloseJobViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = It_It_CloseJob
        }
        else{
            
            urlUpdate()
            
            let itItTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itItTabBarController
            itItTabBarController.selectedIndex = 0 // It_It_ListViewController
        }  
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
        
    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
    //----- Navigation Bar Button -----//
}
