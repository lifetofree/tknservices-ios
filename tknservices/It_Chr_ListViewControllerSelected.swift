//
//  It_Chr_ListViewControllerSelected.swift
//  TKNServices
//
//  Created by lifetofree on 4/20/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Chr_ListViewControllerSelected: UIViewController {
    var val_selected = chr_u0document_detail()
    
    var u0_doc = chr_u0document_detail()
    var arr_u0_doc = [chr_u0document_detail]()
    
    var rsec_idx: Int = 0
    var approve_status: Int = 0
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var lblDocCode: UILabel!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblDeptName: UILabel!
    @IBOutlet weak var lblSecName: UILabel!
    @IBOutlet weak var lblSystemName: UILabel!
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblTopic: UILabel!
    @IBOutlet weak var lblOldSystem: UILabel!
    @IBOutlet weak var lblNewSystem: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set button
        btnConfirm.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnReject.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        
        setDetail()
        
        // check rsec_idx
        if let ud_rsec_idx = defaults.string(forKey: "rsec_idx") {
            rsec_idx = Int(ud_rsec_idx)!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // button event
    @IBAction func btnBack(_ sender: UIButton) {
        goToList()
    }
    
    @IBAction func btnConfirm(_ sender: UIButton) {
        if(rsec_idx == 149) {
            approve_status = 1
            approveCeo()
        } else {
            approve_status = 1
            approveSc()
        }
    }
    
    @IBAction func btnReject(_ sender: UIButton) {
        if(rsec_idx == 149) {
            approve_status = 2
            approveCeo()
        } else {
            approve_status = 2
            approveSc()
        }
    }
    // button event
    
    // function for event
    func setDetail() {
        self.arr_u0_doc = [chr_u0document_detail]()
        
        // set data and url
        let value =
            [
                "DataCHR" : [
                    "BindData_U0Document" : [
                        "staidx": 999,
                        "doc_code": val_selected.doc_code
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetChrDetail + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["DataCHR"]["ReturnCode"].stringValue)!
                    
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["DataCHR"]["BindData_U0Document"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["DataCHR"]["BindData_U0Document"].dictionaryValue {
                                if(key == "u0idx") {
                                    self.u0_doc.u0idx = Int(item.stringValue)!
                                }
                                if(key == "comment") {
                                    self.u0_doc.comment = item.stringValue
                                }
                                if(key == "old_sys") {
                                    self.u0_doc.old_sys = item.stringValue
                                }
                                if(key == "new_sys") {
                                    self.u0_doc.new_sys = item.stringValue
                                }
                                if(key == "Topic") {
                                    self.u0_doc.Topic = item.stringValue
                                }
                                if(key == "org_idx") {
                                    self.u0_doc.org_idx = Int(item.stringValue)!
                                }
                                if(key == "OrgNameTH") {
                                    self.u0_doc.OrgNameTH = item.stringValue
                                }
                                if(key == "rdept_idx") {
                                    self.u0_doc.rdept_idx = Int(item.stringValue)!
                                }
                                if(key == "DeptNameTH") {
                                    self.u0_doc.DeptNameTH = item.stringValue
                                }
                                if(key == "rsec_idx") {
                                    self.u0_doc.rsec_idx = Int(item.stringValue)!
                                }
                                if(key == "SecNameTH") {
                                    self.u0_doc.SecNameTH = item.stringValue
                                }
                                if(key == "doc_code") {
                                    self.u0_doc.doc_code = item.stringValue
                                }
                                if(key == "Sysidx") {
                                    self.u0_doc.Sysidx = Int(item.stringValue)!
                                }
                                if(key == "System_name") {
                                    self.u0_doc.System_name = item.stringValue
                                }
                                if(key == "mtidx") {
                                    self.u0_doc.mtidx = Int(item.stringValue)!
                                }
                                if(key == "Typename") {
                                    self.u0_doc.TypeName = item.stringValue
                                }
                                if(key == "tidx") {
                                    self.u0_doc.tidx = Int(item.stringValue)!
                                }
                                if(key == "EmpName") {
                                    self.u0_doc.EmpName = item.stringValue
                                }
                                if(key == "datecreatestart") {
                                    self.u0_doc.datecreatestart = item.stringValue
                                }
                                if(key == "Email") {
                                    self.u0_doc.Email = item.stringValue
                                }
                                if(key == "MobileNo") {
                                    self.u0_doc.MobileNo = item.stringValue
                                }
                                if(key == "PhoneNo") {
                                    self.u0_doc.PhoneNo = item.stringValue
                                }
                                if(key == "acidx") {
                                    self.u0_doc.acidx = Int(item.stringValue)!
                                }
                                if(key == "noidx") {
                                    self.u0_doc.noidx = Int(item.stringValue)!
                                }
                                if(key == "node_name") {
                                    self.u0_doc.node_name = item.stringValue
                                }
                                if(key == "actor_des") {
                                    self.u0_doc.actor_des = item.stringValue
                                }
                                if(key == "doc_decision") {
                                    self.u0_doc.doc_decision = Int(item.stringValue)!
                                }
                                if(key == "status_name") {
                                    self.u0_doc.status_name = item.stringValue
                                }
                                if(key == "CostNo") {
                                    self.u0_doc.CostNo = item.stringValue
                                }
                                if(key == "StatusDoc") {
                                    self.u0_doc.StatusDoc = item.stringValue
                                }
                                if(key == "Usersap") {
                                    self.u0_doc.Usersap = item.stringValue
                                }
                                if(key == "NEmpidx1") {
                                    self.u0_doc.NEmpidx1 = item.stringValue
                                }
                                if(key == "NEmpidx2") {
                                    self.u0_doc.NEmpidx2 = item.stringValue
                                }
                                if(key == "NEmpidx3") {
                                    self.u0_doc.NEmpidx3 = item.stringValue
                                }
                            }
                            
                            self.arr_u0_doc += [self.u0_doc]
                        } else if swiftyJsonVar["DataCHR"]["BindData_U0Document"].arrayObject != nil {
                            for (item) in swiftyJsonVar["DataCHR"]["BindData_U0Document"].arrayValue {
                                self.u0_doc.u0idx = Int(item["u0idx"].stringValue)!
                                self.u0_doc.comment = item["comment"].stringValue
                                self.u0_doc.old_sys = item["old_sys"].stringValue
                                self.u0_doc.new_sys = item["new_sys"].stringValue
                                self.u0_doc.Topic = item["Topic"].stringValue
                                self.u0_doc.org_idx = Int(item["org_idx"].stringValue)!
                                self.u0_doc.OrgNameTH = item["OrgNameTH"].stringValue
                                self.u0_doc.rdept_idx = Int(item["rdept_idx"].stringValue)!
                                self.u0_doc.DeptNameTH = item["DeptNameTH"].stringValue
                                self.u0_doc.rsec_idx = Int(item["rsec_idx"].stringValue)!
                                self.u0_doc.SecNameTH = item["SecNameTH"].stringValue
                                self.u0_doc.doc_code = item["doc_code"].stringValue
                                self.u0_doc.Sysidx = Int(item["Sysidx"].stringValue)!
                                self.u0_doc.System_name = item["System_name"].stringValue
                                self.u0_doc.mtidx = Int(item["mtidx"].stringValue)!
                                self.u0_doc.TypeName = item["Typename"].stringValue
                                self.u0_doc.tidx = Int(item["tidx"].stringValue)!
                                self.u0_doc.EmpName = item["EmpName"].stringValue
                                self.u0_doc.datecreatestart = item["datecreatestart"].stringValue
                                self.u0_doc.Email = item["Email"].stringValue
                                self.u0_doc.MobileNo = item["MobileNo"].stringValue
                                self.u0_doc.PhoneNo = item["PhoneNo"].stringValue
                                self.u0_doc.acidx = Int(item["acidx"].stringValue)!
                                self.u0_doc.noidx = Int(item["noidx"].stringValue)!
                                self.u0_doc.node_name = item["node_name"].stringValue
                                self.u0_doc.actor_des = item["actor_des"].stringValue
                                self.u0_doc.doc_decision = Int(item["doc_decision"].stringValue)!
                                self.u0_doc.status_name = item["status_name"].stringValue
                                self.u0_doc.CostNo = item["CostNo"].stringValue
                                self.u0_doc.StatusDoc = item["StatusDoc"].stringValue
                                self.u0_doc.Usersap = item["Usersap"].stringValue
                                self.u0_doc.NEmpidx1 = item["NEmpidx1"].stringValue
                                self.u0_doc.NEmpidx2 = item["NEmpidx2"].stringValue
                                self.u0_doc.NEmpidx3 = item["NEmpidx3"].stringValue
                                
                                self.arr_u0_doc += [self.u0_doc]
                            }
                        }
                        self.lblDocCode.text = self.val_selected.doc_code
                        if(self.arr_u0_doc.count > 0) {
                            self.lblEmpName.text = self.arr_u0_doc[0].EmpName
                            self.lblOrgName.text = self.arr_u0_doc[0].OrgNameTH
                            self.lblDeptName.text = self.arr_u0_doc[0].DeptNameTH
                            self.lblSecName.text = self.arr_u0_doc[0].SecNameTH
                            self.lblSystemName.text = self.arr_u0_doc[0].System_name
                            self.lblTypeName.text = self.arr_u0_doc[0].TypeName
                            self.lblTopic.text = self.arr_u0_doc[0].Topic
                            self.lblOldSystem.text = self.arr_u0_doc[0].old_sys
                            self.lblNewSystem.text = self.arr_u0_doc[0].new_sys
                            
                            if(self.rsec_idx != 149 && Int(self.val_selected.check_ceo) == 1)
                            {
                                self.btnConfirm.isHidden = true
                                self.btnReject.isHidden = true
                            }
                        }
                    } else {
                        //                        self.loginFail()
                    }
                    // print(responseData.result.value!)
                }
        }
    }
    
    func approveCeo() {
        // set data and url
        let value =
            [
                "data_chr" : [
                    "BindData_U0Document" : [
                        "u0idx_add": self.arr_u0_doc[0].u0idx,//val_selected.doc_code,
                        "noidx_add": 5,
                        "acidx_add": 5,
                        "Add_IDX_add": Int(sEmpIDX)!,
                        "approve_status_add": approve_status,
                        "comment": "-"
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSetChrCeoApprove + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
//                    self.goToList()
                }
                self.goToList()
        }
    }
    
    func approveSc() {
        // set data and url
        let value =
            [
                "DataCHR" : [
                    "BindData_U0Document" : [
                        "approve_nocheck": val_selected.doc_code,
                        "approve_check": approve_status,
                        "u0idx_check": 888
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSetChrScApprove + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    self.goToList()
                }
        }
    }
    
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itChrListViewController = storyBoard.instantiateViewController(withIdentifier: "It_Chr_ListViewController") as! It_Chr_ListViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itChrListViewController
    }
    // function for event
}
