//
//  CenPerGeneralViewController05.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CenPerGeneralViewController05: UIViewController {
    
    @IBOutlet weak var lblEmpApprove1: UILabel!
    @IBOutlet weak var lblEmpApprove2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_approve1 = defaults.string(forKey: "emp_approve1") {
            lblEmpApprove1.text = ud_emp_approve1
        }
        if let ud_emp_approve2 = defaults.string(forKey: "emp_approve2") {
            lblEmpApprove2.text = ud_emp_approve2
        }
    }
}
