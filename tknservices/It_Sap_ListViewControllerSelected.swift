//
//  It_Sap_ListViewControllerSelected.swift
//  tknservices
//
//  Created by lifetofree on 8/22/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Sap_ListViewControllerSelected: UIViewController {
    
    @IBOutlet weak var view_segment: UIView!
    @IBOutlet weak var view_content: UIView!
    
    var sap_container: ContainerViewController!
    var user_selected = UserRequest_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_segment.layer.cornerRadius = 5
        view_content.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            sap_container.segueIdentifierReceivedFromParent("sap_detail")
        } else if sender.selectedSegmentIndex == 1 {
            sap_container.segueIdentifierReceivedFromParent("sap_comment")
        } else {
            sap_container.segueIdentifierReceivedFromParent("sap_administrator")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sap_container" {
            self.sap_container = segue.destination as! ContainerViewController
            setDetail()
        }
    }
    
    func setDetail() {
        
        DocCode = user_selected.DocCode
        CreateDateUser = user_selected.CreateDateUser + " " + user_selected.TimeCreateJob
        EmpName = user_selected.EmpName
        TelETC = user_selected.TelETC
        LocName = user_selected.LocName
        Comment = user_selected.detailUser
        
        URQIDX = user_selected.URQIDX
        unidx = user_selected.unidx
        acidx = user_selected.acidx
        RDeptIDX = user_selected.RDeptIDX
        SysIDX = user_selected.SysIDX
        EmpIDX_Create = user_selected.EmpIDX
        OrgIDX = user_selected.OrgIDX
        UserLogonName = user_selected.UserLogonName
        TransactionCode = user_selected.TransactionCode
        
    }
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 // It_Sap_ListViewController
    }
    //----- Navigation Bar Button -----//
}
