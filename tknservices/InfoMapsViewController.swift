//
//  InfoMapsViewController.swift
//  tknservices
//
//  Created by lifetofree on 12/22/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class InfoMapsViewController: UIViewController {
    
    @IBOutlet weak var imgMaps: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped))
        imgMaps.isUserInteractionEnabled = true
        imgMaps.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // go to guest central
    @IBAction func btnGuestHome(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "GuestCentralViewController") as! GuestCentralViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginController
    }
    // go to guest central
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        // tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        newImageView.addGestureRecognizer(tap)
        // pinch
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        newImageView.addGestureRecognizer(pinchRecognizer)
        self.view.addSubview(newImageView)
        //        // pan
        //        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        //        newImageView.addGestureRecognizer(panRecognizer)
    }
    
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func handlePinchGesture(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            if CGFloat(view.transform.a) > 5.0 {
                view.transform.a = 5.0 // this is x coordinate
                view.transform.d = 5.0 // this is x coordinate
            }
            if CGFloat(view.transform.d) < 1.0 {
                view.transform.a = 1.0 // this is x coordinate
                view.transform.d = 1.0 // this is x coordinate
            }
            sender.scale = 1
            
            // pan
            let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            if view.transform.a > 1.0 {
                view.addGestureRecognizer(panRecognizer)
                print("aaa")
            } else {
                view.removeGestureRecognizer(panRecognizer)
                print("bbb")
            }
            print(view.transform.a)
        }
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        // get translation
        let translation = sender.translation(in: view)
        sender.setTranslation(CGPoint.zero, in: view)
        //        print(translation)
        
        //create a new Label and give it the parameters of the old one
        let label = sender.view! as UIView
        label.center = CGPoint(x: label.center.x + translation.x, y: label.center.y + translation.y)
        label.isMultipleTouchEnabled = true
        label.isUserInteractionEnabled = true
        
        //        if sender.state == UIGestureRecognizerState.began {
        //            //add something you want to happen when the Label Panning has started
        //            print("started")
        //        }
        //
        //        if sender.state == UIGestureRecognizerState.ended {
        //            //add something you want to happen when the Label Panning has ended
        //            print("ended")
        //        }
        //
        //
        //        if sender.state == UIGestureRecognizerState.changed {
        //            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
        //            print("changed")
        //        }
        //        else {
        //            // or something when its not moving
        //            print("not moving")
        //        }
    }
}
