//
//  It_It_DetailViewController.swift
//  tknservices
//
//  Created by MaI on 7/24/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class It_It_ListViewControllerSelected: UIViewController {
    
    @IBOutlet weak var view_segment: UIView!
    @IBOutlet weak var view_content: UIView!
    
    var user_selected = UserRequest_detail()
    var It_Container : ContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_segment.layer.cornerRadius = 5
        view_content.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            It_Container.segueIdentifierReceivedFromParent("It_Detail")
        }
        else if(sender.selectedSegmentIndex == 1){
            It_Container.segueIdentifierReceivedFromParent("It_Comment")
        }
        else{
            It_Container.segueIdentifierReceivedFromParent("It_Administrator")
            setDetail()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "It_Container"{
            
            It_Container = segue.destination as! ContainerViewController
            setDetail()
        }
    }
    
    func setDetail() {
        DocCode = user_selected.DocCode
        CreateDateUser = user_selected.CreateDateUser + " " + user_selected.TimeCreateJob
        EmpName = user_selected.EmpName
        TelETC = user_selected.TelETC
        LocName = user_selected.LocName
        RemoteName = user_selected.RemoteName
        UserIDRemote = user_selected.UserIDRemote + "," + user_selected.PasswordRemote
        Comment = user_selected.detailUser
        
        URQIDX = user_selected.URQIDX
        unidx = user_selected.unidx
        acidx = user_selected.acidx
        RDeptIDX = user_selected.RDeptIDX
        SysIDX = user_selected.SysIDX
        EmpIDX_Create = user_selected.EmpIDX
        OrgIDX = user_selected.OrgIDX
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itTabBarController
        itTabBarController.selectedIndex = 0
    }

}

