//
//  Vac_ApproveViewControllerSelected.swift
//  tknservices
//
//  Created by lifetofree on 4/6/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Vac_ApproveViewControllerSelected: UIViewController {
    var val_selected = u0_document_detail()
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblLeaveType: UILabel!
    @IBOutlet weak var lblLeaveStat: UILabel!
    @IBOutlet weak var lblLeaveStart: UILabel!
    @IBOutlet weak var lblLeaveEnd: UILabel!
    @IBOutlet weak var lblLeaveComment: UILabel!
    @IBOutlet weak var imgAttach: UIImageView!
    
    @IBOutlet weak var btnApprove: UIButton!
    @IBOutlet weak var btnRejectEdit: UIButton!
    @IBOutlet weak var btnRejectCancel: UIButton!
    
    var leave_limit = [ "", "30", "7", "0", "90", "15", "n" ]
    
    var _emp_idx: String = "0"
    var _leave_type_idx: Int = 0
    var _emp_start_date: String = ""
    var _emp_probation_date: String = ""
    var _vacation_limit: Double = 0
    var _vacation_stat: String = ""
    
    var stat_detail = vacation_stat_detail()
    var arr_stat_detail = [vacation_stat_detail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let ud_emp_start_date = defaults.string(forKey: "emp_start_date") {
            _emp_start_date = ud_emp_start_date
        }
        
        if let ud_emp_probation_date = defaults.string(forKey: "emp_probation_date") {
            _emp_probation_date = ud_emp_probation_date
        }
        
        // set button
        btnApprove.roundButton(rad: 5.0, buttonColor: 0x4E9B13, borderColor: 0x3A4B1A, borderWidth: 1.0)
        btnRejectEdit.roundButton(rad: 5.0, buttonColor: 0xE79115, borderColor: 0xA75107, borderWidth: 1.0)
        btnRejectCancel.roundButton(rad: 5.0, buttonColor: 0xFF372F, borderColor: 0x7F0308, borderWidth: 1.0)
        // set button
        
        setDetail()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function for event
    // set approve detail
    func setDetail() {
        _emp_idx = String(val_selected.emp_idx)
        _leave_type_idx = Int(val_selected.m0_leavetype_idx)
        //print(_emp_idx + "|" + String(_leave_type_idx))
        
        lblEmpName.text = val_selected.emp_name
        lblLeaveType.text = val_selected.m0_leavetype_name
        lblLeaveStart.text = val_selected.u0_leave_start
        lblLeaveEnd.text = val_selected.u0_leave_end
        lblLeaveComment.text = val_selected.u0_leave_comment
        
        getVacationStat(emp_idx: _emp_idx, leave_type: _leave_type_idx)
        
        let img_ext = [".jpg", ".jpeg", ".png", ".gif"]
        
        for item in img_ext {
            // check image exists
            if let httpUrl = String(sUploadUrlVac + String(val_selected.u0_document_idx) + item),
                let imgUrl = URL(string: httpUrl),
                let imgData = NSData(contentsOf: imgUrl) {
                // use image value
                let imgObj = UIImage(data: imgData as Data)
                imgAttach.image = imgObj
                
                let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped))
                imgAttach.isUserInteractionEnabled = true
                imgAttach.addGestureRecognizer(tapGestureRecognizer)
                
                return
            }
        }
    }
    
    func getVacationStat(emp_idx: String, leave_type: Int) {
        let tempUrl = String(urlGetMyStat + emp_idx)
        let sendUrl = URL(string: tempUrl!)!
        //print(sendUrl)

        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue

                    // check return_code
                    if (return_code == 0) {
                        // get value
                        if swiftyJsonVar["data_vacation"]["vacation_stat_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_vacation"]["vacation_stat_list"].dictionaryValue {
                                if(key == "m0_leavetype_idx") {
                                    self.stat_detail.m0_leavetype_idx = Int(item.stringValue)!
                                }
                                if(key == "m0_leavetype_name") {
                                    self.stat_detail.m0_leavetype_name = item.stringValue
                                }
                                if(key == "stat_min") {
                                    self.stat_detail.stat_min = Double(item.stringValue)!
                                }
                            }
                            self.arr_stat_detail += [self.stat_detail]
                        }
                        else if swiftyJsonVar["data_vacation"]["vacation_stat_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_vacation"]["vacation_stat_list"].arrayValue {
                                self.stat_detail.m0_leavetype_idx = Int(item["m0_leavetype_idx"].stringValue)!
                                self.stat_detail.m0_leavetype_name = item["m0_leavetype_name"].stringValue
                                self.stat_detail.stat_min = Double(item["stat_min"].stringValue)!

                                self.arr_stat_detail += [self.stat_detail]
                            }
                        }

                        if(self.arr_stat_detail.count > 0) {
                            for (item) in self.arr_stat_detail
                            {
                                if (item.m0_leavetype_idx == leave_type) {
                                    switch (item.m0_leavetype_idx) {
                                    case 1, 2, 4, 5, 6:
                                        self._vacation_stat = String(convertMinToHour(item.stat_min)) + " / " + self.leave_limit[leave_type] + " วัน"
                                    case 3:
                                        self._vacation_stat = String(convertMinToHour(item.stat_min)) + " / " + String(calcVacation(start_date: self._emp_start_date, probation_date: self._emp_probation_date)) + " วัน"
                                    case 11, 12, 13:
                                        self._vacation_stat = String(item.stat_min)
                                    default:
                                        self._vacation_stat = "0" + " รายการ"
                                    }
                                    continue
                                }
                            }
                        }
                        // set value
                        // print("stat : " + self._vacation_stat + " | " + String(leave_type))
                    } else {
                        self.alertError(return_msg: return_msg)
                    }
                    // print(responseData.result.value!)
                    // set value
                    if(self._vacation_stat != "") {
                        self.lblLeaveStat.text = self._vacation_stat
                    } else {
                        switch (leave_type) {
                        case 1, 2, 4, 5, 6:
                            self._vacation_stat = "0" + " / " + self.leave_limit[leave_type] + " วัน"
                        case 3:
                            self._vacation_stat = "0" + " / " + String(calcVacation(start_date: self._emp_start_date, probation_date: self._emp_probation_date)) + " วัน"
                        case 11, 12, 13:
                            self._vacation_stat = "0"
                        default:
                            self._vacation_stat = "0" + " รายการ"
                        }
                    }
                }
        }
    }
    
    // go to approve list
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vacTabBarController = storyBoard.instantiateViewController(withIdentifier: "Vac_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = vacTabBarController
        vacTabBarController.selectedIndex = 2 // Vac_ApproveViewController
    }
    
    func setApprove(approveType: Int) -> Int {
        // set data and url
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            iEmpIDX = Int(ud_emp_idx as! String)!
        }
//        print(String(iEmpIDX))
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : val_selected.u0_document_idx,
                        "m0_leavetype_idx" : val_selected.m0_leavetype_idx,
                        "u0_emp_shift" : val_selected.u0_emp_shift,
                        "m0_node_idx" : val_selected.m0_node_idx,
                        "m0_actor_idx" : val_selected.m0_actor_idx,
                        "emp_idx" : val_selected.emp_idx
                    ],
                    "u1_document_list" : [
                        "u0_document_idx" : val_selected.u0_document_idx,
                        "action_emp_idx" : iEmpIDX,
                        "action_node_idx" : 2, // node for approve
                        "u1_approve_status" : approveType
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSetU0DocumentApprove + dataIn)
        let sendUrl = URL(string: tempUrl!)!
//        print(value)
//        print(sendUrl)
        // set data and url
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                }
                
        }
        
        return return_code
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goToList()
    }
    
    @IBAction func btnApprove(_ sender: UIButton) {
        return_code = setApprove(approveType: 1) // approve
        // when completed
        goToList()
    }
    
    @IBAction func btnRejectEdit(_ sender: UIButton) {
        return_code = setApprove(approveType: 2) // approve
        // when completed
        goToList()
    }
    
    @IBAction func btnRejectCancel(_ sender: UIButton) {
        return_code = setApprove(approveType: 3) // approve
        // when completed
        goToList()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        // tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        newImageView.addGestureRecognizer(tap)
        // pinch
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        newImageView.addGestureRecognizer(pinchRecognizer)
        self.view.addSubview(newImageView)
//        // pan
//        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
//        newImageView.addGestureRecognizer(panRecognizer)
    }
    
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func handlePinchGesture(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            if CGFloat(view.transform.a) > 5.0 {
                view.transform.a = 5.0 // this is x coordinate
                view.transform.d = 5.0 // this is x coordinate
            }
            if CGFloat(view.transform.d) < 1.0 {
                view.transform.a = 1.0 // this is x coordinate
                view.transform.d = 1.0 // this is x coordinate
            }
            sender.scale = 1
            
            // pan
            let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            if view.transform.a > 1.0 {
                view.addGestureRecognizer(panRecognizer)
//                print("aaa")
            } else {
                view.removeGestureRecognizer(panRecognizer)
//                print("bbb")
            }
//            print(view.transform.a)
        }
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        // get translation
        let translation = sender.translation(in: view)
        sender.setTranslation(CGPoint.zero, in: view)
//        print(translation)
        
        //create a new Label and give it the parameters of the old one
        let label = sender.view! as UIView
        label.center = CGPoint(x: label.center.x + translation.x, y: label.center.y + translation.y)
        label.isMultipleTouchEnabled = true
        label.isUserInteractionEnabled = true
        
//        if sender.state == UIGestureRecognizerState.began {
//            //add something you want to happen when the Label Panning has started
//            print("started")
//        }
//
//        if sender.state == UIGestureRecognizerState.ended {
//            //add something you want to happen when the Label Panning has ended
//            print("ended")
//        }
//
//
//        if sender.state == UIGestureRecognizerState.changed {
//            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
//            print("changed")
//        }
//        else {
//            // or something when its not moving
//            print("not moving")
//        }
    }
    // function for event
}
