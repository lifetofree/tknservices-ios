//
//  It_Chr_ListViewController.swift
//  TKNServices
//
//  Created by lifetofree on 4/20/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Chr_ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var refreshControl = UIRefreshControl()
    
    var u0_doc = chr_u0document_detail()
    var arr_u0_doc = [chr_u0document_detail]()
    
    var url_list: String = ""
    var rsec_idx: Int = 0
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var tbvList: UITableView!
    let textCellIdentifier = "tvcList"
    
    var data_selected = chr_u0document_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set table view w/ transparent background
        self.tbvList.backgroundColor = UIColor.clear
        
        tbvList.delegate = self
        tbvList.dataSource = self
        
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(It_Chr_ListViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tbvList.addSubview(refreshControl)
        
        // check rsec_idx
        if let ud_rsec_idx = defaults.string(forKey: "rsec_idx") {
            rsec_idx = Int(ud_rsec_idx)!
            if(rsec_idx == 149) {
                url_list = urlGetChrCeoList
            } else {
                url_list = urlGetChrScList
            }
        }
        
        if(url_list != "") {
            self.getAllList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_doc.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: It_Chr_CustomCellList = tbvList.dequeueReusableCell(withIdentifier: textCellIdentifier) as! It_Chr_CustomCellList
        
        let row = (indexPath as NSIndexPath).row

        // set value
        let _chr_no = arr_u0_doc[row].doc_code
        let _chr_system = arr_u0_doc[row].System_name
        let _chr_create_date = arr_u0_doc[row].datecreate
        let _chr_emp_name = arr_u0_doc[row].EmpName
        let _chr_status = arr_u0_doc[row].StatusDoc
        
        // load item
        cell.loadItem(chr_no: _chr_no, chr_system: _chr_system, chr_create_date: _chr_create_date, chr_emp_name: _chr_emp_name, chr_status: _chr_status)
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        data_selected = arr_u0_doc[row]
    }
    
    //--- connected to other view ---//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let iIndexPath : IndexPath = self.tbvList.indexPathForSelectedRow! as IndexPath

        let detailView = segue.destination as! It_Chr_ListViewControllerSelected
        
        data_selected = arr_u0_doc[iIndexPath.row]
        detailView.val_selected = data_selected
    }
    
    // function for event
    // get all approve
    func getAllList() {
        self.arr_u0_doc = [chr_u0document_detail]()

        // set data and url
        let value =
            [
                "DataCHR" : [
                    "BindData_U0Document" : [
                        "CEmpidx": Int(sEmpIDX)!
                    ]
                ]
        ]

        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(url_list + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = 0 //Int(swiftyJsonVar["DataCHR"]["ReturnCode"].stringValue)!

                    // check return_code
                    if (return_code == 0) {
                        if(self.rsec_idx == 149) {
                            if swiftyJsonVar["data_chr"]["BindData_U0Document"].dictionaryObject != nil {
                                for (key, item) in swiftyJsonVar["data_chr"]["BindData_U0Document"].dictionaryValue {
                                    if(key == "doc_code") {
                                        self.u0_doc.doc_code = item.stringValue
                                    }
                                    if(key == "System_name") {
                                        self.u0_doc.System_name = item.stringValue
                                    }
                                    if(key == "datecreate") {
                                        self.u0_doc.datecreate = item.stringValue
                                    }
                                    if(key == "EmpName") {
                                        self.u0_doc.EmpName = item.stringValue
                                    }
                                    if(key == "StatusDoc") {
                                        self.u0_doc.StatusDoc = item.stringValue
                                    }
                                }
                                self.arr_u0_doc += [self.u0_doc]
                            } else if swiftyJsonVar["data_chr"]["BindData_U0Document"].arrayObject != nil {
                                for (item) in swiftyJsonVar["data_chr"]["BindData_U0Document"].arrayValue {
                                    self.u0_doc.doc_code = item["doc_code"].stringValue
                                    self.u0_doc.System_name = item["System_name"].stringValue
                                    self.u0_doc.datecreate = item["datecreate"].stringValue
                                    self.u0_doc.EmpName = item["EmpName"].stringValue
                                    self.u0_doc.StatusDoc = item["StatusDoc"].stringValue
                                    
                                    self.arr_u0_doc += [self.u0_doc]
                                }
                            }
                        } else {
                            if swiftyJsonVar["DataCHR"]["BindData_U0Document"].dictionaryObject != nil {
                                for (key, item) in swiftyJsonVar["DataCHR"]["BindData_U0Document"].dictionaryValue {
                                    if(key == "doc_code") {
                                        self.u0_doc.doc_code = item.stringValue
                                    }
                                    if(key == "System_name") {
                                        self.u0_doc.System_name = item.stringValue
                                    }
                                    if(key == "datecreate") {
                                        self.u0_doc.datecreate = item.stringValue
                                    }
                                    if(key == "EmpName") {
                                        self.u0_doc.EmpName = item.stringValue
                                    }
                                    if(key == "StatusDoc") {
                                        self.u0_doc.StatusDoc = item.stringValue
                                    }
                                    if(key == "check_ceo") {
                                        self.u0_doc.check_ceo = Int(item.stringValue)!
                                    }
                                }
                                self.arr_u0_doc += [self.u0_doc]
                            } else if swiftyJsonVar["DataCHR"]["BindData_U0Document"].arrayObject != nil {
                                for (item) in swiftyJsonVar["DataCHR"]["BindData_U0Document"].arrayValue {
                                    self.u0_doc.doc_code = item["doc_code"].stringValue
                                    self.u0_doc.System_name = item["System_name"].stringValue
                                    self.u0_doc.datecreate = item["datecreate"].stringValue
                                    self.u0_doc.EmpName = item["EmpName"].stringValue
                                    self.u0_doc.StatusDoc = item["StatusDoc"].stringValue
                                    self.u0_doc.check_ceo = Int(item["check_ceo"].stringValue)!
                                    
                                    self.arr_u0_doc += [self.u0_doc]
                                }
                            }
                        }
                        
                    } else {
                        //                        self.loginFail()
                    }
                    // print(responseData.result.value!)

                    // count data and reload
                    if (self.arr_u0_doc.count > 0) {
                        // set back to label view
                        self.tbvList.backgroundView = nil;

                        self.tbvList.reloadData()
                    } else {
                        self.tbvList.reloadData()

                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvList.bounds.size.width, height: self.tbvList.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray

                        // set back to label view
                        self.tbvList.backgroundView = lblEmpty;
                        // no separator
                        self.tbvList.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    // function for event
    
    // button event
    @IBAction func btnGoToHome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 1 // CentralTabBarController
    }
    // button event
}
