//
//  CenPerGeneralViewController06.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CenPerGeneralViewController06: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tfOldPassword.delegate = self
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func btnSave(_ sender: Any) {
        setMyPassword()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    
    // function for event
    func setMyPassword() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        
        // check tfOldPassword for nil value
        guard let empOldPassText = tfOldPassword.text , !empOldPassText.isEmpty else {
            alertEmptyValue(tfOldPassword)
            return
        }
        
        // check tfNewPassword for nil value
        guard let empNewPassText = tfNewPassword.text , !empNewPassText.isEmpty else {
            alertEmptyValue(tfNewPassword)
            return
        }
        
        // check tfConfirmPassword for nil value
        guard let empConfirmPassText = tfConfirmPassword.text , !empConfirmPassText.isEmpty else {
            alertEmptyValue(tfConfirmPassword)
            return
        }
        
        if(empNewPassText == empConfirmPassText && empNewPassText.count > 7) {
            // set data and url
            let value =
                [
                    "data_employee" : [
                        "employee_list" : [
                            "emp_idx" : Int(sEmpIDX)!,
                            "emp_password" : convertToMd5(empOldPassText),
                            "new_password" : convertToMd5(empNewPassText),
                            "ip_addres" : "mobile",
                            "type_reset" : 2
                        ]
                    ]
            ]
            
            let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
            let tempUrl = String(urlSetMyPassword + dataIn)
            let sendUrl = URL(string: tempUrl!)!
            // set data and url
            // print(sendUrl)
            // call web service
            Alamofire.request(
                sendUrl,
                method: .get,
                parameters: nil,
                encoding: URLEncoding.default,
                headers: nil)
                .responseJSON { (responseData) -> Void in
                    if((responseData.result.value) != nil) {
                        let swiftyJsonVar = JSON(responseData.result.value!)
                        // get return_code
                        return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                        
                        // check return_code
                        if (return_code == 0) {
                            self.changePasswordSuccess()
                        } else {
                            let notFindAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: "ไม่พบข้อมูลที่ตรงกัน กรุณาลองใหม่อีกครั้งค่ะ", preferredStyle: UIAlertControllerStyle.alert);
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
                                // set focus
                                self.tfOldPassword.becomeFirstResponder()
                            }
                            
                            notFindAlert.addAction(okAction)
                            self.present(notFindAlert, animated: true, completion: nil)
                        }
                        //print(responseData.result.value!)
                    }
                    
            }
        } else {
            let notMatchAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: "กรุณากรอกรหัสผ่านให้ตรงกัน และไม่น้อยกว่า 8 ตัวอักษรค่ะ", preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
                // set focus
                self.tfNewPassword.becomeFirstResponder()
            }
            
            notMatchAlert.addAction(okAction)
            self.present(notMatchAlert, animated: true, completion: nil)
        }
    }
    
    // alert when UITextField empty
    func alertEmptyValue(_ _tbName : UITextField) {
        var _textAlert = [tfOldPassword: "กรุณากรอกรหัสผ่านเดิมด้วยค่ะ", tfNewPassword: "กรุณากรอกรหัสผ่านใหม่ด้วยค่ะ", tfConfirmPassword: "กรุณากรอกยืนยันรหัสผ่านอีกครั้งค่ะ"]
        let emptyAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: _textAlert[_tbName], preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // set focus
            _tbName.becomeFirstResponder()
        }
        
        emptyAlert.addAction(okAction)
        
        self.present(emptyAlert, animated: true, completion: nil)
    }
    
    func changePasswordSuccess() {
        defaults.removeObject(forKey: "emp_idx")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = dataViewController
    }
    // function for event
}

