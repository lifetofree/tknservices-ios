//
//  It_It_JobTransViewController.swift
//  tknservices
//
//  Created by MaI on 8/18/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class It_It_JobTransViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_controller: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblSystem: UITextField!
    
    var dataSystemTransfer = ["Google Apps","SAP"]
    var pvSystem = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_controller.layer.cornerRadius = 10
        
        // set picker view
        pvSystem.delegate = self
        pvSystem.dataSource = self
        pvSystem.tag = 1
        lblSystem.text = dataSystemTransfer[0]
        lblSystem.inputView = pvSystem

        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        /*lblDocCode.text = DocCode
        lblCreateDateTime.text = CreateDateUser
        lblEmpName_Create.text = EmpName
        lblTel.text = TelETC
        lblLocation.text = LocName
        lblRemoteName.text = RemoteName
        lblUserRemote.text = UserIDRemote
        lblComment.text = Comment*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get urlChangeSystem
    func urlChangeSystem() {

        if(lblSystem.text! == "Google Apps"){
            Temp_transfer = 1
        }
        else if(lblSystem.text! == "SAP"){
            Temp_transfer = 2
        }
        else if(lblSystem.text! == "IT"){
            Temp_transfer = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "StaIDX" : 0,
                        "SysIDX_add" : Temp_transfer,
                        "OrgIDX" : OrgIDX,
                        "CommentAMDoing" : "โอนย้ายไประบบ" + lblSystem.text!,
                        "acidx" : acidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_ChangeSystem + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //if(pickerView.tag == 1) {
            return dataSystemTransfer.count
        //}
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //if(pickerView.tag == 1) {
            return dataSystemTransfer[row]
        //}
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //if(pickerView.tag == 1) {
            lblSystem.text = dataSystemTransfer[row]
        //}
        self.view.endEditing(true)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        urlChangeSystem()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itItTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itItTabBarController
        itItTabBarController.selectedIndex = 0 // It_It_ListViewController
        
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
    //----- Event Button -----//
    
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
    //----- Navigation Bar Button -----//
}
