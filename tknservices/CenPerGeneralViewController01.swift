//
//  CenPerGeneralViewController01.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CenPerGeneralViewController01: UIViewController {
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblEmpCode: UILabel!
    @IBOutlet weak var lblEmpType: UILabel!
    @IBOutlet weak var lblEmpStart: UILabel!
    @IBOutlet weak var lblEmpProbation: UILabel!
    @IBOutlet weak var lblEmpCost: UILabel!
    @IBOutlet weak var lblEmpStatus: UILabel!
    @IBOutlet weak var lblEmpReligion: UILabel!
    @IBOutlet weak var lblEmpNickName: UILabel!
    @IBOutlet weak var lblEmpSex: UILabel!
    @IBOutlet weak var lblEmpBirthday: UILabel!
    @IBOutlet weak var lblEmpCardId: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            lblEmpName.text = ud_emp_name_th
        }
        if let ud_emp_code = defaults.string(forKey: "emp_code") {
            lblEmpCode.text = ud_emp_code
        }
        if let ud_emp_type_name = defaults.string(forKey: "emp_type_name") {
            lblEmpType.text = ud_emp_type_name
        }
        if let ud_emp_start_date = defaults.string(forKey: "emp_start_date") {
            lblEmpStart.text = ud_emp_start_date
        }
        if let ud_emp_probation_date = defaults.string(forKey: "emp_probation_date") {
            lblEmpProbation.text = ud_emp_probation_date
        }
        if let ud_costcenter_no = defaults.string(forKey: "costcenter_no") {
            lblEmpCost.text = ud_costcenter_no
        }
        if let ud_emp_status = defaults.string(forKey: "emp_status") {
            lblEmpStatus.text = ud_emp_status
        }
        if let ud_rel_name_th = defaults.string(forKey: "rel_name_th") {
            lblEmpReligion.text = ud_rel_name_th
        }
        if let ud_emp_nickname_th = defaults.string(forKey: "emp_nickname_th") {
            lblEmpNickName.text = ud_emp_nickname_th
        }
        if let ud_sex_name_th = defaults.string(forKey: "sex_name_th") {
            lblEmpSex.text = ud_sex_name_th
        }
        if let ud_emp_birthday = defaults.string(forKey: "emp_birthday") {
            lblEmpBirthday.text = ud_emp_birthday
        }
        if let ud_identitycard = defaults.string(forKey: "IdentityCard") {
            lblEmpCardId.text = ud_identitycard
        }
    }
    // get user data
}
