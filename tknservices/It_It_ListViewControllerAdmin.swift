//
//  It_It_ListViewControllerAdmin.swift
//  tknservices
//
//  Created by MaI on 7/25/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class It_It_ListViewControllerAdmin: UIViewController {
    
    @IBOutlet weak var lblDateReciveJobFirst: UILabel!
    @IBOutlet weak var lblLv1: UILabel!
    @IBOutlet weak var lblLv2: UILabel!
    @IBOutlet weak var lblLv3: UILabel!
    @IBOutlet weak var lblLv4: UILabel!
    @IBOutlet weak var lblsumtime: UILabel!
    @IBOutlet weak var lblDateCloseJob: UILabel!
    @IBOutlet weak var lblStaName: UILabel!
    @IBOutlet weak var lblAdminName: UILabel!
    @IBOutlet weak var lblAdminDoingName: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblCommentAMDoing: UILabel!
    
    var u0_userre = UserRequest_detail()
    var arr_u0_userre = [UserRequest_detail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        getAllList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getAllList() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        // print(sEmpIDX)
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        //"EmpIDX_add" : sEmpIDX,
                        "URQIDX" : URQIDX
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        
        let tempUrl = String(urlSelect_DetailCloseJobList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    //return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "DateReciveJobFirst") {
                                self.u0_userre.DateReciveJobFirst = item.stringValue
                            }
                            if(key == "TimeReciveJobFirst") {
                                self.u0_userre.TimeReciveJobFirst = item.stringValue
                            }
                            if(key == "Name6") {
                                self.u0_userre.Name6 = item.stringValue
                            }
                            if(key == "Name7") {
                                self.u0_userre.Name7 = item.stringValue
                            }
                            if(key == "Name8") {
                                self.u0_userre.Name8 = item.stringValue
                            }
                            if(key == "Name9") {
                                self.u0_userre.Name9 = item.stringValue
                            }
                            if(key == "sumtime") {
                                self.u0_userre.sumtime = item.stringValue
                            }
                            
                            if(key == "StaName") {
                                self.u0_userre.StaName = item.stringValue
                            }
                            if(key == "Link_IT") {
                                self.u0_userre.Link_IT = item.stringValue
                            }
                            if(key == "AdminName") {
                                self.u0_userre.AdminName = item.stringValue
                            }
                            if(key == "AdminDoingName") {
                                self.u0_userre.AdminDoingName = item.stringValue
                            }
                            if(key == "CommentAMDoing") {
                                self.u0_userre.CommentAMDoing = item.stringValue
                            }
                            if(key == "DatecloseJob") {
                                self.u0_userre.DatecloseJob = item.stringValue
                            }
                            if(key == "TimecloseJob") {
                                self.u0_userre.TimecloseJob = item.stringValue
                            }
                        }
                        self.arr_u0_userre += [self.u0_userre]
                    } else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.u0_userre.DateReciveJobFirst = item["DateReciveJobFirst"].stringValue
                            self.u0_userre.TimeReciveJobFirst = item["TimeReciveJobFirst"].stringValue
                            self.u0_userre.Name6 = item["Name6"].stringValue
                            self.u0_userre.Name7 = item["Name7"].stringValue
                            self.u0_userre.Name8 = item["Name8"].stringValue
                            self.u0_userre.Name9 = item["Name9"].stringValue
                            self.u0_userre.sumtime = item["sumtime"].stringValue
                            self.u0_userre.StaName = item["StaName"].stringValue
                            self.u0_userre.Link_IT = item["Link_IT"].stringValue
                            self.u0_userre.AdminName = item["AdminName"].stringValue
                            self.u0_userre.AdminDoingName = item["AdminDoingName"].stringValue
                            self.u0_userre.CommentAMDoing = item["CommentAMDoing"].stringValue
                            self.u0_userre.DatecloseJob = item["DatecloseJob"].stringValue
                            self.u0_userre.TimecloseJob = item["TimecloseJob"].stringValue

                            self.arr_u0_userre += [self.u0_userre]
                        }
                    }
                    
                    self.lblDateReciveJobFirst.text = self.arr_u0_userre[0].DateReciveJobFirst + " " + self.arr_u0_userre[0].TimeReciveJobFirst
                    self.lblLv1.text = self.arr_u0_userre[0].Name6
                    self.lblLv2.text = self.arr_u0_userre[0].Name7
                    self.lblLv3.text = self.arr_u0_userre[0].Name8
                    self.lblLv4.text = self.arr_u0_userre[0].Name9
                    self.lblsumtime.text = self.arr_u0_userre[0].sumtime
                    self.lblStaName.text = self.arr_u0_userre[0].StaName
                    self.lblLink.text = self.arr_u0_userre[0].Link_IT
                    self.lblAdminName.text = self.arr_u0_userre[0].AdminName
                    self.lblAdminDoingName.text = self.arr_u0_userre[0].AdminDoingName
                    self.lblCommentAMDoing.text = self.arr_u0_userre[0].CommentAMDoing
                    self.lblDateCloseJob.text = self.arr_u0_userre[0].DatecloseJob + " " + self.arr_u0_userre[0].TimecloseJob
                }
        }
    }
}
