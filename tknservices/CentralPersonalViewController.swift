//
//  CentralPersonalViewController.swift
//  tknservices
//
//  Created by lifetofree on 3/14/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class CentralPersonalViewController: UIViewController {
    
    @IBOutlet weak var menuBoxStart: UIView!
    @IBOutlet weak var menuBoxEnd: UIView!
    
    @IBOutlet weak var btnGeneral: UIButton!
    @IBOutlet weak var btnPosition: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnHealth: UIButton!
    @IBOutlet weak var btnApprover: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    
    @IBOutlet weak var ivContentBox: UIView!
    @IBOutlet weak var cvGeneral: UIView!
    @IBOutlet weak var cvPosition: UIView!
    @IBOutlet weak var cvAddress: UIView!
    @IBOutlet weak var cvHealth: UIView!
    @IBOutlet weak var cvApprover: UIView!
    @IBOutlet weak var cvPassword: UIView!
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblEmpPos: UILabel!
    
    var activeColor: UIColor!
    var inactiveColor: UIColor = UIColor.lightGray
    var arrayIC: [String] = [ "icon_personal_general", "icon_personal_position", "icon_personal_address", "icon_personal_health", "icon_personal_approver", "icon_personal_password" ]
    
    var emp_detail = employee_detail()
    var arr_emp_detail = [employee_detail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set menu box rouned
        if #available(iOS 11.0, *) {
            // set menu box rouned
            menuBoxStart.layer.cornerRadius = 10.0
            menuBoxStart.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            menuBoxEnd.layer.cornerRadius = 10.0
            menuBoxEnd.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]

            ivContentBox.layer.cornerRadius = 10.0
            
            activeColor = UIColor(named: "color_blue_dark")
        } else {
            // Fallback on earlier versions
            activeColor = UIColor.blue
        }
        
        // change button active color
        setActiveBtn(asBtnName: btnGeneral, asNumber: 0)
        // show general and hide other container views
        closeAllCV()
        cvGeneral.isHidden = false
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // menu list select
    @IBAction func btnMenuSelected(_ sender: UIButton) {
        setActiveView(asBtnName: sender)
    }
    // menu list select
    
    // get user data
    func getMyProfile() {
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            lblEmpName.text = ud_emp_name_th
        }
        if let ud_pos_name_th = defaults.string(forKey: "pos_name_th") {
            lblEmpPos.text = ud_pos_name_th
        }
    }
    // get user data
    
    // re-use
    func closeAllCV() {
        let arrayCV: [UIView] = [ cvGeneral, cvPosition, cvAddress, cvHealth, cvApprover, cvPassword ]
        
        for cv in arrayCV {
            cv.isHidden = true
        }
    }
    
    func resetAllMenuBtn() {
        let arrayBTN: [UIButton] = [ btnGeneral, btnPosition, btnAddress, btnHealth, btnApprover, btnPassword ]
        var btnNo: Int = 0

        for btn in arrayBTN {
            btn.setBackgroundImage(UIImage(named: arrayIC[btnNo]), for: .normal)
            btn.tintColor = inactiveColor
            btn.setTitleColor(inactiveColor, for: .normal)
            btnNo += 1
        }
    }
    
    func setActiveBtn(asBtnName btnName: UIButton, asNumber btnNo: Int) {
        let origImage = UIImage(named: arrayIC[btnNo])
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        btnName.setBackgroundImage(tintedImage, for: .normal)
        btnName.tintColor = activeColor
        btnName.setTitleColor(activeColor, for: .normal)
    }
    
    func setActiveView(asBtnName btnName: UIButton) {
        resetAllMenuBtn()
        closeAllCV()
        
        switch btnName {
        case btnGeneral:
            cvGeneral.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 0)
        case btnPosition:
            cvPosition.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 1)
        case btnAddress:
            cvAddress.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 2)
        case btnHealth:
            cvHealth.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 3)
        case btnApprover:
            cvApprover.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 4)
        case btnPassword:
            cvPassword.isHidden = false
            setActiveBtn(asBtnName: btnName, asNumber: 5)
        default:
            print("error")
        }
    }
    // re-use
}
