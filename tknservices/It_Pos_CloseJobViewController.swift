//
//  It_Pos_CloseJobViewController.swift
//  tknservices
//
//  Created by lifetofree on 8/18/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Pos_CloseJobViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfLv1: UITextField!
    @IBOutlet weak var tfLv2: UITextField!
    @IBOutlet weak var tfLv3: UITextField!
    @IBOutlet weak var tfLv4: UITextField!
    @IBOutlet weak var tfCommentAdmin: UITextField!
    
    var pvLv1 = UIPickerView()
    var pvLv2 = UIPickerView()
    var pvLv3 = UIPickerView()
    var pvLv4 = UIPickerView()
    
    var Lv_detail = UserRequest_detail()
    var arr_Lv_detail = [UserRequest_detail]()
    
    var Lv_Pos_detail = POSList()
    var arr_Lv_Pos_detail = [POSList]()
    var arr_Lv_2_Pos_detail = [POSList]()
    var arr_Lv_3_Pos_detail = [POSList]()
    var arr_Lv_4_Pos_detail = [POSList]()
    
    var dataLv1: [String] = []
    var dataLv2: [String] = []
    var dataLv3: [String] = []
    var dataLv4: [String] = []
    
    var name_lv1: String = ""
    var name_lv2: String = ""
    var name_lv3: String = ""
    var name_lv4: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set picker view
        pvLv1.delegate = self
        pvLv1.dataSource = self
        pvLv1.tag = 1
        tfLv1.inputView = pvLv1
        
        pvLv2.delegate = self
        pvLv2.dataSource = self
        pvLv2.tag = 2
        tfLv2.inputView = pvLv2
        
        pvLv3.delegate = self
        pvLv3.dataSource = self
        pvLv3.tag = 3
        tfLv3.inputView = pvLv3
        
        pvLv4.delegate = self
        pvLv4.dataSource = self
        pvLv4.tag = 4
        tfLv4.inputView = pvLv4
        
        ddlLv1()
        //ddlLv2()
        //ddlLv3()
        //ddlLv4()
        
        // create tool bar for date picker
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        tfCommentAdmin.inputAccessoryView = toolBarComment
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickerComment()
    {
        tfCommentAdmin.resignFirstResponder()
    }
    func CancelPickerComment()
    {
        tfCommentAdmin.text = ""
        tfCommentAdmin.resignFirstResponder()
    }
    
    // get ddlLv1
    func ddlLv1() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxPOSList" : [
                        "POS1IDX" : 0,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelectCaseLV1POSU0 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryValue {
                            if(key == "POS1IDX") {
                                self.Lv_Pos_detail.POS1IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code1") {
                                self.Lv_Pos_detail.Name_Code1 = item.stringValue
                            }
                        }
                        self.arr_Lv_Pos_detail += [self.Lv_Pos_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayValue {
                            self.Lv_Pos_detail.POS1IDX = Int(item["POS1IDX"].stringValue)!
                            self.Lv_Pos_detail.Name_Code1 = item["Name_Code1"].stringValue
                            
                            self.arr_Lv_Pos_detail += [self.Lv_Pos_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_Pos_detail.count {
                        if(self.arr_Lv_Pos_detail[i].Name_Code1 != ""){
                            self.dataLv1 += [self.arr_Lv_Pos_detail[i].Name_Code1]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv2
    func ddlLv2() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxPOSList" : [
                        "POS1_Name" : self.name_lv1,
                        "POS1IDX" : 0
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelectCaseLV2POSU0 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_2_Pos_detail.removeAll()
        self.dataLv2.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryValue {
                            if(key == "POS2IDX") {
                                self.Lv_Pos_detail.POS2IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code2") {
                                self.Lv_Pos_detail.Name_Code2 = item.stringValue
                            }
                        }
                        self.arr_Lv_2_Pos_detail += [self.Lv_Pos_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayValue {
                            self.Lv_Pos_detail.POS2IDX = Int(item["POS2IDX"].stringValue)!
                            self.Lv_Pos_detail.Name_Code2 = item["Name_Code2"].stringValue
                            
                            self.arr_Lv_2_Pos_detail += [self.Lv_Pos_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_2_Pos_detail.count {
                        if(self.arr_Lv_2_Pos_detail[i].Name_Code2 != ""){
                            self.dataLv2 += [self.arr_Lv_2_Pos_detail[i].Name_Code2]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv3
    func ddlLv3() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxPOSList" : [
                        "POS2_Name" : self.name_lv1,
                        "POS1IDX" : 0
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelectCaseLV3POSU0 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_3_Pos_detail.removeAll()
        self.dataLv3.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryValue {
                            if(key == "POS3IDX") {
                                self.Lv_Pos_detail.POS3IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code3") {
                                self.Lv_Pos_detail.Name_Code3 = item.stringValue
                            }
                        }
                        self.arr_Lv_3_Pos_detail += [self.Lv_Pos_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayValue {
                            self.Lv_Pos_detail.POS3IDX = Int(item["POS3IDX"].stringValue)!
                            self.Lv_Pos_detail.Name_Code3 = item["Name_Code3"].stringValue
                            
                            self.arr_Lv_3_Pos_detail += [self.Lv_Pos_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_3_Pos_detail.count {
                        if(self.arr_Lv_3_Pos_detail[i].Name_Code3 != ""){
                            self.dataLv3 += [self.arr_Lv_3_Pos_detail[i].Name_Code3]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv4
    func ddlLv4() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxPOSList" : [
                        "POS3_Name" : self.name_lv1,
                        "POS1IDX" : 0
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelectCaseLV4POSU0 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_4_Pos_detail.removeAll()
        self.dataLv4.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].dictionaryValue {
                            if(key == "POS4IDX") {
                                self.Lv_Pos_detail.POS4IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code4") {
                                self.Lv_Pos_detail.Name_Code4 = item.stringValue
                            }
                        }
                        self.arr_Lv_4_Pos_detail += [self.Lv_Pos_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxPOSList"].arrayValue {
                            self.Lv_Pos_detail.POS4IDX = Int(item["POS4IDX"].stringValue)!
                            self.Lv_Pos_detail.Name_Code4 = item["Name_Code4"].stringValue
                            
                            self.arr_Lv_4_Pos_detail += [self.Lv_Pos_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_4_Pos_detail.count {
                        if(self.arr_Lv_4_Pos_detail[i].Name_Code4 != ""){
                            self.dataLv4 += [self.arr_Lv_4_Pos_detail[i].Name_Code4]
                        }
                    }
                    
                }
        }
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 2 && acidx == 1){
            staidx = 30
        }
        else{
            staidx = 29
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_POSGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get urlCloseJob_Admin
    func urlCloseJob_Admin() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "CIT1IDX" : 0,
                        "CIT2IDX" : 0,
                        "CIT3IDX" : 0,
                        "CIT4IDX" : 0,
                        "Name_Code1" : tfLv1.text!,
                        "Name_Code2" : tfLv2.text!,
                        "Name_Code3" : tfLv3.text!,
                        "Name_Code4" : tfLv4.text!,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "CommentAMDoing" : tfCommentAdmin.text!,
                        "CCAIDX" : 0,
                        "RecieveIDX" : 0,
                        "EmpIDX_add" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "StaIDX" : 1
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlPOS_CloseJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLv1.count
        }
        else if(pickerView.tag == 2) {
            //ddlLv2()
            return self.dataLv2.count
        }
        else if(pickerView.tag == 3) {
            return self.dataLv3.count
        }
        else {
            return self.dataLv4.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLv1[row]
        }
        else if(pickerView.tag == 2) {
            return self.dataLv2[row]
        }
        else if(pickerView.tag == 3) {
            return self.dataLv3[row]
        }
        else {
            return self.dataLv4[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLv1.text = self.dataLv1[row]
            self.name_lv1 = self.dataLv1[row]
            tfLv2.text = ""
            tfLv3.text = ""
            tfLv4.text = ""
            ddlLv2()
        }
        else if(pickerView.tag == 2) {
            tfLv2.text = self.dataLv2[row]
            self.name_lv2 = self.dataLv2[row]
            tfLv3.text = ""
            tfLv4.text = ""
            ddlLv3()
        }
        else if(pickerView.tag == 3) {
            tfLv3.text = self.dataLv3[row]
            self.name_lv3 = self.dataLv3[row]
            tfLv4.text = ""
            ddlLv4()
        }
        else {
            tfLv4.text = self.dataLv4[row]
        }
        self.view.endEditing(true)
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func Check_Alert(){
        let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณากรอกข้อมูลให้ครบถ้วน", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        if(tfLv1.text! == "" || tfLv2.text! == "" || tfLv3.text! == "" || tfLv4.text! == "" || tfCommentAdmin.text! == ""){
            Check_Alert()
        }
        else {
            
            urlUpdate()
            urlCloseJob_Admin()
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itPosTabBarController
            itPosTabBarController.selectedIndex = 0 // It_Pos_ListViewController
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
        itPosTabBarController.selectedIndex = 0 //
    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
        itPosTabBarController.selectedIndex = 0 //
    }
    //----- Navigation Bar Button -----//
}
