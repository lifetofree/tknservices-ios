//
//  It_Bp_ListViewControllerComment.swift
//  TKNServices
//
//  Created by lifetofree on 7/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bp_ListViewControllerComment: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tvcItBpComment: UITableView!
    @IBOutlet weak var tfComment: UITextField!
    
    var refreshControl = UIRefreshControl()
    
    var u0_userre = UserRequest_detail()
    var arr_u0_userre = [UserRequest_detail]()
    
    let textCellIdentifier = "tvcItBpComment"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //--- Tabel View required --//
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_userre.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: It_Bp_CustomCellList = tvcItBpComment.dequeueReusableCell(withIdentifier: textCellIdentifier) as! It_Bp_CustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _FullNameTH = arr_u0_userre[row].FullNameTH
        let _CreateDateUser = arr_u0_userre[row].CDate + " " + arr_u0_userre[row].CTime
        let _CommentAuto = arr_u0_userre[row].CommentAuto
        
        // load item
        cell.loadItem_Comment(FullNameTH: _FullNameTH, CommentAuto: _CommentAuto, CDate: _CreateDateUser)
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    //--- Tabel View required --//
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        setDataDetail()
        goBack()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    //----- Event Button -----//
    
    //----- Reuse -----//
    func initView() {
        tvcItBpComment.delegate = self
        tvcItBpComment.dataSource = self
        
        tfComment.delegate = self
        tfComment.returnKeyType = .done
        
        getAllList()
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // refresh control
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(It_Bi_ListViewControllerComment.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tvcItBpComment.addSubview(refreshControl)
        // refresh control
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bp_ListViewControllerComment.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bp_ListViewControllerComment.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // getAllList
    func getAllList() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        // print(sEmpIDX)
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "SysIDX_add" : 23,
                        "URQIDX" : URQIDX,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        
        let tempUrl = String(urlSelect_GvComment_List + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "FullNameTH") {
                                self.u0_userre.FullNameTH = item.stringValue
                            }
                            if(key == "CommentAuto") {
                                self.u0_userre.CommentAuto = item.stringValue
                            }
                            if(key == "CDate") {
                                self.u0_userre.CDate = item.stringValue
                            }
                            if(key == "CTime") {
                                self.u0_userre.CTime = item.stringValue
                            }
                            
                        }
                        self.arr_u0_userre += [self.u0_userre]
                    } else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.u0_userre.FullNameTH = item["FullNameTH"].stringValue
                            self.u0_userre.CommentAuto = item["CommentAuto"].stringValue
                            self.u0_userre.CDate = item["CDate"].stringValue
                            self.u0_userre.CTime = item["CTime"].stringValue
                            
                            self.arr_u0_userre += [self.u0_userre]
                        }
                    }
                    
                    // count data and reload
                    if (self.arr_u0_userre.count > 0) {
                        // set back to label view
                        self.tvcItBpComment.backgroundView = nil;
                        
                        self.tvcItBpComment.reloadData()
                    }
                    else {
                        self.tvcItBpComment.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tvcItBpComment.bounds.size.width, height: self.tvcItBpComment.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray
                        
                        // set back to label view
                        self.tvcItBpComment.backgroundView = lblEmpty;
                        // no separator
                        self.tvcItBpComment.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    // setDataDetail
    func setDataDetail() {
        if(tfComment.text! != ""){
            let value =
                [
                    "DataSupportIT" : [
                        "BoxUserRequest" : [
                            "URQIDX" : URQIDX,
                            "EmpIDX_add" : Int(sEmpIDX)!,
                            "CommentAuto" : tfComment.text!,
                            "CStatus" : 1,
                        ]
                    ]
            ]
            
            let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
            let tempUrl = String(urlInsertCommentSAP + dataIn)
            let sendUrl = URL(string: tempUrl!)!
            // set data and url
            // print(sendUrl)
            // call web service
            Alamofire.request(
                sendUrl,
                method: .get,
                parameters: nil,
                encoding: URLEncoding.default,
                headers: nil)
                .responseJSON { (responseData) -> Void in
                    if((responseData.result.value) != nil) {
                        
                    }
            }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpTabBarController
        itBpTabBarController.selectedIndex = 0 //
    }
    
    // TextField resign first responder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
    //----- Reuse -----//
}
