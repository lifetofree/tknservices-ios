//
//  Vac_ProfileViewController.swift
//  tknservices
//
//  Created by lifetofree on 4/4/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Vac_ProfileViewController: UIViewController {
    var refreshControl = UIRefreshControl()
    
    var stat_detail = vacation_stat_detail()
    var arr_stat_detail = [vacation_stat_detail]()
    
    var process_detail = vacation_process_detail()
    var arr_process_detail = [vacation_process_detail]()
    
    var vacation_limit: Double = 0
    
    var emp_start_date: String!
    var emp_probation_date: String!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var view_content: UIView!
    
    @IBOutlet weak var lbl_type1_used: UILabel!
    @IBOutlet weak var lbl_type1_all: UILabel!
    @IBOutlet weak var lbl_type2_used: UILabel!
    @IBOutlet weak var lbl_type2_all: UILabel!
    @IBOutlet weak var lbl_type3_used: UILabel!
    @IBOutlet weak var lbl_type3_all: UILabel!
    @IBOutlet weak var lbl_type4_used: UILabel!
    @IBOutlet weak var lbl_type4_all: UILabel!
    @IBOutlet weak var lbl_type5_used: UILabel!
    @IBOutlet weak var lbl_type5_all: UILabel!
    @IBOutlet weak var lbl_type6_used: UILabel!
    @IBOutlet weak var lbl_type6_all: UILabel!
    @IBOutlet weak var lbl_type11_used: UILabel!
    @IBOutlet weak var lbl_type12_used: UILabel!
    @IBOutlet weak var lbl_type13_used: UILabel!
    @IBOutlet weak var lbl_waiting: UILabel!
    @IBOutlet weak var lbl_completed: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // pull to refresh
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(Vac_ProfileViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.profileScrollView.addSubview(refreshControl)

        // get data
        self.getMyStat()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // function for event
    // get my stat
    func getMyStat() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        //        print(sEmpIDX)
        
        if let ud_emp_start_date = defaults.string(forKey: "emp_start_date") {
            emp_start_date = ud_emp_start_date
        }
        if let ud_emp_probation_date = defaults.string(forKey: "emp_probation_date") {
            emp_probation_date = ud_emp_probation_date
        }
        
        let tempUrl = String(urlGetMyStat + sEmpIDX)
        let sendUrl = URL(string: tempUrl!)!
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    if (return_code == 0) {
                        // get value
                        if swiftyJsonVar["data_vacation"]["vacation_stat_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_vacation"]["vacation_stat_list"].dictionaryValue {
                                if(key == "m0_leavetype_idx") {
                                    self.stat_detail.m0_leavetype_idx = Int(item.stringValue)!
                                }
                                if(key == "m0_leavetype_name") {
                                    self.stat_detail.m0_leavetype_name = item.stringValue
                                }
                                if(key == "stat_min") {
                                    self.stat_detail.stat_min = Double(item.stringValue)!
                                }
                            }
                            self.arr_stat_detail += [self.stat_detail]
                        }
                        else if swiftyJsonVar["data_vacation"]["vacation_stat_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_vacation"]["vacation_stat_list"].arrayValue {
                                self.stat_detail.m0_leavetype_idx = Int(item["m0_leavetype_idx"].stringValue)!
                                self.stat_detail.m0_leavetype_name = item["m0_leavetype_name"].stringValue
                                self.stat_detail.stat_min = Double(item["stat_min"].stringValue)!
                                
                                self.arr_stat_detail += [self.stat_detail]
                            }
                        }
                        
                        if swiftyJsonVar["data_vacation"]["vacation_process_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_vacation"]["vacation_process_list"].dictionaryValue {
                                if(key == "waiting") {
                                    self.process_detail.waiting = Int(item.stringValue)!
                                }
                                if(key == "completed") {
                                    self.process_detail.completed = Int(item.stringValue)!
                                }
                            }
                            self.arr_process_detail += [self.process_detail]
                        }
                        else if swiftyJsonVar["data_vacation"]["vacation_process_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_vacation"]["vacation_process_list"].arrayValue {
                                self.process_detail.waiting = Int(item["waiting"].stringValue)!
                                self.process_detail.completed = Int(item["completed"].stringValue)!
                                self.arr_process_detail += [self.process_detail]
                            }
                        }
                        
                        // set value
                        self.lbl_type1_all.text = "30"
                        self.lbl_type2_all.text = "7"
                        // self.lbl_type3_all.text = "n"
                        // set vacation limit
                        self.lbl_type3_all.text = String(calcVacation(start_date: self.emp_start_date, probation_date: self.emp_probation_date))
                        self.lbl_type4_all.text = "90"
                        self.lbl_type5_all.text = "15"
                        self.lbl_type6_all.text = "n"
                        
                        if(self.arr_stat_detail.count > 0) {
                            for (item) in self.arr_stat_detail
                            {
                                if(item.m0_leavetype_idx == 1)
                                {
                                    self.lbl_type1_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 2)
                                {
                                    self.lbl_type2_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 3)
                                {
                                    self.lbl_type3_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 4)
                                {
                                    self.lbl_type4_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 5)
                                {
                                    self.lbl_type5_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 6)
                                {
                                    self.lbl_type6_used.text = String(convertMinToHour(item.stat_min))
                                }
                                if(item.m0_leavetype_idx == 11)
                                {
                                    self.lbl_type11_used.text = String(item.stat_min)
                                }
                                if(item.m0_leavetype_idx == 12)
                                {
                                    self.lbl_type12_used.text = String(item.stat_min)
                                }
                                if(item.m0_leavetype_idx == 13)
                                {
                                    self.lbl_type13_used.text = String(item.stat_min)
                                }
                            }
                        }
                        
                        self.lbl_waiting.text = String(self.arr_process_detail[0].waiting)
                        self.lbl_completed.text = String(self.arr_process_detail[0].completed)
                        // set value
                    } else {
                        self.alertError(return_msg: return_msg)
                    }
                    // print(responseData.result.value!)
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getMyStat()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    // function for event
    
    // go to central
    @IBAction func btnCentral(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    // go to central
}
