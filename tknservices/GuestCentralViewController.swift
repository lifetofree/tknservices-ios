//
//  GuestCentralViewController.swift
//  tknservices
//
//  Created by lifetofree on 12/21/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class GuestCentralViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        //custom navbar background
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "bg_title")!.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- Guest -----//
    @IBAction func btnGuestNews(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        appDelegate.window?.rootViewController = viewController
    }

    @IBAction func btnGuestMaps(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let infoMapsViewController = storyBoard.instantiateViewController(withIdentifier: "InfoMapsViewController") as! InfoMapsViewController

        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        appDelegate.window?.rootViewController = infoMapsViewController
    }

    @IBAction func btnGuestSet(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let infoSetViewController = storyBoard.instantiateViewController(withIdentifier: "InfoSetViewController") as! InfoSetViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = infoSetViewController
    }

    @IBAction func btnGuestAbout(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let infoAboutViewController = storyBoard.instantiateViewController(withIdentifier: "InfoAboutViewController") as! InfoAboutViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = infoAboutViewController
    }
    //----- Guest -----//
    
    
    //----- Navigation Bar Button -----//
    @IBAction func btnLogout(_ sender: UIBarButtonItem) {
        defaults.removeObject(forKey: "emp_idx")

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        appDelegate.window?.rootViewController = dataViewController
    }
    
    // go to info
    @IBAction func btnInfo(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let infoController = storyBoard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = infoController
    }
    // go to info
    //----- Navigation Bar Button -----//
    
    
}
