//
//  Vac_CreateViewController.swift
//  tknservices
//
//  Created by lifetofree on 4/4/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Vac_CreateViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var val_leavetype = vacation_leavetype()
    var arr_leavetype_detail = [vacation_leavetype]()
    var val_shfittime = vacation_shifttime()
    var arr_shfittime_detail = [vacation_shifttime]()
    
    var dataLeaveType: [String] = []
    var dataShiftName: [String] = []
    var dataDocType = ["รายการทั่วไป","ใบคำร้อง(ยกเลิก)","ใบคำร้อง(ย้อนหลัง)"]
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var tfDocType: UITextField!
    @IBOutlet weak var tfLeaveType: UITextField!
    @IBOutlet weak var tfShiftName: UITextField!
    @IBOutlet weak var tfLeaveStart: UITextField!
    @IBOutlet weak var tfLeaveEnd: UITextField!
    @IBOutlet weak var tfLeaveComment: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    
    var pvDocType = UIPickerView()
    var pvLeaveType = UIPickerView()
    var pvShiftName = UIPickerView()
    var dpLeaveStart = UIDatePicker()
    var dpLeaveEnd = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()

        // get picker view data
        ddlLeaveType()
        ddlShfitTime()
        // get picker view data

        // set picker view
        pvDocType.delegate = self
        pvDocType.dataSource = self
        pvDocType.tag = 3
        tfDocType.text = dataDocType[0]
        tfDocType.inputView = pvDocType

        pvLeaveType.delegate = self
        pvLeaveType.dataSource = self
        pvLeaveType.tag = 1
        tfLeaveType.inputView = pvLeaveType

        pvShiftName.delegate = self
        pvShiftName.dataSource = self
        pvShiftName.tag = 2
        tfShiftName.inputView = pvShiftName
        // set picker view

        // set date picker
        tfLeaveStart.inputView = dpLeaveStart
        tfLeaveEnd.inputView = dpLeaveEnd
        // set date picker
        
        // set detail
        setDetail()

        // create tool bar for date picker
        let toolBarLeaveStart = UIToolbar()
        toolBarLeaveStart.barStyle = .default
        toolBarLeaveStart.isTranslucent = true
        toolBarLeaveStart.sizeToFit()

        let toolBarLeaveEnd = UIToolbar()
        toolBarLeaveEnd.barStyle = .default
        toolBarLeaveEnd.isTranslucent = true
        toolBarLeaveEnd.sizeToFit()

        let doneButtonLeaveStart = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveStart))
        let spaceButtonLeaveStart = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonLeaveStart = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveStart))

        let doneButtonLeaveEnd = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveEnd))
        let spaceButtonLeaveEnd = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonLeaveEnd = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveEnd))

        toolBarLeaveStart.setItems([cancelButtonLeaveStart, spaceButtonLeaveStart, doneButtonLeaveStart], animated: false)
        toolBarLeaveStart.isUserInteractionEnabled = true

        toolBarLeaveEnd.setItems([cancelButtonLeaveEnd, spaceButtonLeaveEnd, doneButtonLeaveEnd
            ], animated: false)
        toolBarLeaveEnd.isUserInteractionEnabled = true

        // set tool bar w/ button
        tfLeaveStart.inputAccessoryView = toolBarLeaveStart
        tfLeaveEnd.inputAccessoryView = toolBarLeaveEnd
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnClear.roundButton(rad: 5.0, buttonColor: colorBlueButton, borderColor: colorBlueBorder, borderWidth: 1.0)
        // set button

        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(Vac_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Vac_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // function for event
    // set detail
    func setDetail() {
        setInitDate()
    }
    
    // set init date
    func setInitDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let convertedDateString = dateFormatter.string(from: Date())
        lblCreateDate.text = convertedDateString
        
        dateFormatter.dateFormat = "dd/MM/yyyy 08:30"
        let convertedDateStartString = dateFormatter.string(from: Date())
        tfLeaveStart.text = convertedDateStartString
//        dpLeaveStart.setDate(dateFormatter.date(from: convertedDateStartString)!, animated: false)
        
        dateFormatter.dateFormat = "dd/MM/yyyy 17:30"
        let convertedDateEndString = dateFormatter.string(from: Date())
        tfLeaveEnd.text = convertedDateEndString
//        dpLeaveEnd.setDate(dateFormatter.date(from: convertedDateEndString)!, animated: false)
    }
    
    // set init text
    func setInitText() {
        tfDocType.text = dataDocType[0]
        tfLeaveType.text = ""
        tfShiftName.text = ""
        tfLeaveComment.text = ""
    }
    
    // get leave type
    func ddlLeaveType() {
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : 0,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetLeaveTypeList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].dictionaryValue {
                            if(key == "midx") {
                                self.val_leavetype.midx = Int(item.stringValue)!
                            }
                            if(key == "type_name") {
                                self.val_leavetype.type_name = item.stringValue
                            }
                        }
                        self.arr_leavetype_detail += [self.val_leavetype]
                    } else if swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].arrayValue {
                            self.val_leavetype.midx = Int(item["midx"].stringValue)!
                            self.val_leavetype.type_name = item["type_name"].stringValue
                            
                            self.arr_leavetype_detail += [self.val_leavetype]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_leavetype_detail.count {
                        self.dataLeaveType += [self.arr_leavetype_detail[i].type_name]
                    }
                }
        }
    }
    
    //get shift time
    func ddlShfitTime() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            iEmpIDX = Int(ud_emp_idx as! String)!
        }
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : 0,
                        "emp_idx" : iEmpIDX
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetShfitTimeList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].dictionaryValue {
                            if(key == "midx") {
                                self.val_shfittime.midx = Int(item.stringValue)!
                            }
                            if(key == "TypeWork") {
                                self.val_shfittime.TypeWork = item.stringValue
                            }
                        }
                        self.arr_shfittime_detail += [self.val_shfittime]
                    } else if swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].arrayValue {
                            self.val_shfittime.midx = Int(item["midx"].stringValue)!
                            self.val_shfittime.TypeWork = item["TypeWork"].stringValue
                            
                            self.arr_shfittime_detail += [self.val_shfittime]
                            
                        }
                        
                    }
                    
                    for i in 0 ..< self.arr_shfittime_detail.count {
                        self.dataShiftName += [self.arr_shfittime_detail[i].TypeWork]
                    }
                }
        }
    }
    
    //--- required --//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLeaveType.count
        } else if(pickerView.tag == 2) {
            return self.dataShiftName.count
        } else {
            return dataDocType.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLeaveType[row]
        } else if(pickerView.tag == 2) {
            return self.dataShiftName[row]
        } else {
            return dataDocType[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLeaveType.text = self.dataLeaveType[row]
        } else if(pickerView.tag == 2) {
            tfShiftName.text = self.dataShiftName[row]
        } else {
            tfDocType.text = dataDocType[row]
        }
        self.view.endEditing(true)
    }
    
    // click done button
    func donePickerLeaveStart()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let convertedDateStartString = dateFormatter.string(from: dpLeaveStart.date as Date)
        tfLeaveStart.text = convertedDateStartString
        
        tfLeaveStart.resignFirstResponder()
    }
    
    func donePickerLeaveEnd()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let convertedDateEndString = dateFormatter.string(from: dpLeaveEnd.date as Date)
        tfLeaveEnd.text = convertedDateEndString
        
        tfLeaveEnd.resignFirstResponder()
    }
    
    // go to create list
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vacTabBarController = storyBoard.instantiateViewController(withIdentifier: "Vac_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = vacTabBarController
        vacTabBarController.selectedIndex = 0 // Vac_ListViewController
    }
    
    @IBAction func btnClearData(_ sender: UIButton) {
        setInitDate()
        setInitText()
    }
    
    @IBAction func btnSaveData(_ sender: UIButton) {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            iEmpIDX = Int(ud_emp_idx as! String)!
        }
        
        // set document type
        var type_id = 0
        if(tfDocType.text == "รายการทั่วไป") {
            type_id = 1
        } else if(tfDocType.text == "ใบคำร้อง(ยกเลิก)") {
            type_id = 4
        } else {
            type_id = 5
        }
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "m0_node_idx" : 2,
                        "m0_actor_idx" : 2,
                        "emp_idx" : iEmpIDX,
                        
                        "m0_leavetype_name" : tfLeaveType.text!,
                        "u0_emp_shift_name" : tfShiftName.text!,
                        "u0_leave_start" : tfLeaveStart.text!,
                        "u0_leave_end" : tfLeaveEnd.text!,
                        "u0_leave_comment" : tfLeaveComment.text!,
                        "type_list" : type_id,
                        
                    ],
                    "u1_document_list" : [
                        "action_emp_idx" : iEmpIDX,
                        "action_node_idx" : 1, // node for approve
                        "u1_approve_status" : 0// ใช้
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetSaveList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                }
        }
        
        self.goToList()
    }
    
    // function for event
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
    
    // go to central
    @IBAction func btnCentral(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    // go to central
}
