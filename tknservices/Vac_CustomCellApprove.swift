//
//  Vac_CustomCellApprove.swift
//  tknservices
//
//  Created by lifetofree on 4/6/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class Vac_CustomCellApprove: UITableViewCell {
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblVacType: UILabel!
    @IBOutlet weak var lblVacTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(emp_name: String, vac_type: String, vac_time: String) {
        lblEmpName.text = emp_name
        lblVacType.text = vac_type
        lblVacTime.text = vac_time
    }
    
}
