//
//  CustomFunction.swift
//  tknservices
//
//  Created by lifetofree on 3/31/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import Foundation
import SwiftyJSON
import CryptoSwift

// convert data for post form
func convertDataToSendForm(_ _valIn : [String : AnyObject]) -> String {
    let _tempData =
        String((((String(describing: JSON(_valIn))
            .addingPercentEncoding(withAllowedCharacters: .urlUserAllowed)?
            .replacingOccurrences(of: "%20", with: "%20"))!
            .replacingOccurrences(of: "%0A", with: ""))
            .replacingOccurrences(of: ",", with: "%2C")))
    
    return _tempData!
}
// convert data for post form

// convert to MD5
func convertToMd5(_ _valIn : String) -> String {
    return _valIn.md5()
}
// convert to MD5

// convert min to hour
func convertMinToHour(_ _valIn : Double) -> Double {
    return (_valIn/(60*8)).roundTo(places: 2)
}
// convert min to hour

// calculate vacation limit
func calcVacation(start_date: String, probation_date: String) -> Double {
    var vacation_limit: Double = 0
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    dateFormatter.locale = Locale(identifier: "en_US")
    
    let _currentDate = dateFormatter.string(from: Date())
    let dStartDate = dateFormatter.date(from: start_date)
    let dProbationDate = dateFormatter.date(from: probation_date)
    let dCurrentDate = dateFormatter.date(from: _currentDate)
    let userCalendar = Calendar.current
    
    if(dProbationDate! > dStartDate!) {
        let iInterval = userCalendar.dateComponents([.month], from: dStartDate!, to: dCurrentDate!)
        // check condition
        let startMonth = userCalendar.component(.month, from: dStartDate!)
        let startYear = userCalendar.component(.year, from: dStartDate!)
        let currentYear = userCalendar.component(.year, from: dCurrentDate!)
        if(iInterval.month! < 12 && currentYear == startYear + 1) {
            vacation_limit = Double(Double(6)-Double((Double(startMonth - 1) * 0.5)))
        }
        else if(iInterval.month! >= 12 && iInterval.month! < 36) {
            vacation_limit = 6
        }
        else if(iInterval.month! >= 36 && iInterval.month! < 48) {
            vacation_limit = 7
        }
        else if(iInterval.month! >= 48 && iInterval.month! < 60) {
            vacation_limit = 8
        }
        else if(iInterval.month! >= 60) {
            vacation_limit = 9
        }
        else {
            vacation_limit = 0
        }
    }
    
    return (vacation_limit).roundTo(places: 1)
}
// calculate vacation limit
