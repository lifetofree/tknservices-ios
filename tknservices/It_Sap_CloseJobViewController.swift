//
//  It_Sap_CloseJobViewController.swift
//  tknservices
//
//  Created by lifetofree on 8/22/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Sap_CloseJobViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfLv1: UITextField!
    @IBOutlet weak var tfLv2: UITextField!
    @IBOutlet weak var tfLv3: UITextField!
    @IBOutlet weak var tfLv4: UITextField!
    @IBOutlet weak var tfLv5: UITextField!
    @IBOutlet weak var tfPriority_name: UITextField!
    @IBOutlet weak var tfManHours: UITextField!
    @IBOutlet weak var lblSapmsg: UITextField!
    @IBOutlet weak var lblLink: UITextField!
    @IBOutlet weak var lblCommentAMDoing: UITextField!
    
    var pvLv1 = UIPickerView()
    var pvLv2 = UIPickerView()
    var pvLv3 = UIPickerView()
    var pvLv4 = UIPickerView()
    var pvLv5 = UIPickerView()
    
    var Lv_detail = UserRequest_detail()
    var arr_Lv_detail = [UserRequest_detail]()
    var arr_Lv_2_detail = [UserRequest_detail]()
    var arr_Lv_3_detail = [UserRequest_detail]()
    var arr_Lv_4_detail = [UserRequest_detail]()
    var arr_Lv_5_detail = [UserRequest_detail]()
    
    var dataLv1: [String] = []
    var dataLv2: [String] = []
    var dataLv3: [String] = []
    var dataLv4: [String] = []
    var dataLv5: [String] = []
    
    var name_lv1: String = ""
    var name_lv2: String = ""
    var name_lv3: String = ""
    var name_lv4: String = ""
    var PIDX_Add: Int = 0
    
    var dataSystemClose = ["ด่วน","ไม่ด่วน","ปานกลาง"]
    var pvClose = UIPickerView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set picker view
        pvLv1.delegate = self
        pvLv1.dataSource = self
        pvLv1.tag = 1
        tfLv1.inputView = pvLv1
        
        pvLv2.delegate = self
        pvLv2.dataSource = self
        pvLv2.tag = 2
        tfLv2.inputView = pvLv2
        
        pvLv3.delegate = self
        pvLv3.dataSource = self
        pvLv3.tag = 3
        tfLv3.inputView = pvLv3
        
        pvLv4.delegate = self
        pvLv4.dataSource = self
        pvLv4.tag = 4
        tfLv4.inputView = pvLv4
        
        pvLv5.delegate = self
        pvLv5.dataSource = self
        pvLv5.tag = 5
        tfLv5.inputView = pvLv5
        
        // set picker view
        pvClose.delegate = self
        pvClose.dataSource = self
        pvClose.tag = 6
        tfPriority_name.inputView = pvClose
        
        ddlLv1()

        // create tool bar for date picker
        let toolBarManHours = UIToolbar()
        toolBarManHours.barStyle = .default
        toolBarManHours.isTranslucent = true
        toolBarManHours.sizeToFit()
        
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        let toolBarlblSapmsg = UIToolbar()
        toolBarlblSapmsg.barStyle = .default
        toolBarlblSapmsg.isTranslucent = true
        toolBarlblSapmsg.sizeToFit()
        
        let toolBarLink = UIToolbar()
        toolBarLink.barStyle = .default
        toolBarLink.isTranslucent = true
        toolBarLink.sizeToFit()
        
        let doneButtonManHours = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerManHours))
        let spaceButtonManHours = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonManHours = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerManHours))
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        let doneButtonSapmsg = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerSapmsg))
        let spaceButtonSapmsg = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonSapmsg = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerSapmsg))
        
        let doneButtonLink = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLink))
        let spaceButtonLink = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonLink = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerLink))
        
        toolBarManHours.setItems([cancelButtonManHours,spaceButtonManHours,doneButtonManHours], animated: false)
        toolBarManHours.isUserInteractionEnabled = true
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        toolBarlblSapmsg.setItems([cancelButtonSapmsg,spaceButtonSapmsg, doneButtonSapmsg], animated: false)
        toolBarlblSapmsg.isUserInteractionEnabled = true
        
        toolBarLink.setItems([cancelButtonLink,spaceButtonLink, doneButtonLink], animated: false)
        toolBarLink.isUserInteractionEnabled = true
        
        tfManHours.inputAccessoryView = toolBarManHours
        lblCommentAMDoing.inputAccessoryView = toolBarComment
        lblSapmsg.inputAccessoryView = toolBarlblSapmsg
        lblLink.inputAccessoryView = toolBarLink
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickerManHours()
    {
        tfManHours.resignFirstResponder()
    }
    func CancelPickerManHours()
    {
        tfManHours.text = ""
        tfManHours.resignFirstResponder()
    }
    
    func donePickerComment()
    {
        lblCommentAMDoing.resignFirstResponder()
    }
    func CancelPickerComment()
    {
        lblCommentAMDoing.text = ""
        lblCommentAMDoing.resignFirstResponder()
    }
    
    func donePickerSapmsg()
    {
        lblSapmsg.resignFirstResponder()
    }
    func CancelPickerSapmsg()
    {
        lblSapmsg.text = ""
        lblSapmsg.resignFirstResponder()
    }
    
    func donePickerLink()
    {
        lblLink.resignFirstResponder()
    }
    func CancelPickerLink()
    {
        lblLink.text = ""
        lblLink.resignFirstResponder()
    }
    
    // get ddlLv1
    func ddlLv1() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 0,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV1 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS1IDX") {
                                self.Lv_detail.MS1IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code1") {
                                self.Lv_detail.Name_Code1 = item.stringValue
                            }
                        }
                        self.arr_Lv_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS1IDX = Int(item["MS1IDX"].stringValue)!
                            self.Lv_detail.Name_Code1 = item["Name_Code1"].stringValue
                            
                            self.arr_Lv_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_detail.count {
                        if(self.arr_Lv_detail[i].Name_Code1 != ""){
                            self.dataLv1 += [self.arr_Lv_detail[i].Name_Code1]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv2
    func ddlLv2() {

        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV2 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_2_detail.removeAll()
        self.dataLv2.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS2IDX") {
                                self.Lv_detail.MS2IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code2") {
                                self.Lv_detail.Name_Code2 = item.stringValue
                            }
                        }
                        self.arr_Lv_2_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS2IDX = Int(item["MS2IDX"].stringValue)!
                            self.Lv_detail.Name_Code2 = item["Name_Code2"].stringValue
                            
                            self.arr_Lv_2_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_2_detail.count {
                        if(self.arr_Lv_2_detail[i].Name_Code2 != ""){
                            self.dataLv2 += [self.arr_Lv_2_detail[i].Name_Code2]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv3
    func ddlLv3() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV3 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_3_detail.removeAll()
        self.dataLv3.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS3IDX") {
                                self.Lv_detail.MS3IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code3") {
                                self.Lv_detail.Name_Code3 = item.stringValue
                            }
                        }
                        self.arr_Lv_3_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS3IDX = Int(item["MS3IDX"].stringValue)!
                            self.Lv_detail.Name_Code3 = item["Name_Code3"].stringValue
                            
                            self.arr_Lv_3_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_3_detail.count {
                        if(self.arr_Lv_3_detail[i].Name_Code3 != ""){
                            self.dataLv3 += [self.arr_Lv_3_detail[i].Name_Code3]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv4
    func ddlLv4() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV4 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_4_detail.removeAll()
        self.dataLv4.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS4IDX") {
                                self.Lv_detail.MS4IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code4") {
                                self.Lv_detail.Name_Code4 = item.stringValue
                            }
                        }
                        self.arr_Lv_4_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS4IDX = Int(item["MS4IDX"].stringValue)!
                            self.Lv_detail.Name_Code4 = item["Name_Code4"].stringValue
                            
                            self.arr_Lv_4_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_4_detail.count {
                        if(self.arr_Lv_4_detail[i].Name_Code4 != ""){
                            self.dataLv4 += [self.arr_Lv_4_detail[i].Name_Code4]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv5
    func ddlLv5() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV5 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS5IDX") {
                                self.Lv_detail.MS5IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code5") {
                                self.Lv_detail.Name_Code5 = item.stringValue
                            }
                        }
                        self.arr_Lv_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS5IDX = Int(item["MS5IDX"].stringValue)!
                            self.Lv_detail.Name_Code5 = item["Name_Code5"].stringValue
                            
                            self.arr_Lv_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_detail.count {
                        if(self.arr_Lv_detail[i].Name_Code5 != ""){
                            self.dataLv5 += [self.arr_Lv_detail[i].Name_Code5]
                        }
                    }
                    
                }
        }
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 2 && acidx == 1){
            if(approve == 5){
                staidx = 24
            }
            else{
                staidx = 23
            }
        }
        
        if(tfPriority_name.text! == "ด่วน"){
            PIDX_Add = 1
        }
        else if(tfPriority_name.text! == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else if(tfPriority_name.text! == "ปานกลาง"){
            PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                        "PIDX_Add" : PIDX_Add,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_SapGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get urlCloseJob_Admin
    func urlCloseJob_Admin() {
        
        if(unidx == 4 && acidx == 3){
            staidx = 22
        }
        
        if(tfPriority_name.text! == "ด่วน"){
            PIDX_Add = 1
        }
        else if(tfPriority_name.text! == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else{
            PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "MS1IDX" : 0,
                        "MS2IDX" : 0,
                        "MS3IDX" : 0,
                        "MS4IDX" : 0,
                        "MS5IDX" : 0,
                        "Name_Code1" : tfLv1.text!,
                        "Name_Code2" : tfLv2.text!,
                        "Name_Code3" : tfLv3.text!,
                        "Name_Code4" : tfLv4.text!,
                        "Name_Code5" : tfLv5.text!,
                        "PIDX_Add" : PIDX_Add,
                        "ManHours" : tfManHours.text!,
                        "Link" : lblLink.text!,
                        "SapMsg" : lblSapmsg.text!,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "CommentAMDoing" : lblCommentAMDoing.text!,
                        "CCAIDX" : 0,
                        "RecieveIDX" : 0,
                        "EmpIDX_add" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "StaIDX" : 1,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSap_CloseJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLv1.count
        }
        else if(pickerView.tag == 2) {
            return self.dataLv2.count
        }
        else if(pickerView.tag == 3) {
            return self.dataLv3.count
        }
        else if(pickerView.tag == 4) {
            return self.dataLv4.count
        }
        else if(pickerView.tag == 5) {
            return self.dataLv5.count
        }
        else{
            return self.dataSystemClose.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLv1[row]
        }
        else if(pickerView.tag == 2) {
            return self.dataLv2[row]
        }
        else if(pickerView.tag == 3) {
            return self.dataLv3[row]
        }
        else if(pickerView.tag == 4) {
            return self.dataLv4[row]
        }
        else if(pickerView.tag == 5) {
            return self.dataLv5[row]
        }
        else{
            return self.dataSystemClose[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLv1.text = self.dataLv1[row]
            self.name_lv1 = self.dataLv1[row]
            tfLv2.text = ""
            tfLv3.text = ""
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv2()
        }
        else if(pickerView.tag == 2) {
            tfLv2.text = self.dataLv2[row]
            self.name_lv2 = self.dataLv2[row]
            tfLv3.text = ""
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv3()
        }
        else if(pickerView.tag == 3) {
            tfLv3.text = self.dataLv3[row]
            self.name_lv3 = self.dataLv3[row]
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv4()
        }
        else if(pickerView.tag == 4) {
            tfLv4.text = self.dataLv4[row]
            self.name_lv4 = self.dataLv4[row]
            tfLv5.text = ""
            ddlLv5()
        }
        else if(pickerView.tag == 5) {
            tfLv5.text = self.dataLv5[row]
        }
        else{
            tfPriority_name.text = dataSystemClose[row]
        }
        
        self.view.endEditing(true)
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func Check_Alert(){
        let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณากรอกข้อมูลให้ครบถ้วน", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        if(tfLv1.text! == "" || tfLv2.text! == "" || tfLv3.text! == "" || tfLv4.text! == "" || tfLv5.text! == "" || tfPriority_name.text! == "" || tfManHours.text! == "" || lblSapmsg.text! == "" || lblLink.text! == "" || lblCommentAMDoing.text! == ""){
            Check_Alert()
        }
        else{
            urlUpdate()
            urlCloseJob_Admin()
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itSapTabBarController
            itSapTabBarController.selectedIndex = 0 // It_Sap_ListViewController
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 //
        
    }
    //----- Navigation Bar Button -----//
}
