//
//  DataStruct.swift
//  tknservices
//
//  Created by lifetofree on 3/31/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import Foundation

// employee data
struct employee_detail {
    var emp_idx: Int = 0
    var emp_code: String = ""
    var emp_password: String = ""
    var emp_type_idx: Int = 0
    var emp_type_name: String = ""
    
    var rpos_idx: Int = 0
    var pos_name_th: String = ""
    var pos_name_en: String = ""
    
    var rsec_idx: Int = 0
    var sec_name_th: String = ""
    var sec_name_en: String = ""
    
    var rdept_idx: Int = 0
    var dept_name_th: String = ""
    var dept_name_en: String = ""
    
    var org_idx: Int = 0
    var org_name_th: String = ""
    var org_name_en: String = ""
    
    var jobgrade_idx: Int = 0
    var jobgrade_level: Int = 0
    
    var costcenter_idx: Int = 0
    var costcenter_no: String = ""
    
    var sex_idx: Int = 0
    var sex_name_th: String = ""
    var sex_name_en: String = ""
    
    var prefix_idx: Int = 0
    var prefix_name_th: String = ""
    var prefix_name_en: String = ""
    
    var emp_firstname_th: String = ""
    var emp_lastname_th: String = ""
    var emp_nickname_th: String = ""
    var emp_name_th: String = ""
    
    var emp_firstname_en: String = ""
    var emp_lastname_en: String = ""
    var emp_nickname_en: String = ""
    var emp_name_en: String = ""
    
    var emp_phone_no: String = ""
    var emp_mobile_no: String = ""
    var emp_email: String = ""
    var emp_birthday: String = ""
    
    var nat_idx: Int = 0
    var nat_name: String = ""
    var race_idx: Int = 0
    var race_name: String = ""
    var rel_idx: Int = 0
    var rel_name_th: String = ""
    var rel_name_en: String = ""
    
    var emp_address: String = ""
    var dist_idx: Int = 0
    var dist_name: String = ""
    var amp_idx: Int = 0
    var amp_name: String = ""
    var prov_idx: Int = 0
    var prov_name: String = ""
    var post_code: String = ""
    var country_idx: Int = 0
    var country_name: String = ""
    
    var married_status: Int = 0
    var married_name: String = ""
    
    var inhabit_status: Int = 0
    
    var emp_start_date: String = ""
    var emp_resign_date: String = ""
    
    var emp_status: Int = 0
    
    var emp_createdate: String = ""
    var emp_updatedate: String = ""
    
    var emp_probation_status: Int = 0
    var emp_probation_date: String = ""
    
    var emp_idx_approve1: Int = 0
    var emp_approve1: String = ""
    var emp_idx_approve2: Int = 0
    var emp_approve2: String = ""
    
    var new_password: String = ""
    var type_reset: Int = 0
    
    var ip_address: String = ""
    
    var Height: Double = 0
    var Weight: Double = 0
    var Scar: String = ""
    var BTypeIDX: Int = 0
    var BGName: String = ""
    var BRHIDX: Int = 0
    var Allergy: String = ""
    var CDisease: String = ""
    var Operate: String = ""
    var EmerContact: String = ""
    
    var IdentityCard: String = ""
    var IssuedAt: String = ""
    var IDateExpired: String = ""
    
    var SocStatus: Int = 0
    var SocNo: String = ""
    var SocHosIDX: Int = 0
    var SocHosName: String = ""
    var SocDateExpired: String = ""
    
    var TaxID: String = ""
    var TIssuedAt: String = ""
    
    var MilIDX: Int = 0
    var MilName: String = ""
    
    var OrgNameEN: String = ""
    var LocName: String = ""
    var r0idx: Int = 0
}

// employee M0_VersionMobile
struct Version_Mobile_Detail {
    var VersionCode: String = ""
    var VersionName: String = ""
    var UrlUpdate: String = ""
    var Status_System: Int = 0
}

// vacation u0_document data
struct u0_document_detail {
    var u0_document_idx: Int = 0
    var m0_leavetype_idx: Int = 0
    var m0_leavetype_name: String = ""
    var u0_leave_start: String = ""
    var u0_leave_end: String = ""
    var u0_leave_calc: Float = 0
    var u0_leave_comment: String = ""
    var u0_emp_shift: Int = 0
    var u0_emp_shift_name: String = ""
    var m0_node_idx: Int = 0
    var m0_actor_idx: Int = 0
    var u0_leave_status: Int = 0
    var u0_status_name: String = ""
    
    var emp_idx: Int = 0
    var emp_name: String = ""
    
    var type_list: Int = 0
}

// vacation vacation stat
struct vacation_stat_detail {
    var m0_leavetype_idx: Int = 0
    var m0_leavetype_name: String = ""
    var stat_min: Double = 0
}

// vacation vacation process
struct vacation_process_detail {
    var waiting: Int = 0
    var completed: Int = 0
}

// vacation vacation leave type
struct vacation_leavetype {
    var midx: Int = 0
    var type_name: String = ""
}

// vacation vacation shift time
struct vacation_shifttime {
    var midx: Int = 0
    var TypeWork: String = ""
}

// news u0_news data
struct news_u0_news_detail {
    var uidx: Int = 0
    var news_title: String = ""
    var news_detail: String = ""
    var news_author: String = ""
    var news_status: String = ""
    var create_date: String = ""
    var update_date: String = ""
}

//---------IT----------//
struct UserRequest_detail {
    var ReturnMsg: String = ""
    var ReturnCode: String = ""
    
    var RemoteIDX: Int = 0
    var RemoteName: String = ""
    var FullNameTH: String = ""
    var EmpIDX: Int = 0
    var DeviceIDX: Int = 0
    var u0_code: String = ""
    var DeviceETC: String = ""
    
    var URQIDX: Int = 0
    var DocCode: String = ""
    var SysIDX: Int = 0
    var node_status: Int = 0
    var uidx: Int = 0
    var SysName: String = ""
    var NCEmpIDX: Int = 0
    var CreateDateUser: String = ""
    var TimeCreateJob: String = ""
    var EmpName: String = ""
    var LocName: String = ""
    var RDeptName: String = ""
    var detailUser: String = ""
    var AdminName: String = ""
    var AdminDoingName: String = ""
    var DatecloseJob: String = ""
    var TimecloseJob: String = ""
    var CommentAMDoing: String = ""
    var StaIDX: Int = 0
    var MS1IDX: Int = 0
    var MS2IDX: Int = 0
    var MS3IDX: Int = 0
    var MS4IDX: Int = 0
    var MS5IDX: Int = 0
    var AdminDoingIDX: Int = 0
    var StaName: String = ""
    var Statusans: String = ""
    var CStatus: Int = 0
    var UserIDRemote: String = ""
    var PasswordRemote: String = ""
    var Comment: String = ""
    var TelETC: String = ""
    var MobileNo: String = ""
    
    //// Close Job /////
    var DateReciveJobFirst: String = ""
    var TimeReciveJobFirst: String = ""
    var Name1: String = "" //LV1
    var Name2: String = "" //LV2
    var Name3: String = "" //LV3
    var Name4: String = "" //LV4
    var Name5: String = "" //LV5
    var Name6: String = "" //LV6
    var Name7: String = "" //LV7
    var Name8: String = "" //LV8
    var Name9: String = "" //LV9
    var Name10: String = "" //LV10
    var Name11: String = "" //LV11
    var Name12: String = "" //LV12
    var Name13: String = "" //LV13
    var nonkpi: String = ""
    var alltime: String = ""
    var Priority_name: String = "" //ลำดับความสำคัญ
    var ManHours: Int = 0
    var sumtime: String = "" //Downtime
    var Link: String = ""
    var Link_IT: String = ""
    var SapMsg: String = ""
    
    var CMIDX: Int = 0
    var CommentAuto: String = ""
    var CDate: String = ""
    var CTime: String = ""
    
    var unidx: Int = 0
    var acidx: Int = 0
    var staidx: Int = 0
    var RDeptIDX: Int = 0
    
    var Name_Code1: String = ""
    var CIT1IDX: Int = 0
    var Name_Code2: String = ""
    var CIT2IDX: Int = 0
    var Name_Code3: String = ""
    var CIT3IDX: Int = 0
    var Name_Code4: String = ""
    var CIT4IDX: Int = 0
    var OrgIDX: Int = 0
    var Name_Code5: String = ""
    
    var POS1IDX: Int = 0
    var POS2IDX: Int = 0
    var POS3IDX: Int = 0
    var POS4IDX: Int = 0

    var UserLogonName: String = ""
    var TransactionCode: String = ""
    var emp_name_th: String = ""
}

struct POSList {

    var POS1IDX: Int = 0
    var POS2IDX: Int = 0
    var POS3IDX: Int = 0
    var POS4IDX: Int = 0
    
    var POS1_Name: String = ""
    var POS2_Name: String = ""
    var POS3_Name: String = ""
    var POS4_Name: String = ""
    
    var POS1_Code: String = ""
    var POS2_Code: String = ""
    var POS3_Code: String = ""
    var POS4_Code: String = ""
    
    var CEmpIDX: Int = 0
    var POS1Status: Int = 0
    var POS2Status: Int = 0
    var POS3Status: Int = 0
    var POS4Status: Int = 0
    
    var POSStatusDetail: String = ""
    var POSIDX: Int = 0
    var POSStatus: Int = 0
    var Name_Code1: String = ""
    var Name_Code2: String = ""
    var Name_Code3: String = ""
    var Name_Code4: String = ""
}

struct employee_member {
    var Member_EmpUser: String = ""
    var Member_EmpPassword: String = ""
    var Member_EmpEmail: String = ""
}

struct chr_u0document_detail {
    var u0idx_add: Int = 0
    var u0idx_check: Int = 0
    var staidx: Int = 0
    var unidx: Int = 0
    var Sysidx: Int = 0
    var md0idx: Int = 0
    var org_idx: Int = 0
    var tidx: Int = 0
    var mtidx: Int = 0
    var CEmpidx: Int = 0
    var doc_decision: Int = 0
    var SysSapIDX: Int = 0
    var System_name: String = ""
    var NEmpidx1: String = ""
    var NEmpidx2: String = ""
    var NEmpidx3: String = ""
    var NEmpidxAbout: String = ""
    var topic: String = ""
    var Usersap: String = ""
    var CostNo: String = ""
    var TypeJobname: String = ""
    var PosNameTH: String = ""
    var old_sys: String = ""
    var TypeName: String = ""
    var new_sys: String = ""
    var rpos_idx: Int = 0
    var rdept_idx: Int = 0
    var rsec_idx: Int = 0
    var noidx: Int = 0
    var acidx: Int = 0
    var doc_staidx: Int = 0
    var EmpName: String = ""
    var status_name: String = ""
    var dategetjob: String = ""
    var dateendjob: String = ""
    var datecreate: String = ""
    var datecreatestart: String = ""
    var AEmpidx: Int = 0
    var comment: String = ""
    var commentapp1: String = ""
    var commentapp2: String = ""
    var commentapp3: String = ""
    var commentabout: String = ""
    var Email: String = ""
    var MobileNo: String = ""
    var OrgNameTH: String = ""
    var DeptNameTH: String = ""
    var SecNameTH: String = ""
    var user_lock: Int = 0
    var locks: Int = 0
    var doc_code: String = ""
    var node_name: String = ""
    var actor_des: String = ""
    var StatusDoc: String = ""
    var noidx_add: Int = 0
    var acidx_add: Int = 0
    var unidx_add: Int = 0
    var approve_status_add: Int = 0
    var tidx_add: Int = 0
    var Add_IDX_add: Int = 0
    var UIDX: Int = 0
    var ccidx_add: Int = 0;
    var comment_add: String = ""
    var approve_check: String = ""
    var approve_nocheck: String = ""
    
    var u0idx: Int = 0
    var Topic: String = ""
    var PhoneNo: String = ""
    
    var check_ceo: Int = 0
}
//---------IT----------//

// suggestion
struct data_requestlist {
    var RQIDX: Int = 0
    var RCode: String = ""
    var Request_type: Int = 0
    var Comment: String = ""
    var CreateDate: String = ""
    var uq_status: Int = 0
    var deptname: String = ""
    var fullname: String = ""
    var EmpIDX: Int = 0
}
// suggestion

// telephone
struct u0_telephone_detail {
    var u0_idx: Int = 0
    var site_name: String = ""
    var site_phone: String = ""
}

struct u1_telephone_detail {
    var u1_idx: Int = 0
    var u0_idx: Int = 0
    var dept_name: String = ""
    var dept_status: Int = 0
}

struct u2_telephone_detail {
    var u2_idx: Int = 0
    var u1_idx: Int = 0
    var telephone_name: String = ""
    var telephone_number: String = ""
    var telephone_status: Int = 0
}
// telephone
