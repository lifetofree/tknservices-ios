//
//  News_CustomCellList.swift
//  tknservices
//
//  Created by lifetofree on 3/31/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class News_CustomCellList: UITableViewCell {
    
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblNewsTitle: UILabel!
    @IBOutlet weak var lblNewsAuthorDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(news_uidx: Int, news_title: String, news_author: String, update_date: String) {
        lblNewsTitle.text = news_title
        lblNewsAuthorDate.text = "by: " + news_author + " @ " + update_date
        
        let img_ext = [".jpg", ".jpeg", ".png", ".gif"]
        
        for item in img_ext {
            // check image exists
            if let httpUrl = String(urlNewsImage + String(news_uidx) + item),
                let imgUrl = URL(string: httpUrl),
                let imgData = NSData(contentsOf: imgUrl) {
                // use image value
                let imgObj = UIImage(data: imgData as Data)
                imgNews.image = imgObj
                
                return
            }
        }
    }
    
}
