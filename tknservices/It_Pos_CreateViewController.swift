//
//  It_Pos_CreateViewController.swift
//  tknservices
//
//  Created by lifetofree on 8/18/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Pos_CreateViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var view_content: UIView!
    
    var user_selected = UserRequest_detail()
    var emp_detail = employee_detail()
    var arr_emp_detail = [employee_detail]()
    
    var remote_detail = UserRequest_detail()
    var arr_remote_detail = [UserRequest_detail]()
    
    var doccode_detail = UserRequest_detail()
    var arr_doccode_detail = [UserRequest_detail]()
    
    var dataLocation: [String] = []
    var dataRemote: [String] = []
    var dataEmployee: [String] = []
    var dataEmployee_empidx: [String] = []
    var dataHolderDevice: [String] = []
    
    var emp_name: String = ""
    var _emp_name: String = ""
    var emp_code: String = ""
    var emp_name_th: String = ""
    var rsec_idx: Int = 0
    var emp_idx_select: Int = 0
    var remote_idx: Int = 0
    var CheckRemote: Int = 0
    var remote_id: String = ""
    var remote_pass: String = ""
    
    var DocCode: String = ""
    var return_msg_: String = ""
    var return_code_: String = ""
    
    var pvLocation = UIPickerView()
    var pvRemote = UIPickerView()
    var pvEmployee = UIPickerView()
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfCreateName: UITextField!
    @IBOutlet weak var tfTel: UITextField!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var tfTypeRemote: UITextField!
    @IBOutlet weak var tfUserRemote: UITextField!
    @IBOutlet weak var tfComment: UITextField!
    @IBOutlet weak var myImageView: UIImageView!
    
    @IBOutlet weak var btnAttachFile: UIButton!
    
    @IBAction func buttonPopup(_ sender: UITextField) {
        showInputDialog()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        getMyProfile();
        //print(self.emp_name)
        getProfile_Defult();
        
        ddlLocation();
        ddlUserRemote();
        ddlEmployee();
        
        // set picker view
        pvLocation.delegate = self
        pvLocation.dataSource = self
        pvLocation.tag = 1
        tfLocation.inputView = pvLocation
        
        pvRemote.delegate = self
        pvRemote.dataSource = self
        pvRemote.tag = 2
        tfTypeRemote.inputView = pvRemote
        
        pvEmployee.delegate = self
        pvEmployee.dataSource = self
        pvEmployee.tag = 3
        tfCreateName.inputView = pvEmployee
        
        // create tool bar for date picker
        let toolBarTel = UIToolbar()
        toolBarTel.barStyle = .default
        toolBarTel.isTranslucent = true
        toolBarTel.sizeToFit()
        
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        let doneButtonTel = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerTel))
        let spaceButtonTel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonTel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerTel))
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        toolBarTel.setItems([cancelButtonTel,spaceButtonTel,doneButtonTel], animated: false)
        toolBarTel.isUserInteractionEnabled = true
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        tfTel.inputAccessoryView = toolBarTel
        tfComment.inputAccessoryView = toolBarComment
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_Pos_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_Pos_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        /// disable btn
        // check iOS version
        let os = ProcessInfo().operatingSystemVersion
        switch (os.majorVersion, os.minorVersion, os.patchVersion) {
        case (9, _, _):
            btnAttachFile.isEnabled = true
        case (10, _, _):
            btnAttachFile.isEnabled = true
        default:
            // this code will have already crashed on iOS 7, so >= iOS 10.0
            btnAttachFile.isEnabled = false
            btnAttachFile.setTitle("--- coming soon ---", for: .normal)
        }
    }
    
    @IBAction func selectPhotoButtonTapped(_ sender: Any) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            myImageView.image = image
            //print(image)
        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickerTel()
    {
        tfTel.resignFirstResponder()
    }
    func CancelPickerTel()
    {
        tfTel.text = ""
        tfTel.resignFirstResponder()
    }
    
    func donePickerComment()
    {
        tfComment.resignFirstResponder()
    }
    func CancelPickerComment()
    {
        tfComment.text = ""
        tfComment.resignFirstResponder()
    }
    
    func getProfile_Defult() {
        
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        if let emp_code_temp = defaults.value(forKey: "emp_code") {
            emp_code = emp_code_temp as! String
        }
        if let emp_name_th_temp = defaults.value(forKey: "emp_name_th") {
            emp_name_th = emp_name_th_temp as! String
        }
        if let rsec_idx_temp = defaults.value(forKey: "rsec_idx") {
            rsec_idx = rsec_idx_temp as! Int
        }
        
    }
    
    // get my profile
    func getMyProfile() {
        
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        
        let tempUrl = String(urlGetMyProfile + sEmpIDX)
        let sendUrl = URL(string: tempUrl!)!
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    // get value
                    if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                            if(key == "emp_idx") {
                                self.emp_detail.emp_idx = Int(item.stringValue)!
                            }
                            if(key == "emp_code") {
                                self.emp_detail.emp_code = item.stringValue
                            }
                            if(key == "emp_name_th") {
                                self.emp_detail.emp_name_th = item.stringValue
                            }
                            if(key == "rsec_idx") {
                                self.emp_detail.rsec_idx = Int(item.stringValue)!
                            }
                            
                        }
                        self.arr_emp_detail += [self.emp_detail]
                    }
                    else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                            self.emp_detail.emp_idx = Int(item["emp_idx"].stringValue)!
                            self.emp_detail.emp_code = item["emp_code"].stringValue
                            self.emp_detail.emp_name_th = item["emp_name_th"].stringValue
                            self.emp_detail.rsec_idx = Int(item["rsec_idx"].stringValue)!
                            
                            self.arr_emp_detail += [self.emp_detail]
                        }
                    }
                    
                    // set value
                    //defaults.set(self.arr_emp_detail[0].emp_idx, forKey: "emp_idx")
                    //defaults.set(self.arr_emp_detail[0].emp_code, forKey: "emp_code")
                    //defaults.set(self.arr_emp_detail[0].emp_name_th, forKey: "emp_name_th")
                    
                    if let emp_name_th_temp = defaults.value(forKey: "emp_name_th") {
                        self._emp_name = emp_name_th_temp as! String
                    }
                    
                    
                    print(self._emp_name)
                    self.tfCreateName.text = self._emp_name
                    
                    //}
                }
                
        }
        
    }
    
    // Set Insert Repair
    func Insert_Repair() {
        
        if(tfTypeRemote.text! == "Teamviewer"){
            self.remote_idx = 2
            self.CheckRemote = 1
        }
        else if(tfTypeRemote.text! == "RemoteDesktop"){
            self.remote_idx = 39
            self.CheckRemote = 1
        }
        else{
            self.remote_idx = 2
            self.CheckRemote = 0
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        "EmpIDX_add" : sEmpIDX,
                        //"RSecID" : self.rsec_idx,
                        
                        //"RPosIDX_J" : iRsecIDX, //อ้างตามขื่อผู้แจ้ง ***
                        "LocName" : tfLocation.text!, //Location
                        
                        //"OrgIDX" : iRsecIDX, ***
                        "SysIDX_add" : 20,
                        "ISO_User" : 20, //แจ้งซ่อม Pos
                        //"PIDX_Add" : 3, //ลำดับความสำคัญ
                        "TelETC" : tfTel.text!,
                        "EmailETC" : "",
                        "CostIDX" : 0,
                        "NCEmpIDX" : sEmpIDX, //EmpIDX ของผูที่ถูกแจ้งแทน
                        
                        "CheckRemote" : self.CheckRemote,
                        "RemoteIDX" : self.remote_idx,
                        "UserIDRemote" : self.remote_id,
                        "PasswordRemote" : self.remote_pass,
                        
                        //complete
                        "AdminIDX" : 0,
                        "AdminDoingIDX" : 0,
                        "CommentAMDoing" : "",
                        "FileAMDoing" : 0,
                        "CCAIDX" : 0,
                        "StaIDX" : 1,
                        
                        "EmailIDX" : 1, //IT
                        
                        "detailUser" : tfComment.text!,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlInsertItrepair + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get employee
    func ddlEmployee() {
        
        tfCreateName.text = self.emp_name
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "RSecID" : rsec_idx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetChooseOther + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "EmpIDX") {
                                self.remote_detail.EmpIDX = Int(item.stringValue)!
                            }
                            if(key == "FullNameTH") {
                                self.remote_detail.FullNameTH = item.stringValue
                            }
                        }
                        self.arr_remote_detail += [self.remote_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.remote_detail.EmpIDX = Int(item["EmpIDX"].stringValue)!
                            self.remote_detail.FullNameTH = item["FullNameTH"].stringValue
                            
                            self.arr_remote_detail += [self.remote_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_remote_detail.count {
                        if(self.arr_remote_detail[i].FullNameTH != ""){
                            self.dataEmployee += [self.arr_remote_detail[i].FullNameTH]
                        }
                    }
                    
                }
        }
    }
    
    // get UserRemote
    func ddlUserRemote() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "RemoteIDX" : 0,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetRemote + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "RemoteIDX") {
                                self.remote_detail.RemoteIDX = Int(item.stringValue)!
                            }
                            if(key == "RemoteName") {
                                self.remote_detail.RemoteName = item.stringValue
                            }
                        }
                        self.arr_remote_detail += [self.remote_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.remote_detail.RemoteIDX = Int(item["RemoteIDX"].stringValue)!
                            self.remote_detail.RemoteName = item["RemoteName"].stringValue
                            
                            self.arr_remote_detail += [self.remote_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_remote_detail.count {
                        if(self.arr_remote_detail[i].RemoteName != ""){
                            self.dataRemote += [self.arr_remote_detail[i].RemoteName]
                        }
                    }
                    
                }
                //print(responseData.result.value!)
        }
    }
    
    // get Location
    func ddlLocation() {
        
        let value =
            [
                "data_employee" : [
                    "employee_list" : [
                        "org_idx" : 3,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetPlace + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                            if(key == "r0idx") {
                                self.emp_detail.r0idx = Int(item.stringValue)!
                            }
                            if(key == "LocName") {
                                self.emp_detail.LocName = item.stringValue
                            }
                        }
                        self.arr_emp_detail += [self.emp_detail]
                    }
                    else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                            self.emp_detail.r0idx = Int(item["r0idx"].stringValue)!
                            self.emp_detail.LocName = item["LocName"].stringValue
                            
                            self.arr_emp_detail += [self.emp_detail]
                            
                        }
                    }
                    
                    
                    
                    for i in 0 ..< self.arr_emp_detail.count {
                        if(self.arr_emp_detail[i].LocName != ""){
                            self.dataLocation += [self.arr_emp_detail[i].LocName]
                        }
                    }
                }
                
        }
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    
    //--- required --//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLocation.count
        }
        else if(pickerView.tag == 2) {
            return self.dataRemote.count
        }
        else if(pickerView.tag == 3) {
            return self.dataEmployee.count
        }
        else {
            return dataLocation.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLocation[row]
        }
        else if(pickerView.tag == 2) {
            return self.dataRemote[row]
        }
        else if(pickerView.tag == 3) {
            return self.dataEmployee[row]
        }
        else {
            return dataLocation[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLocation.text = self.dataLocation[row]
        }
        else if(pickerView.tag == 2) {
            tfTypeRemote.text = self.dataRemote[row]
        }
        else if(pickerView.tag == 3) {
            tfCreateName.text = self.dataEmployee[row]
            print()
        }
        else {
            tfLocation.text = dataLocation[row]
        }
        self.view.endEditing(true)
    }
    
    func Check_Alert(){
        let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณากรอกข้อมูลให้ครบถ้วน", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        if(tfLocation.text! == "" || tfComment.text! == ""){
            Check_Alert()
        }
        else{
            
            Insert_Repair();
            myImageUploadRequest();
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = itPosTabBarController
            itPosTabBarController.selectedIndex = 0 // It_Pos_TabBarController
        }
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
        itPosTabBarController.selectedIndex = 0 // It_Pos_TabBarController
    }
    //----- Event Button -----//
    
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itPosTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Pos_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itPosTabBarController
        itPosTabBarController.selectedIndex = 0 //
    }
    //----- Navigation Bar Button -----//
    
    func showInputDialog() {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Program Remote", message: "ID / Password", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "OK", style: .default) { (_) in
            
            //getting the input values from user
            let id = alertController.textFields?[0].text
            let password = alertController.textFields?[1].text
            
            self.remote_id = id!
            self.remote_pass = password!
            
            self.tfUserRemote.text = id! + "," + password!
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "ID"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "PassWord"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    func myImageUploadRequest()  {
        
        print ("Start upload photo")
        // ================================
        // Upload the photo to the server
        // ================================
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "SysIDX_add" : 20, // POS
                        "EmpIDX_add" : sEmpIDX, //tfCreateName.text!,
                    ]
                ]
        ]
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_DocCode + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "DocCode") {
                                self.doccode_detail.DocCode = item.stringValue
                            }
                            if(key == "URQIDX") {
                                self.doccode_detail.URQIDX = Int(item.stringValue)!
                            }
                        }
                        self.arr_doccode_detail += [self.doccode_detail]
                    }
                        
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.doccode_detail.DocCode = item["DocCode"].stringValue
                            self.doccode_detail.URQIDX = Int(item["URQIDX"].stringValue)!
                            self.arr_doccode_detail += [self.doccode_detail]
                        }
                    }
                    
                    self.return_code_ = self.arr_doccode_detail[0].DocCode
                    
                    
                    let uploadURL = sUploadUrl + "/ITRepair/mobile_upload/uploads.php"
                    let myUrl = NSURL(string: uploadURL);
                    
                    let request = NSMutableURLRequest(url:myUrl! as URL);
                    request.httpMethod = "POST";
                    let param = [
                        "firstName"  : self.return_code_,
                        "lastName"    : "ITRepair",
                        "userId"    : "1",
                        ]
                    
                    //                    print(param)
                    
                    let boundary = self.generateBoundaryString()
                    
                    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                    
                    
                    if(self.myImageView.image != nil){
                        
                        let imageData = UIImageJPEGRepresentation(self.myImageView.image!, 1)
                        
                        if(imageData==nil)  { return; }
                        
                        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary, Doccode: self.return_code_) as Data
                        
                        //myActivityIndicator.startAnimating();
                        
                        let task = URLSession.shared.dataTask(with: request as URLRequest) {
                            data, response, error in
                            
                            if error != nil {
                                print("error=\(String(describing: error))")
                                return
                            }
                            
                            // You can print out response object
                            print("******* response = \(String(describing: response))")
                            
                            // Print out reponse body
                            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                            print("****** response data = \(responseString!)")
                            
                            do {
                                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                                
                                print(json!)
                                
                                DispatchQueue.main.async(execute: {
                                    //self.myActivityIndicator.stopAnimating()
                                    self.myImageView.image = nil;
                                });
                                
                            }catch
                            {
                                print(error)
                            }
                            
                        }
                        
                        task.resume()
                    }
                    
                }
        }
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, Doccode: String) -> NSData {
        let body = NSMutableData();
        
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString_pos(string: "--\(boundary)\r\n")
                body.appendString_pos(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString_pos(string: "\(value)\r\n")
            }
        }
        
        let filename = Doccode + "0.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString_pos(string: "--\(boundary)\r\n")
        body.appendString_pos(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString_pos(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString_pos(string: "\r\n")
        
        body.appendString_pos(string: "--\(boundary)--\r\n")
        
        return body
        
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(UUID().uuidString)"
    }
    
}

extension NSMutableData {
    
    func appendString_pos(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
