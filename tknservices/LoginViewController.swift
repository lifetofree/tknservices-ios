//
//  LoginViewController.swift
//  tknservices
//
//  Created by lifetofree on 4/3/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController, UITextFieldDelegate {
    var emp_detail = employee_detail()
    var arr_emp_detail = [employee_detail]()
    
    @IBOutlet weak var ivLoginBox: UIImageView!
    @IBOutlet weak var tfEmpCode: UITextField!
    @IBOutlet weak var tfEmpPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // set ivLoginBox rounded
        ivLoginBox.layer.cornerRadius = 15.0
        
        // set button
        btnLogin.roundButton(rad: 20.0, buttonColor: 0x0a5587, borderColor: 0x0a5587, borderWidth: 1.0)
        
        if let _ = defaults.value(forKey: "emp_idx") {
            self.loginSuccess()
        }
        else {
            tfEmpCode.delegate = self
            tfEmpPassword.delegate = self
            // set placeholder text and color
//            tfEmpCode.attributedPlaceholder = NSAttributedString(string:"username", attributes:[NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.2)])
//            tfEmpPassword.attributedPlaceholder = NSAttributedString(string:"password", attributes:[NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.2)])
            
            // add UIToolBar on keyboard and Next button on UIToolBar
            self.addNextButtonOnKeyboard()
            // hide keyboard when tab outside
            self.hideKeyboardWhenTappedAround()
            
            if let ud_emp_code = defaults.value(forKey: "emp_code") {
                tfEmpCode.text = ud_emp_code as? String
            }
        }
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        let providedEmailAddress = tfEmpCode.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        var yourArray = [tfEmpCode.text!]
        let mInt = Int(yourArray[0])

        
        if isEmailAddressValid
        {
            loginCheck_member()
        }
        else if(mInt != nil)  {
            loginCheck()
        }
        else{
            loginCheck_member()
        }

    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = registerViewController
        
    }
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    // function for event
    // check login
    func loginCheck() {
        // check tbEmpCode for nil value
        guard let empCodeText = tfEmpCode.text , !empCodeText.isEmpty else {
            alertEmptyValue(tfEmpCode)
            return
        }
        
        // check tbPassword for nil value
        guard let empPassText = tfEmpPassword.text , !empPassText.isEmpty else {
            alertEmptyValue(tfEmpPassword)
            return
        }
        
        // set data and url
        let value =
            [
                "data_employee" : [
                    "employee_list" : [
                        // "emp_code" : "56000088",
                        // "emp_password" : "53d9d48078580fbf6a0fd139c2622bba"
                        // "emp_code" : "90000001",
                        // "emp_password" : "81dc9bdb52d04dc20036dbd8313ed055"
                        "emp_code" : tfEmpCode.text!,
                        "emp_password" : convertToMd5(tfEmpPassword.text!)
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlCheckLogin + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee"]["return_code"].stringValue)!
                    
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                                if(key == "emp_idx") {
                                    self.emp_detail.emp_idx = Int(item.stringValue)!
                                }
                                if(key == "emp_code") {
                                    self.emp_detail.emp_code = item.stringValue
                                }
                                if(key == "emp_name_th") {
                                    self.emp_detail.emp_name_th = item.stringValue
                                }
                                if(key == "rsec_idx") {
                                    self.emp_detail.rsec_idx = Int(item.stringValue)!
                                }
                                if(key == "rdept_idx") {
                                    self.emp_detail.rdept_idx = Int(item.stringValue)!
                                }
                            }
                            self.arr_emp_detail += [self.emp_detail]
                        } else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                                self.emp_detail.emp_idx = Int(item["emp_idx"].stringValue)!
                                self.emp_detail.emp_code = item["emp_code"].stringValue
                                self.emp_detail.emp_name_th = item["emp_name_th"].stringValue
                                self.emp_detail.rsec_idx = Int(item["rsec_idx"].stringValue)!
                                self.emp_detail.rdept_idx = Int(item["rdept_idx"].stringValue)!

                                self.arr_emp_detail += [self.emp_detail]
                            }
                        }
                        sEmpIDX = String(self.arr_emp_detail[0].emp_idx)
                        defaults.set(sEmpIDX, forKey: "emp_idx")
                        defaults.set(self.arr_emp_detail[0].emp_code, forKey: "emp_code")
                        defaults.set(self.arr_emp_detail[0].emp_name_th, forKey: "emp_name_th")
                        defaults.set(self.arr_emp_detail[0].rsec_idx, forKey: "rsec_idx")
                        defaults.set(self.arr_emp_detail[0].rdept_idx, forKey: "rdept_idx")

                        self.loginSuccess()
                    } else {
                        self.loginFail()
                    }
                    //print(responseData.result.value!)
                }
                
        }
    }
    
    
    func loginCheck_member() {
        // check tbEmpCode for nil value
        guard let empCodeText = tfEmpCode.text , !empCodeText.isEmpty else {
            alertEmptyValue(tfEmpCode)
            return
        }
        
        // check tbEmpCode for nil value
        guard let empPassText = tfEmpPassword.text , !empPassText.isEmpty else {
            alertEmptyValue(tfEmpPassword)
            return
        }
        
        // set data and url
        let value =
            [
                "data_employee_member" : [
                    "BoxEmp_member" : [
                        "Member_EmpUser" : tfEmpCode.text!,
                        "Member_EmpPassword" : tfEmpPassword.text!,
                        
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetLogin_member + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_employee_member"]["ReturnCode"].stringValue)!
                    
                    //print(return_code)
                    // check return_code
                    if (return_code == 0) {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "GuestCentralViewController") as! GuestCentralViewController
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        
                        appDelegate.window?.rootViewController = viewController
                        
                    } else {
                        self.loginFail_member()
                    }
                }
                
        }
    }
    
    // success login send to list page
    func loginSuccess() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    
    
    // fail login then show alert
    func loginFail() {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: "รหัสพนักงาน หรือรหัสผ่านไม่ถูกต้องค่ะ", preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    // fail login then show alert
    func loginFail_member() {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: "ไม่มีข้อมูลภายในระบบค่ะ", preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    // alert when UITextField empty
    func alertEmptyValue(_ _tbName : UITextField) {
        var _textAlert = [tfEmpCode: "กรุณากรอกรหัสพนักงานด้วยค่ะ", tfEmpPassword: "กรุณากรอกรหัสผ่านด้วยค่ะ"]
        let emptyAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: _textAlert[_tbName], preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // set focus
            _tbName.becomeFirstResponder()
        }
        
        emptyAlert.addAction(okAction)
        
        self.present(emptyAlert, animated: true, completion: nil)
    }
    // function for event
    
    // add next button for keyboard : custom button
    func addNextButtonOnKeyboard() {
        let nextToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        nextToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginViewController.nextButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(next)
        
        nextToolbar.items = items
        nextToolbar.sizeToFit()
        
        self.tfEmpCode.inputAccessoryView = nextToolbar
    }
    
    func nextButtonAction() {
        self.tfEmpCode.resignFirstResponder()
        self.tfEmpPassword.becomeFirstResponder()
    }
    // add next button for keyboard : custom button
    
    // function for key submit on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.returnKeyType == UIReturnKeyType.send)
        {
            loginCheck()
        }
        return true
    }
    // function for key submit on keyboard
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
    
    // go to news
    @IBAction func btnNews(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = viewController
    }
    // go to news
    
}
