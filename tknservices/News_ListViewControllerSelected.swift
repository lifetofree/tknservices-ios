//
//  News_ListViewControllerSelected.swift
//  tknservices
//
//  Created by lifetofree on 4/27/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class News_ListViewControllerSelected: UIViewController {
    var val_selected = news_u0_news_detail()
    
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblNewsTitle: UILabel!
    @IBOutlet weak var lblNewsDetail: UILabel!
    @IBOutlet weak var lblNewsAuthor: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        setDetail()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function for event
    // set approve detail
    func setDetail() {
        lblNewsTitle.text = val_selected.news_title
        lblNewsDetail.text = val_selected.news_detail
        lblNewsAuthor.text = "by: " + val_selected.news_author + " @ " + val_selected.update_date
        
        let img_ext = [".jpg", ".jpeg", ".png", ".gif"]
        
        for item in img_ext {
            // check image exists
            if let httpUrl = String(urlNewsImage + String(val_selected.uidx) + item),
                let imgUrl = URL(string: httpUrl),
                let imgData = NSData(contentsOf: imgUrl) {
                // use image value
                let imgObj = UIImage(data: imgData as Data)
                imgNews.image = imgObj
                
                return
            }
        }
    }
    
    // go to news list
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = viewController
    }
    
    @IBAction func btnBack(_ sender: UIBarButtonItem) {
        goToList()
    }
    // function for event
}
