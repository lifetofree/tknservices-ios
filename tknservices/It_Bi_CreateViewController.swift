//
//  It_Bi_CreateViewController.swift
//  TKNServices
//
//  Created by lifetofree on 7/6/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bi_CreateViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfCreateName: UITextField!
    @IBOutlet weak var tfTel: UITextField!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var tfUserLogon: UITextField!
    @IBOutlet weak var tfTcode: UITextField!
    @IBOutlet weak var tfPriority: UITextField!
    @IBOutlet weak var tfComment: UITextField!
    
    var pvLocation = UIPickerView()
    var pvPriority = UIPickerView()
    var pvEmployee = UIPickerView()
    
    var emp_idx: String = ""
    var emp_code: String = ""
    var emp_name_th: String = ""
    var rsec_idx: Int = 0
    
    var emp_name: String = ""
    
    var priority_idx: Int = 0
    
    var dataLocation: [String] = []
    var dataEmployee: [String] = []
    var dataEmployee_empidx: [String] = []
    var dataPriority = ["ด่วน","ไม่ด่วน","ปานกลาง"]
    
    var emp_location = employee_detail()
    var arr_emp_location = [employee_detail]()
    var emp_list = UserRequest_detail()
    var arr_emp_list = [UserRequest_detail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- Event Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if(tfLocation.text! == "" || tfComment.text! == "" || tfUserLogon.text! == "" || tfTcode.text! == "" || tfPriority.text! == "") {
            alertError(return_msg: "กรุณากรอกข้อมูลให้ครบถ้วนค่ะ")
        } else {
            setDataDetail()
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    //----- Event Button -----//
    
    //----- PickerView Requried -----//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(pickerView.tag) {
        case 1:
            return self.dataLocation.count
        case 2:
            return self.dataPriority.count
        default:
            return self.dataEmployee.count
        }
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(pickerView.tag) {
        case 1:
            return self.dataLocation[row]
        case 2:
            return self.dataPriority[row]
        default:
            return dataEmployee[row]
        }
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch(pickerView.tag) {
        case 1:
            tfLocation.text = self.dataLocation[row]
        case 2:
            tfPriority.text = self.dataPriority[row]
        default:
            tfCreateName.text = dataEmployee[row]
        }
        self.view.endEditing(true)
    }
    //----- PickerView Requried -----//
    
    //----- Reuse -----//
    func initView() {
        // set content corner
        view_content.layer.cornerRadius = 10
        
        tfUserLogon.delegate = self
        tfUserLogon.returnKeyType = .done
        
        tfTcode.delegate = self
        tfTcode.returnKeyType = .done
        
        tfComment.delegate = self
        tfComment.returnKeyType = .done
        
        // done button on number pad
        self.addDoneButtonOnKeyboard()
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bi_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bi_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        getMyProfile()
//        ddlEmployee()
        tfCreateName.text = self.emp_name_th
        tfCreateName.isUserInteractionEnabled = false
        ddlLocation()
        
        // set picker view
        pvLocation.delegate = self
        pvLocation.dataSource = self
        pvLocation.tag = 1
        tfLocation.inputView = pvLocation
        
        pvPriority.delegate = self
        pvPriority.dataSource = self
        pvPriority.tag = 2
        tfPriority.text = dataPriority[0]
        tfPriority.inputView = pvPriority
        
//        pvEmployee.delegate = self
//        pvEmployee.dataSource = self
//        pvEmployee.tag = 3
//        tfCreateName.inputView = pvEmployee
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_code = defaults.string(forKey: "emp_idx") {
            emp_idx = ud_emp_code
        }
        if let ud_emp_code = defaults.string(forKey: "emp_code") {
            emp_code = ud_emp_code
        }
        if let ud_emp_name_th = defaults.string(forKey: "emp_name_th") {
            emp_name_th = ud_emp_name_th
        }
        if let ud_rsec_idx = defaults.string(forKey: "rsec_idx") {
            rsec_idx = Int(ud_rsec_idx)!
        }
    }
    // get user data
    
    // get Location
    func ddlLocation() {
        
        let value =
            [
                "data_employee" : [
                    "employee_list" : [
                        "org_idx" : 1
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetPlace + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_employee"]["return_msg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_employee"]["employee_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_employee"]["employee_list"].dictionaryValue {
                            if(key == "r0idx") {
                                self.emp_location.r0idx = Int(item.stringValue)!
                            }
                            if(key == "LocName") {
                                self.emp_location.LocName = item.stringValue
                            }
                        }
                        self.arr_emp_location += [self.emp_location]
                    }
                    else if swiftyJsonVar["data_employee"]["employee_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_employee"]["employee_list"].arrayValue {
                            self.emp_location.r0idx = Int(item["r0idx"].stringValue)!
                            self.emp_location.LocName = item["LocName"].stringValue
                            
                            self.arr_emp_location += [self.emp_location]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_emp_location.count {
                        if(self.arr_emp_location[i].LocName != ""){
                            self.dataLocation += [self.arr_emp_location[i].LocName]
                        }
                    }
                }
        }
    }
    // get Location
    
    // set data detail
    func setDataDetail() {
        switch tfPriority.text {
        case "ด่วน":
            priority_idx = 1
        case "ไม่ด่วน":
            priority_idx = 2
        default:
            priority_idx = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        "EmpIDX_add" : sEmpIDX,
                        "LocName" : tfLocation.text!,
                        
                        "SysIDX_add" : 22, // BI
                        "ISO_User" : 2,
                        "PIDX_Add" : priority_idx, //priority
                        "TelETC" : tfTel.text!,
                        "EmailETC" : "",
                        "CostIDX" : 0,
                        "NCEmpIDX" : sEmpIDX,
                        
                        "UserLogonName" : tfUserLogon.text!,
                        "TransactionCode" : tfTcode.text!,
                        
                        "CheckRemote" : 0,
                        "RemoteIDX" : 0,
                        "UserIDRemote" : "",
                        "PasswordRemote" : "",
                        
                        "AdminIDX" : 0,
                        "AdminDoingIDX" : 0,
                        "CommentAMDoing" : "",
                        "FileAMDoing" : 0,
                        "CCAIDX" : 0,
                        "StaIDX" : 1,
                        "EmailIDX" : 1, // Default User Stored Mobile
                        "detailUser" : tfComment.text!
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlInsertItrepair + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
        
        goBack()
    }
    // set data detail
    
    // alertError
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBiTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bi_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBiTabBarController
        itBiTabBarController.selectedIndex = 0 //
    }
    
    // TextField resign first responder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // done button on number pad
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(It_Bi_CreateViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.tfTel.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.tfTel.resignFirstResponder()
    }
    // done button on number pad
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
    //----- Reuse -----//
}
