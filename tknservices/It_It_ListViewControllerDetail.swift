//
//  It_It_ListViewControllerDetail.swift
//  tknservices
//
//  Created by MaI on 7/25/2560 BE.
//  Copyright © 2560 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class It_It_ListViewControllerDetail: UIViewController {

    //@IBOutlet weak var btnReceive: UIButton!
    @IBOutlet weak var btnReceiveJob: UIButton!
    @IBOutlet weak var btntransfer: UIButton!
    @IBOutlet weak var btnClosejob: UIButton!
    @IBOutlet weak var btnUserClosejob: UIButton!
    
    @IBOutlet weak var lblDocCode: UILabel!
    @IBOutlet weak var lblCreateDateTime: UILabel!
    @IBOutlet weak var lblEmpName_Create: UILabel!
    @IBOutlet weak var lblTel: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblRemoteName: UILabel!
    @IBOutlet weak var lblUserRemote: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var imgAttach: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setDetail()
        
        btnReceiveJob.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btntransfer.roundButton(rad: 5.0, buttonColor: colorOrangeButton, borderColor: colorOrangeBorder, borderWidth: 1.0)
        btnClosejob.roundButton(rad: 5.0, buttonColor: colorBlueButton, borderColor: colorBlueBorder, borderWidth: 1.0)
        btnUserClosejob.roundButton(rad: 5.0, buttonColor: colorBlueButton, borderColor: colorBlueBorder, borderWidth: 1.0)
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // set detail
    func setDetail() {
        lblDocCode.text = DocCode
        lblCreateDateTime.text = CreateDateUser + " " + TimeCreateJob
        lblEmpName_Create.text = EmpName
        lblTel.text = TelETC
        lblLocation.text = LocName
        lblRemoteName.text = RemoteName
        lblUserRemote.text = UserIDRemote + "," + PasswordRemote
        lblComment.text = Comment

        if(acidx == 1){ //ผู้สร้าง
            
            if(unidx == 2 && Int(sEmpIDX)! == EmpIDX_Create){
                btnReceiveJob.isHidden = true
                btntransfer.isHidden = true
                btnClosejob.isHidden = true
                btnUserClosejob.isHidden = false
            }
            else{
                btnReceiveJob.isHidden = true
                btntransfer.isHidden = true
                btnClosejob.isHidden = true
                btnUserClosejob.isHidden = true
            }
        }
        else if(acidx == 3){ //เจ้าหน้าที่ IT
            if(unidx == 7 && SysIDX == 3 && (RDeptIDX == 20 || RDeptIDX == 21)){
                btnReceiveJob.isHidden = false
                btntransfer.isHidden = false
                btnClosejob.isHidden = true
                btnUserClosejob.isHidden = true
            }
            else if(unidx == 4 && SysIDX == 3 && (RDeptIDX == 20 || RDeptIDX == 21)){
                btnReceiveJob.isHidden = true
                btntransfer.isHidden = true
                btnClosejob.isHidden = false
                btnUserClosejob.isHidden = true
            }
        }
        else{
            btnReceiveJob.isHidden = true
            btntransfer.isHidden = true
            btnClosejob.isHidden = true
            btnUserClosejob.isHidden = true
        }
        
        let img_ext = [".jpg", ".jpeg", ".png", ".gif"]
        for item in img_ext {
            // check image exists
            
            if let httpUrl = String(urlItRepairImage +  String(DocCode) + "/"  + String(DocCode) + "0" + item),
                
                let imgUrl = URL(string: httpUrl),
                let imgData = NSData(contentsOf: imgUrl) {
                // use image value
                let imgObj = UIImage(data: imgData as Data)
                imgAttach.image = imgObj
                
                
                let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped))
                imgAttach.isUserInteractionEnabled = true
                imgAttach.addGestureRecognizer(tapGestureRecognizer)
                
                return
            }
        }
    }
        
    func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        // tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        newImageView.addGestureRecognizer(tap)
        // pinch
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        newImageView.addGestureRecognizer(pinchRecognizer)
        self.view.addSubview(newImageView)
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 2 && acidx == 1){
            if(approve == 5){
                staidx = 24
            }
            else{
                staidx = 23
            }
        }

        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_ITGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }

    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func handlePinchGesture(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            if CGFloat(view.transform.a) > 5.0 {
                view.transform.a = 5.0 // this is x coordinate
                view.transform.d = 5.0 // this is x coordinate
            }
            if CGFloat(view.transform.d) < 1.0 {
                view.transform.a = 1.0 // this is x coordinate
                view.transform.d = 1.0 // this is x coordinate
            }
            sender.scale = 1
            
            // pan
            let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            if view.transform.a > 1.0 {
                view.addGestureRecognizer(panRecognizer)
//                print("aaa")
            } else {
                view.removeGestureRecognizer(panRecognizer)
//                print("bbb")
            }
//            print(view.transform.a)
        }
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        // get translation
        let translation = sender.translation(in: view)
        sender.setTranslation(CGPoint.zero, in: view)
        //        print(translation)
        
        //create a new Label and give it the parameters of the old one
        let label = sender.view! as UIView
        label.center = CGPoint(x: label.center.x + translation.x, y: label.center.y + translation.y)
        label.isMultipleTouchEnabled = true
        label.isUserInteractionEnabled = true
        
    }
    
    //----- Event Button -----//
    @IBAction func btnReceiveJob(_ sender: UIButton) {

        urlUpdate()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_It_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0

    }
    
    @IBAction func btnTransferJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itItJobTransViewController = storyBoard.instantiateViewController(withIdentifier: "It_It_JobTransViewController") as! It_It_JobTransViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itItJobTransViewController
    }
    
    @IBAction func btnCloseJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let ItItCloseJobViewController = storyBoard.instantiateViewController(withIdentifier: "It_It_ReceiveJobViewController") as! It_It_ReceiveJobViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = ItItCloseJobViewController
        
    }
    
    @IBAction func btnUserCloseJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itItCloseJobUViewController = storyBoard.instantiateViewController(withIdentifier: "It_It_CloseJobUViewController") as! It_It_CloseJobUViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itItCloseJobUViewController
    }
}
