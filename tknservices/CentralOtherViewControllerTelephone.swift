//
//  CentralOtherViewControllerTelephone.swift
//  TKNServices
//
//  Created by lifetofree on 7/12/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CentralOtherViewControllerTelephone: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnCallPhone: UIButton!
    @IBOutlet weak var tfSiteList: UITextField!
    @IBOutlet weak var tfDeptList: UITextField!
    @IBOutlet weak var tbvTelephone: UITableView!
    
    var pvSiteList = UIPickerView()
    var pvDeptList = UIPickerView()
    
    var u0_detail = u0_telephone_detail()
    var u1_detail = u1_telephone_detail()
    var u2_detail = u2_telephone_detail()
    var arr_u0_detail = [u0_telephone_detail]()
    var arr_u1_detail = [u1_telephone_detail]()
    var arr_u2_detail = [u2_telephone_detail]()
    
    var data_site: String = ""
    var data_dept: String = ""
    var data_phone: String = ""
    
    var button_title: String = ""
    
    var dataU0: [String] = []
    var dataU1: [String] = []
    var dataU2: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- PickerView Requried -----//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(pickerView.tag) {
        case 1:
            return self.dataU0.count
        case 2:
            return self.dataU1.count
        default:
            return self.dataU2.count
        }
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(pickerView.tag) {
        case 1:
            return self.dataU0[row]
        case 2:
            return self.dataU1[row]
        default:
            return self.dataU2[row]
        }
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch(pickerView.tag) {
        case 1:
            tfSiteList.text = self.dataU0[row]
            data_site = self.dataU0[row]
            tfDeptList.text = ""
            getDeptList()
            
            // set call button
            self.button_title = "Call to " + self.data_site
            self.btnCallPhone.setTitle(self.button_title + " " + data_phone, for: .normal)
            self.btnCallPhone.isHidden = false
            // set call button
            
            arr_u2_detail = [u2_telephone_detail]()
            reloadTelephone()
        case 2:
            tfDeptList.text = self.dataU1[row]
            data_dept = self.dataU1[row]
            getTelephoneList()
        default:
            tfDeptList.text = self.dataU1[row]
            data_dept = self.dataU1[row]
            getTelephoneList()
        }
        
        self.view.endEditing(true)
    }
    //----- PickerView Requried -----//
    
    //--- Tabel View required --//
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u2_detail.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tvcTelephone", for: indexPath)
        
        // load item
        cell.textLabel?.text = arr_u2_detail[indexPath.row].telephone_name
        cell.detailTextLabel?.text = arr_u2_detail[indexPath.row].telephone_number
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    //--- Tabel View required --//
    
    //----- Event Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnCallPhone(_ sender: UIButton) {
        if(data_phone != "") {
            data_phone.makeMyCall()
        }
        // print("phone no. " + data_phone)
    }
    //----- Event Button -----//
    
    //----- reuse -----//
    func initView() {
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        tbvTelephone.delegate = self
        tbvTelephone.dataSource = self
        
        // set button
        btnCallPhone.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        // set button
        
        // set picker view
        pvSiteList.delegate = self
        pvSiteList.dataSource = self
        pvSiteList.tag = 1
        tfSiteList.inputView = pvSiteList
        
        pvDeptList.delegate = self
        pvDeptList.dataSource = self
        pvDeptList.tag = 2
        tfDeptList.inputView = pvDeptList
        // set picker view
        
        getSiteList()
        reloadTelephone()
        btnCallPhone.isHidden = true
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 3 // CentralOtherViewController
    }
    
    // get site list
    func getSiteList() {
        let value =
            [
                "data_telephone" : [
                    "u0_telephone_list" : [
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetTelephoneListU0 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_telephone"]["return_code"].stringValue
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["data_telephone"]["u0_telephone_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_telephone"]["u0_telephone_list"].dictionaryValue {
                                if(key == "u0_idx") {
                                    self.u0_detail.u0_idx = Int(item.stringValue)!
                                }
                                if(key == "site_name") {
                                    self.u0_detail.site_name = item.stringValue
                                }
                                if(key == "site_phone") {
                                    self.u0_detail.site_phone = item.stringValue
                                }
                            }
                            self.arr_u0_detail += [self.u0_detail]
                        }
                        else if swiftyJsonVar["data_telephone"]["u0_telephone_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_telephone"]["u0_telephone_list"].arrayValue {
                                self.u0_detail.u0_idx = Int(item["u0_idx"].stringValue)!
                                self.u0_detail.site_name = item["site_name"].stringValue
                                self.u0_detail.site_phone = item["site_phone"].stringValue
                                
                                self.arr_u0_detail += [self.u0_detail]
                                
                            }
                        }
                    }
                    
                    for i in 0 ..< self.arr_u0_detail.count {
                        if(self.arr_u0_detail[i].site_name != ""){
                            self.dataU0 += [self.arr_u0_detail[i].site_name]
                        }
                    }
                }
        }
    }
    
    // get dept list
    func getDeptList() {
        arr_u1_detail = [u1_telephone_detail]()
        let site_idx = arr_u0_detail.filter{$0.site_name.contains(data_site)}.map{$0.u0_idx}
        let site_phone = arr_u0_detail.filter{$0.site_name.contains(data_site)}.map{$0.site_phone}
        self.data_phone = site_phone[0]
        
        let value =
            [
                "data_telephone" : [
                    "u1_telephone_list" : [
                        "u0_idx" : site_idx[0]
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetTelephoneListU1 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_telephone"]["return_code"].stringValue
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["data_telephone"]["u1_telephone_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_telephone"]["u1_telephone_list"].dictionaryValue {
                                if(key == "u1_idx") {
                                    self.u1_detail.u1_idx = Int(item.stringValue)!
                                }
                                if(key == "dept_name") {
                                    self.u1_detail.dept_name = item.stringValue
                                }
                            }
                            self.arr_u1_detail += [self.u1_detail]
                        }
                        else if swiftyJsonVar["data_telephone"]["u1_telephone_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_telephone"]["u1_telephone_list"].arrayValue {
                                self.u1_detail.u1_idx = Int(item["u1_idx"].stringValue)!
                                self.u1_detail.dept_name = item["dept_name"].stringValue
                                
                                self.arr_u1_detail += [self.u1_detail]
                                
                            }
                        }
                    }
                    
                    for i in 0 ..< self.arr_u1_detail.count {
                        if(self.arr_u1_detail[i].dept_name != ""){
                            self.dataU1 += [self.arr_u1_detail[i].dept_name]
                        }
                    }
                }
        }
    }
    
    // get telephone list
    func getTelephoneList() {
        arr_u2_detail = [u2_telephone_detail]()
        let dept_idx = arr_u1_detail.filter{$0.dept_name.contains(data_dept)}.map{$0.u1_idx}
        
        let value =
            [
                "data_telephone" : [
                    "u2_telephone_list" : [
                        "u1_idx" : dept_idx
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetTelephoneListU2 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_telephone"]["return_code"].stringValue
                    // check return_code
                    if (return_code == 0) {
                        if swiftyJsonVar["data_telephone"]["u2_telephone_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_telephone"]["u2_telephone_list"].dictionaryValue {
                                if(key == "u2_idx") {
                                    self.u2_detail.u2_idx = Int(item.stringValue)!
                                }
                                if(key == "telephone_name") {
                                    self.u2_detail.telephone_name = item.stringValue
                                }
                                if(key == "telephone_number") {
                                    self.u2_detail.telephone_number = item.stringValue
                                }
                            }
                            self.arr_u2_detail += [self.u2_detail]
                        }
                        else if swiftyJsonVar["data_telephone"]["u2_telephone_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_telephone"]["u2_telephone_list"].arrayValue {
                                self.u2_detail.u2_idx = Int(item["u2_idx"].stringValue)!
                                self.u2_detail.telephone_name = item["telephone_name"].stringValue
                                self.u2_detail.telephone_number = item["telephone_number"].stringValue
                                
                                self.arr_u2_detail += [self.u2_detail]
                                
                            }
                        }
                    }
                    
                    // count data and reload
                    if (self.arr_u2_detail.count > 0) {
                        // set back to label view
                        self.tbvTelephone.backgroundView = nil;
                        
                        self.tbvTelephone.reloadData()
                    }
                    else {
                        self.tbvTelephone.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvTelephone.bounds.size.width, height: self.tbvTelephone.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray
                        
                        // set back to label view
                        self.tbvTelephone.backgroundView = lblEmpty;
                        // no separator
                        self.tbvTelephone.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    func reloadTelephone() {
        // count data and reload
        if (self.arr_u2_detail.count > 0) {
            // set back to label view
            self.tbvTelephone.backgroundView = nil;
            
            self.tbvTelephone.reloadData()
        }
        else {
            self.tbvTelephone.reloadData()
            
            // set label size
            let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvTelephone.bounds.size.width, height: self.tbvTelephone.bounds.size.height))
            // set the message
            lblEmpty.text = "No data is available";
            lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
            // center the text
            lblEmpty.textAlignment = .center;
            lblEmpty.textColor = UIColor.darkGray
            
            // set back to label view
            self.tbvTelephone.backgroundView = lblEmpty;
            // no separator
            self.tbvTelephone.separatorStyle = UITableViewCellSeparatorStyle.none;
        }
    }
    //----- reuse -----//
}

