//
//  Vac_ListViewControllerSelected.swift
//  tknservices
//
//  Created by lifetofree on 4/20/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Vac_ListViewControllerSelected: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var val_selected = u0_document_detail()
    var val_leavetype = vacation_leavetype()
    var arr_leavetype_detail = [vacation_leavetype]()
    var val_shfittime = vacation_shifttime()
    var arr_shfittime_detail = [vacation_shifttime]()
    
    var dataLeaveType: [String] = []
    var dataShiftName: [String] = []
    var dataDocType = ["รายการทั่วไป","ใบคำร้อง(ยกเลิก)","ใบคำร้อง(ย้อนหลัง)"]
    
    @IBOutlet weak var lblSelected: UILabel!
    @IBOutlet weak var view_content: UIView!
    
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var tfLeaveType: UITextField!
    @IBOutlet weak var tfShiftName: UITextField!
    @IBOutlet weak var tfLeaveStart: UITextField!
    @IBOutlet weak var tfLeaveEnd: UITextField!
    @IBOutlet weak var tfLeaveComment: UITextField!
    @IBOutlet weak var imgAttach: UIImageView!
    
    @IBOutlet weak var btnSaveData: UIButton!
    @IBOutlet weak var btnClearData: UIButton!
    
    var pvLeaveType = UIPickerView()
    var pvShiftName = UIPickerView()
    
    var dpLeaveStart = UIDatePicker()
    var dpLeaveEnd = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // get picker view data
        ddlLeaveType()
        ddlShfitTime()
        // get picker view data
        
        // set picker view
        pvLeaveType.delegate = self
        pvLeaveType.dataSource = self
        pvLeaveType.tag = 1
        tfLeaveType.inputView = pvLeaveType
        
        pvShiftName.delegate = self
        pvShiftName.dataSource = self
        pvShiftName.tag = 2
        tfShiftName.inputView = pvShiftName
        // set picker view
        
        // set date picker
        tfLeaveStart.inputView = dpLeaveStart
        tfLeaveEnd.inputView = dpLeaveEnd
        // set date picker
        
        // set detail
        setDetail()
        
        // create tool bar for date picker
        let toolBarLeaveStart = UIToolbar()
        toolBarLeaveStart.barStyle = .default
        toolBarLeaveStart.isTranslucent = true
        toolBarLeaveStart.sizeToFit()
        
        let toolBarLeaveEnd = UIToolbar()
        toolBarLeaveEnd.barStyle = .default
        toolBarLeaveEnd.isTranslucent = true
        toolBarLeaveEnd.sizeToFit()
        
        let doneButtonLeaveStart = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveStart))
        let spaceButtonLeaveStart = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonLeaveStart = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveStart))
        
        let doneButtonLeaveEnd = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveEnd))
        let spaceButtonLeaveEnd = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonLeaveEnd = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerLeaveEnd))
        
        toolBarLeaveStart.setItems([cancelButtonLeaveStart, spaceButtonLeaveStart, doneButtonLeaveStart], animated: false)
        toolBarLeaveStart.isUserInteractionEnabled = true
        
        toolBarLeaveEnd.setItems([cancelButtonLeaveEnd, spaceButtonLeaveEnd, doneButtonLeaveEnd
            ], animated: false)
        toolBarLeaveEnd.isUserInteractionEnabled = true
        
        // set tool bar w/ button
        tfLeaveStart.inputAccessoryView = toolBarLeaveStart
        tfLeaveEnd.inputAccessoryView = toolBarLeaveEnd
        
        // set button
        btnSaveData.roundButton(rad: 5.0, buttonColor: 0x4E9B13, borderColor: 0x3A4B1A, borderWidth: 1.0)
        btnClearData.roundButton(rad: 5.0, buttonColor: 0x397FC2, borderColor: 0x26488D, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(Vac_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Vac_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function for event
    // set detail
    func setDetail() {
        lblEmpName.text = val_selected.emp_name
        tfLeaveType.text = val_selected.m0_leavetype_name
        tfShiftName.text = val_selected.u0_emp_shift_name
        tfLeaveStart.text = val_selected.u0_leave_start
        tfLeaveEnd.text = val_selected.u0_leave_end
        tfLeaveComment.text = val_selected.u0_leave_comment
        
        let leave_status = val_selected.u0_leave_status
        let node_status = val_selected.m0_node_idx
        
        if (node_status == 1 && leave_status == 0) {
            lblSelected.text = "แก้ไขรายการลา"
        } else {
            lblSelected.text = "รายละเอียดการลา"
            
            btnSaveData.isHidden = true
            btnClearData.isHidden = true
            
            tfLeaveType.isEnabled = false
            tfShiftName.isEnabled = false
            tfLeaveStart.isEnabled = false
            tfLeaveEnd.isEnabled = false
            tfLeaveComment.isEnabled = false
            
            let img_ext = [".jpg", ".jpeg", ".png", ".gif"]
            
            for item in img_ext {
                // check image exists
                if let httpUrl = String(sUploadUrlVac + String(val_selected.u0_document_idx) + item),
                    let imgUrl = URL(string: httpUrl),
                    let imgData = NSData(contentsOf: imgUrl) {
                    // use image value
                    let imgObj = UIImage(data: imgData as Data)
                    imgAttach.image = imgObj
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped))
                    imgAttach.isUserInteractionEnabled = true
                    imgAttach.addGestureRecognizer(tapGestureRecognizer)
                    
                    return
                }
            }
        }
    }
    
    // set init date
    func setInitDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        
//        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
//        let convertedDateString = dateFormatter.string(from: Date())
//        lblCreateDate.text = convertedDateString
        
        dateFormatter.dateFormat = "dd/MM/yyyy 08:30"
        let convertedDateStartString = dateFormatter.string(from: Date())
        tfLeaveStart.text = convertedDateStartString
//        dpLeaveStart.setDate(dateFormatter.date(from: convertedDateStartString)!, animated: false)
        
        dateFormatter.dateFormat = "dd/MM/yyyy 17:30"
        let convertedDateEndString = dateFormatter.string(from: Date())
        tfLeaveEnd.text = convertedDateEndString
//        dpLeaveEnd.setDate(dateFormatter.date(from: convertedDateEndString)!, animated: false)
    }
    
    // set init text
    func setInitText() {
//        tfDocType.text = dataDocType[0]
        tfLeaveType.text = ""
        tfShiftName.text = ""
        tfLeaveComment.text = ""
    }
    
    // get leave type
    func ddlLeaveType() {
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : 0,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetLeaveTypeList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].dictionaryValue {
                            if(key == "midx") {
                                self.val_leavetype.midx = Int(item.stringValue)!
                            }
                            if(key == "type_name") {
                                self.val_leavetype.type_name = item.stringValue
                            }
                        }
                        self.arr_leavetype_detail += [self.val_leavetype]
                    } else if swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_vacation"]["vacation_leavetype_list"].arrayValue {
                            self.val_leavetype.midx = Int(item["midx"].stringValue)!
                            self.val_leavetype.type_name = item["type_name"].stringValue
                            
                            self.arr_leavetype_detail += [self.val_leavetype]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_leavetype_detail.count {
                        self.dataLeaveType += [self.arr_leavetype_detail[i].type_name]
                    }
                }
        }
    }
    
    //get shift time
    func ddlShfitTime() {
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : 0
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetShfitTimeList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].dictionaryValue {
                            if(key == "midx") {
                                self.val_shfittime.midx = Int(item.stringValue)!
                            }
                            if(key == "TypeWork") {
                                self.val_shfittime.TypeWork = item.stringValue
                            }
                        }
                        self.arr_shfittime_detail += [self.val_shfittime]
                    } else if swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].arrayObject != nil {
                        for (item) in swiftyJsonVar["data_vacation"]["vacation_shifttime_list"].arrayValue {
                            self.val_shfittime.midx = Int(item["midx"].stringValue)!
                            self.val_shfittime.TypeWork = item["TypeWork"].stringValue
                            
                            self.arr_shfittime_detail += [self.val_shfittime]
                            
                        }
                        
                    }
                    
                    for i in 0 ..< self.arr_shfittime_detail.count {
                        self.dataShiftName += [self.arr_shfittime_detail[i].TypeWork]
                    }
                }
        }
    }
    
    //--- required --//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //--- required --//
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1) {
            return self.dataLeaveType.count
        } else {
            return self.dataShiftName.count
        }
    }
    
    //--- required --//
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1) {
            return self.dataLeaveType[row]
        } else {
            return self.dataShiftName[row]
        }
    }
    
    //--- required --//
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1) {
            tfLeaveType.text = self.dataLeaveType[row]
        } else {
            tfShiftName.text = self.dataShiftName[row]
        }
        self.view.endEditing(true)
    }
    
    // click done button
    func donePickerLeaveStart()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let convertedDateStartString = dateFormatter.string(from: dpLeaveStart.date as Date)
        tfLeaveStart.text = convertedDateStartString
        
        tfLeaveStart.resignFirstResponder()
    }
    
    func donePickerLeaveEnd()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let convertedDateEndString = dateFormatter.string(from: dpLeaveEnd.date as Date)
        tfLeaveEnd.text = convertedDateEndString
        
        tfLeaveEnd.resignFirstResponder()
    }
    
    // go to create list
    func goToList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vacTabBarController = storyBoard.instantiateViewController(withIdentifier: "Vac_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = vacTabBarController
        vacTabBarController.selectedIndex = 0 // Vac_ListViewController
    }
    
    func setList(approveType: Int) -> Int {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            iEmpIDX = Int(ud_emp_idx as! String)!
        }
        
        let value =
            [
                "data_vacation" : [
                    "u0_document_list" : [
                        "u0_document_idx" : val_selected.u0_document_idx,
                        
                        "type_list" : val_selected.type_list,
                        "m0_node_idx" : val_selected.m0_node_idx,
                        "m0_actor_idx" : val_selected.m0_actor_idx,
                        "emp_idx" : val_selected.emp_idx,
                        
                        "m0_leavetype_name" : tfLeaveType.text!,
                        "u0_emp_shift_name" : tfShiftName.text!,
                        "u0_leave_start" : tfLeaveStart.text!,
                        "u0_leave_end" : tfLeaveEnd.text!,
                        "u0_leave_comment" : tfLeaveComment.text!,
                    ],
                    "u1_document_list" : [
                        "u0_document_idx" : val_selected.u0_document_idx,
                        "action_emp_idx" : iEmpIDX,
                        "action_node_idx" : 1, // node for approve
                        "u1_approve_status" : 0
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSetU0DocumentList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
        
        return return_code
    }
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        // tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        newImageView.addGestureRecognizer(tap)
        // pinch
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        newImageView.addGestureRecognizer(pinchRecognizer)
        self.view.addSubview(newImageView)
        //        // pan
        //        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        //        newImageView.addGestureRecognizer(panRecognizer)
    }
    
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func handlePinchGesture(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            if CGFloat(view.transform.a) > 5.0 {
                view.transform.a = 5.0 // this is x coordinate
                view.transform.d = 5.0 // this is x coordinate
            }
            if CGFloat(view.transform.d) < 1.0 {
                view.transform.a = 1.0 // this is x coordinate
                view.transform.d = 1.0 // this is x coordinate
            }
            sender.scale = 1
            
            // pan
            let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            if view.transform.a > 1.0 {
                view.addGestureRecognizer(panRecognizer)
                print("aaa")
            } else {
                view.removeGestureRecognizer(panRecognizer)
                print("bbb")
            }
            print(view.transform.a)
        }
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        // get translation
        let translation = sender.translation(in: view)
        sender.setTranslation(CGPoint.zero, in: view)
        //        print(translation)
        
        //create a new Label and give it the parameters of the old one
        let label = sender.view! as UIView
        label.center = CGPoint(x: label.center.x + translation.x, y: label.center.y + translation.y)
        label.isMultipleTouchEnabled = true
        label.isUserInteractionEnabled = true
        
        //        if sender.state == UIGestureRecognizerState.began {
        //            //add something you want to happen when the Label Panning has started
        //            print("started")
        //        }
        //
        //        if sender.state == UIGestureRecognizerState.ended {
        //            //add something you want to happen when the Label Panning has ended
        //            print("ended")
        //        }
        //
        //
        //        if sender.state == UIGestureRecognizerState.changed {
        //            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
        //            print("changed")
        //        }
        //        else {
        //            // or something when its not moving
        //            print("not moving")
        //        }
    }
    
    @IBAction func btnSaveData(_ sender: UIButton) {
        return_code = setList(approveType: 1) // List
        // when completed
        goToList()
    }
    
    @IBAction func btnClearData(_ sender: UIButton) {
        setInitDate()
        setInitText()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goToList()
    }
    // function for event
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
}
