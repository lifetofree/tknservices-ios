//
//  Vac_ListViewController.swift
//  tknservices
//
//  Created by lifetofree on 4/4/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Vac_ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var refreshControl = UIRefreshControl()
    
    var u0_doc = u0_document_detail()
    var arr_u0_doc = [u0_document_detail]()
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var tbvList: UITableView!
    let textCellIdentifier = "tvcList"
    
    var data_selected = u0_document_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set table view w/ transparent background
        self.tbvList.backgroundColor = UIColor.clear
        
        tbvList.delegate = self
        tbvList.dataSource = self
        
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(Vac_ListViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tbvList.addSubview(refreshControl)
        
        self.getAllList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_doc.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: Vac_CustomCellList = tbvList.dequeueReusableCell(withIdentifier: textCellIdentifier) as! Vac_CustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _vac_type = arr_u0_doc[row].m0_leavetype_name
        let _vac_start = arr_u0_doc[row].u0_leave_start
        let _vac_end = arr_u0_doc[row].u0_leave_end
        var _vac_status = ""
        let _vac_m0_node = arr_u0_doc[row].m0_node_idx
        let _vac_m0_actor = arr_u0_doc[row].m0_actor_idx
        
        if(_vac_m0_node == 1 && _vac_m0_actor == 1){
            _vac_status = "แก้ไข"
        }
        else{
            _vac_status = arr_u0_doc[row].u0_status_name
        }
        // load item
        cell.loadItem(vac_type: _vac_type, vac_start: _vac_start, vac_end: _vac_end, vac_status: _vac_status)
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        data_selected = arr_u0_doc[row]
    }
    
    //--- connected to other view ---//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let iIndexPath : IndexPath = self.tbvList.indexPathForSelectedRow! as IndexPath
        
        let detailView = segue.destination as! Vac_ListViewControllerSelected
        
        data_selected = arr_u0_doc[iIndexPath.row]
        detailView.val_selected = data_selected
    }
    
    // function for event
    // get all approve
    func getAllList() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        // print(sEmpIDX)
        
        let tempUrl = String(urlGetU0DocumentList + sEmpIDX)
        let sendUrl = URL(string: tempUrl!)!
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    // check return_code
                    if (return_code == 0) {
                        // clear data
                        self.arr_u0_doc.removeAll()
                        // get value
                        if swiftyJsonVar["data_vacation"]["u0_document_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_vacation"]["u0_document_list"].dictionaryValue {
                                if(key == "u0_document_idx") {
                                    self.u0_doc.u0_document_idx = Int(item.stringValue)!
                                }
                                if(key == "m0_leavetype_idx") {
                                    self.u0_doc.m0_leavetype_idx = Int(item.stringValue)!
                                }
                                if(key == "m0_leavetype_name") {
                                    self.u0_doc.m0_leavetype_name = item.stringValue
                                }
                                if(key == "u0_leave_start") {
                                    self.u0_doc.u0_leave_start = item.stringValue
                                }
                                if(key == "u0_leave_end") {
                                    self.u0_doc.u0_leave_end = item.stringValue
                                }
                                if(key == "u0_leave_calc") {
                                    self.u0_doc.u0_leave_calc = Float(item.stringValue)!
                                }
                                if(key == "u0_leave_comment") {
                                    self.u0_doc.u0_leave_comment = item.stringValue
                                }
                                if(key == "u0_emp_shift") {
                                    self.u0_doc.u0_emp_shift = Int(item.stringValue)!
                                }
                                if(key == "u0_emp_shift_name") {
                                    self.u0_doc.u0_emp_shift_name = item.stringValue
                                }
                                if(key == "m0_node_idx") {
                                    self.u0_doc.m0_node_idx = Int(item.stringValue)!
                                }
                                if(key == "m0_actor_idx") {
                                    self.u0_doc.m0_actor_idx = Int(item.stringValue)!
                                }
                                if(key == "u0_leave_status") {
                                    self.u0_doc.u0_leave_status = Int(item.stringValue)!
                                }
                                
                                if(key == "emp_idx") {
                                    self.u0_doc.emp_idx = Int(item.stringValue)!
                                }
                                if(key == "emp_name") {
                                    self.u0_doc.emp_name = item.stringValue
                                }
                                
                                if(key == "u0_status_name") {
                                    self.u0_doc.u0_status_name = item.stringValue
                                }
                                
                                if(key == "type_list") {
                                    self.u0_doc.type_list = Int(item.stringValue)!
                                }
                            }
                            self.arr_u0_doc += [self.u0_doc]
                        } else if swiftyJsonVar["data_vacation"]["u0_document_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_vacation"]["u0_document_list"].arrayValue {
                                self.u0_doc.u0_document_idx = Int(item["u0_document_idx"].stringValue)!
                                self.u0_doc.m0_leavetype_idx = Int(item["m0_leavetype_idx"].stringValue)!
                                self.u0_doc.m0_leavetype_name = item["m0_leavetype_name"].stringValue
                                self.u0_doc.u0_leave_start = item["u0_leave_start"].stringValue
                                self.u0_doc.u0_leave_end = item["u0_leave_end"].stringValue
                                self.u0_doc.u0_leave_calc = Float(item["u0_leave_calc"].stringValue)!
                                self.u0_doc.u0_leave_comment = item["u0_leave_comment"].stringValue
                                self.u0_doc.u0_emp_shift = Int(item["u0_emp_shift"].stringValue)!
                                self.u0_doc.u0_emp_shift_name = item["u0_emp_shift_name"].stringValue
                                self.u0_doc.m0_node_idx = Int(item["m0_node_idx"].stringValue)!
                                self.u0_doc.m0_actor_idx = Int(item["m0_actor_idx"].stringValue)!
                                self.u0_doc.u0_leave_status = Int(item["u0_leave_status"].stringValue)!
                                
                                self.u0_doc.emp_idx = Int(item["emp_idx"].stringValue)!
                                self.u0_doc.emp_name = item["emp_name"].stringValue
                                
                                self.u0_doc.u0_status_name = item["u0_status_name"].stringValue
                                
                                self.u0_doc.type_list = Int(item["type_list"].stringValue)!
                                
                                self.arr_u0_doc += [self.u0_doc]
                            }
                        }
                    } else {
                        self.alertError(return_msg: return_msg)
                    }
                    // print(responseData.result.value!)
                    
                    // count data and reload
                    if (self.arr_u0_doc.count > 0) {
                        // set back to label view
                        self.tbvList.backgroundView = nil;
                        
                        self.tbvList.reloadData()
                    }
                    else {
                        self.tbvList.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvList.bounds.size.width, height: self.tbvList.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.white
                        
                        // set back to label view
                        self.tbvList.backgroundView = lblEmpty;
                        // no separator
                        self.tbvList.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
//    @IBAction func btnNews(_ sender: UIBarButtonItem) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        appDelegate.window?.rootViewController = viewController
//    }
    // function for event
    
    // go to central
    @IBAction func btnCentral(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    // go to central
    
    // go to create
    @IBAction func btnCreate(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vacTabBarController = storyBoard.instantiateViewController(withIdentifier: "Vac_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = vacTabBarController
        vacTabBarController.selectedIndex = 1 // Vac_CreateViewController
    }
    // go to create
}
