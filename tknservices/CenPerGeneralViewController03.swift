//
//  CenPerGeneralViewController03.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CenPerGeneralViewController03: UIViewController {
    
    @IBOutlet weak var lblEmpAddress: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblProvName: UILabel!
    @IBOutlet weak var lblAmpName: UILabel!
    @IBOutlet weak var lblDistName: UILabel!
    @IBOutlet weak var lblPostCode: UILabel!
    @IBOutlet weak var lblEmpMobileNo: UILabel!
    @IBOutlet weak var lblEmpPhoneNo: UILabel!
    @IBOutlet weak var lblEmpEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_emp_address = defaults.string(forKey: "emp_address") {
            lblEmpAddress.text = ud_emp_address
        }
        if let ud_country_name = defaults.string(forKey: "country_name") {
            lblCountryName.text = ud_country_name
        }
        if let ud_prov_name = defaults.string(forKey: "prov_name") {
            lblProvName.text = ud_prov_name
        }
        if let ud_amp_name = defaults.string(forKey: "amp_name") {
            lblAmpName.text = ud_amp_name
        }
        if let ud_dist_name = defaults.string(forKey: "dist_name") {
            lblDistName.text = ud_dist_name
        }
        if let ud_post_code = defaults.string(forKey: "post_code") {
            lblPostCode.text = ud_post_code
        }
        if let ud_emp_mobile_no = defaults.string(forKey: "emp_mobile_no") {
            lblEmpMobileNo.text = ud_emp_mobile_no
        }
        if let ud_emp_phone_no = defaults.string(forKey: "emp_phone_no") {
            lblEmpPhoneNo.text = ud_emp_phone_no
        }
        if let ud_emp_email = defaults.string(forKey: "emp_email") {
            lblEmpEmail.text = ud_emp_email
        }
    }
}

