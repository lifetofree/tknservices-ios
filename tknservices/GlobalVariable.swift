//
//  GlobalVariable.swift
//  tknservices
//
//  Created by lifetofree on 3/31/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import Foundation
// ## application version ##
let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
let appBuildNo = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
let appLink = "http://bit.ly/2GJ9SJr"

// ## user default ##
var defaults = UserDefaults.standard //collect default value
var sEmpIDX: String = "0"
var sRescIDX: String = "0"
var iEmpIDX: Int = 0
var iRsecIDX: Int = 0
var return_code: Int = 999
var return_msg: String = ""
var emp_name_th: String = "0"

var DocCode: String = ""
var CreateDateUser: String = ""
var TimeCreateJob: String = ""
var EmpName: String = ""
var TelETC: String = ""
var LocName: String = ""
var RemoteName: String = ""
var UserIDRemote: String = ""
var PasswordRemote: String = ""
var Comment: String = ""

var URQIDX: Int = 0
var DateReciveJobFirst: String = ""
var TimeReciveJobFirst: String = ""
var DatecloseJob: String = ""
var TimecloseJob: String = ""
var Name1: String = ""
var Name2: String = ""
var Name3: String = ""
var Name4: String = ""
var Name5: String = ""
var Name6: String = ""
var Name7: String = ""
var Name8: String = ""
var Name9: String = ""
var Priority_name: String = ""
var ManHours: Int = 0
var sumtime: String = ""
var Link: String = ""
var Link_IT: String = ""
var SapMsg: String = ""
var CommentAMDoing: String = ""
var AdminName: String = ""
var AdminDoingName: String = ""

var CMIDX: Int = 0
var CommentAuto: String = ""
var CDate: String = ""
var CTime: String = ""

var unidx: Int = 0
var acidx: Int = 0
var staidx: Int = 0
var RDeptIDX: Int = 0
var SysIDX: Int = 0
var EmpIDX_Create: Int = 0
var approve: Int = 0

var Temp_transfer: Int = 0
var OrgIDX: Int = 0

var UserLogonName: String = ""
var TransactionCode: String = ""

// ## color ## //
var colorBlueDark: UInt = 0x0a5587
var colorGreenButton: UInt = 0x4E9B13
var colorGreenBorder: UInt = 0x3A4B1A
var colorRedButton: UInt = 0xFF372F
var colorRedBorder: UInt = 0x7F0308
var colorOrangeButton: UInt = 0xE79115
var colorOrangeBorder: UInt = 0xA75107
var colorBlueButton: UInt = 0x397FC2
var colorBlueBorder: UInt = 0x26488D
// ## color ## //

// ## web service url ##
var sUrl = "http://services.taokaenoi.co.th/webapi/" // production server
//var sUrl = "http://172.16.11.44/services.taokaenoi.co.th/webapi/" //dev server

// ## upload url ##
var sUploadUrl = "http://mas.taokaenoi.co.th/uploadfiles/";
//var sUploadUrl = "http://172.16.11.5/dev.taokaenoi.co.th/uploadfiles/";

var sUploadUrlVac = "http://www.taokaenoi.co.th/WebSystem/LON/FileUpload/";
//var sUploadUrlVac = "http://172.16.11.44/taokaenoi.co.th/WebSystem/LON/FileUpload/";

//  ## vacation folder ##
var urlVacationImage = sUploadUrl + "vacation/"

//  ## news folder ##
var urlNewsImage = sUploadUrl + "news/"

//  ## ItRepair folder ##
var urlItRepairImage = sUploadUrl + "ITRepair/User/"

// ## service name ##
// # api_employee
// check login
var urlCheckLogin = sUrl + "api_employee.asmx/CheckLogin?jsonIn="
// get my profile
var urlGetMyProfile = sUrl + "api_employee.asmx/GetMyProfile?emp_idx="
// get my profile mobile
var urlGetMyProfileMobile = sUrl + "api_employee.asmx/GetMyProfile_Mobile?emp_idx="
// set my password
var urlSetMyPassword = sUrl + "api_employee.asmx/SetPassword?jsonIn="
// get application version and build
var urlGetAppVersion = sUrl + "api_employee.asmx/VersionMobile?jsonIn="
// # api_employee

// # api_news
var urlGetNewsList = sUrl + "api_news.asmx/GetNewsList?jsonIn="
// # api_news

// # api_vacation
// get leave type list
var urlGetLeaveTypeList = sUrl + "api_vacation.asmx/GetLeaveTypeList?jsonIn="
// get shift time list
var urlGetShfitTimeList = sUrl + "api_vacation.asmx/GetShfitTimeList?jsonIn="

// get my list
var urlGetU0DocumentList = sUrl + "api_vacation.asmx/GetAllList?emp_idx="
// set list status
var urlSetU0DocumentList = sUrl + "api_vacation.asmx/SetListStatus?jsonIn="
// save when create or edit
var urlGetSaveList = sUrl + "api_vacation.asmx/GetSaveList?jsonIn="

// get all approve list
var urlGetU0DocumentApprove = sUrl + "api_vacation.asmx/GetAllApprove?emp_idx="
// set approve status
var urlSetU0DocumentApprove = sUrl + "api_vacation.asmx/SetApproveStatus?jsonIn="

// get my stat
var urlGetMyStat = sUrl + "api_vacation.asmx/GetMyStat?emp_idx="
// # api_vacation

// set GetCen_Member
var urlGetCenMember = sUrl + "api_employee_member.asmx/Set_member?jsonIn="
var urlGetLogin_member = sUrl + "api_employee_member.asmx/get_member?jsonIn="

//# api_support
var urlGetPlace = sUrl + "api_employee.asmx/GetPlace?jsonIn="

var urlGetRemote = sUrl + "api_itrepair.asmx/Select_Remote?jsonIn="

var urlGetChooseOther = sUrl + "api_itrepair.asmx/SelectChooseOther?jsonIn="

var urlInsertItrepair = sUrl + "api_itrepair.asmx/InsertItrepair?jsonIn="

var urlInsertWorkTime = sUrl + "api_itrepair.asmx/InsertWorkTime?jsonIn="

var urlSelect_Holder = sUrl + "api_itrepair.asmx/Select_Holder?jsonIn="

var urlSelect_List = sUrl + "api_itrepair.asmx/Select_List?jsonIn="

var urlSelect_list_mobile = sUrl + "api_itrepair.asmx/Select_List_mobile?jsonIn="

var urlSelect_DetailCloseJobList = sUrl + "api_itrepair.asmx/Select_DetailCloseJobList?jsonIn="

var urlSelect_GvComment_List = sUrl + "api_itrepair.asmx/Select_GvComment_List?jsonIn="

var urlSelect_ddlLV1IT = sUrl + "api_itrepair.asmx/Select_ddlLV1IT?jsonIn="

var urlSelect_ddlLV2IT = sUrl + "api_itrepair.asmx/Select_ddlLV2IT?jsonIn="

var urlSelect_ddlLV3IT = sUrl + "api_itrepair.asmx/Select_ddlLV3IT?jsonIn="

var urlSelect_ddlLV4IT = sUrl + "api_itrepair.asmx/Select_ddlLV4IT?jsonIn="

var urlUpdate_SapGetJob = sUrl + "api_itrepair.asmx/Update_SapGetJob?jsonIn="
var urlUpdate_ITGetJob = sUrl + "api_itrepair.asmx/Update_ITGetJob?jsonIn="
var urlUpdate_GMGetJob = sUrl + "api_itrepair.asmx/Update_GMGetJob?jsonIn="
var urlUpdate_POSGetJob = sUrl + "api_itrepair.asmx/Update_POSGetJob?jsonIn="

var urlSap_CloseJob = sUrl + "api_itrepair.asmx/Sap_CloseJob?jsonIn="
var urlIT_CloseJob = sUrl + "api_itrepair.asmx/IT_CloseJob?jsonIn="
var urlGM_CloseJob = sUrl + "api_itrepair.asmx/GM_CloseJob?jsonIn="
var urlPOS_CloseJob = sUrl + "api_itrepair.asmx/POS_CloseJob?jsonIn="

var urlUpdate_ChangeSystem = sUrl + "api_itrepair.asmx/Update_ChangeSystem?jsonIn="
var urlInsertCommentIT = sUrl + "api_itrepair.asmx/InsertCommentIT?jsonIn="
var urlInsertCommentSAP = sUrl + "api_itrepair.asmx/InsertCommentSAP?jsonIn="
var urlInsertCommentGM = sUrl + "api_itrepair.asmx/InsertCommentGM?jsonIn="
var urlInsertCommentPOS = sUrl + "api_itrepair.asmx/InsertCommentPOS?jsonIn="
var urlInsertITList = sUrl + "api_itrepair.asmx/InsertITList?jsonIn="
var urlInsertInsertSAPList = sUrl + "api_itrepair.asmx/InsertSAPList?jsonIn="
var urlInsertGMList = sUrl + "api_itrepair.asmx/InsertGMList?jsonIn="
var urlInsertPOSList = sUrl + "api_itrepair.asmx/InsertPOSList?jsonIn="

var urlSelect_DetailList = sUrl + "api_itrepair.asmx/Select_DetailList?jsonIn="

var urlSelectCaseLV1POSU0 = sUrl + "api_itrepair.asmx/SelectCaseLV1POSU0?jsonIn="
var urlSelectCaseLV2POSU0 = sUrl + "api_itrepair.asmx/SelectCaseLV2POSU0?jsonIn="
var urlSelectCaseLV3POSU0 = sUrl + "api_itrepair.asmx/SelectCaseLV3POSU0?jsonIn="
var urlSelectCaseLV4POSU0 = sUrl + "api_itrepair.asmx/SelectCaseLV4POSU0?jsonIn="

var urlSelect_ddlLV1 = sUrl + "api_itrepair.asmx/Select_ddlLV1?jsonIn="
var urlSelect_ddlLV2 = sUrl + "api_itrepair.asmx/Select_ddlLV2?jsonIn="
var urlSelect_ddlLV3 = sUrl + "api_itrepair.asmx/Select_ddlLV3?jsonIn="
var urlSelect_ddlLV4 = sUrl + "api_itrepair.asmx/Select_ddlLV4?jsonIn="
var urlSelect_ddlLV5 = sUrl + "api_itrepair.asmx/Select_ddlLV5?jsonIn="

var urlSelect_DocCode = sUrl + "api_itrepair.asmx/Select_DocCode?jsonIn="
//# api_support

// # api_itrepair
var urlGetSuggestionList = sUrl + "api_itrepair.asmx/Get_requestlist?jsonIn="
var urlSetSuggestionList = sUrl + "api_itrepair.asmx/Insert_requestlist?jsonIn="
// # api_itrepair

// # api_chr
var urlGetChrCeoList = sUrl + "api_chr.asmx/Select_CHR_CEO?jsonIn="
var urlSetChrCeoApprove = sUrl + "api_chr.asmx/Update_Approver?jsonIn="
var urlGetChrScList = sUrl + "api_chr.asmx/Select_ListBeforeCEO?jsonIn="
var urlSetChrScApprove = sUrl + "api_chr.asmx/Select_Approve_CHR?jsonIn="
var urlGetChrDetail = sUrl + "api_chr.asmx/Select_DetailCHR?jsonIn="
// # api_chr

// # api_telephone
var urlGetTelephoneListU0 = sUrl + "api_telephone.asmx/GetTelephoneListU0?jsonIn="
var urlGetTelephoneListU1 = sUrl + "api_telephone.asmx/GetTelephoneListU1?jsonIn="
var urlGetTelephoneListU2 = sUrl + "api_telephone.asmx/GetTelephoneListU2?jsonIn="
// # api_telephone
