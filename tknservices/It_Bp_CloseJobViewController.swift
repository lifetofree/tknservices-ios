//
//  It_Bp_CloseJobViewController.swift
//  TKNServices
//
//  Created by lifetofree on 7/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bp_CloseJobViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tfLv1: UITextField!
    @IBOutlet weak var tfLv2: UITextField!
    @IBOutlet weak var tfLv3: UITextField!
    @IBOutlet weak var tfLv4: UITextField!
    @IBOutlet weak var tfLv5: UITextField!
    @IBOutlet weak var tfPriority_name: UITextField!
    @IBOutlet weak var tfManHours: UITextField!
    @IBOutlet weak var tfSapmsg: UITextField!
    @IBOutlet weak var tfLink: UITextField!
    @IBOutlet weak var tfCommentAMDoing: UITextField!
    
    var pvLv1 = UIPickerView()
    var pvLv2 = UIPickerView()
    var pvLv3 = UIPickerView()
    var pvLv4 = UIPickerView()
    var pvLv5 = UIPickerView()
    
    var Lv_detail = UserRequest_detail()
    var arr_Lv_detail = [UserRequest_detail]()
    var arr_Lv_2_detail = [UserRequest_detail]()
    var arr_Lv_3_detail = [UserRequest_detail]()
    var arr_Lv_4_detail = [UserRequest_detail]()
    var arr_Lv_5_detail = [UserRequest_detail]()
    
    var dataLv1: [String] = []
    var dataLv2: [String] = []
    var dataLv3: [String] = []
    var dataLv4: [String] = []
    var dataLv5: [String] = []
    
    var name_lv1: String = ""
    var name_lv2: String = ""
    var name_lv3: String = ""
    var name_lv4: String = ""
    var PIDX_Add: Int = 0
    
    var dataSystemClose = ["ด่วน","ไม่ด่วน","ปานกลาง"]
    var pvClose = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- PickerView Requried -----//
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(pickerView.tag) {
        case 1:
            return self.dataLv1.count
        case 2:
            return self.dataLv2.count
        case 3:
            return self.dataLv3.count
        case 4:
            return self.dataLv4.count
        case 5:
            return self.dataLv5.count
        default:
            return self.dataSystemClose.count
        }
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(pickerView.tag) {
        case 1:
            return self.dataLv1[row]
        case 2:
            return self.dataLv2[row]
        case 3:
            return self.dataLv3[row]
        case 4:
            return self.dataLv4[row]
        case 5:
            return self.dataLv5[row]
        default:
            return self.dataSystemClose[row]
        }
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch(pickerView.tag) {
        case 1:
            tfLv1.text = self.dataLv1[row]
            self.name_lv1 = self.dataLv1[row]
            tfLv2.text = ""
            tfLv3.text = ""
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv2()
        case 2:
            tfLv2.text = self.dataLv2[row]
            self.name_lv2 = self.dataLv2[row]
            tfLv3.text = ""
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv3()
        case 3:
            tfLv3.text = self.dataLv3[row]
            self.name_lv3 = self.dataLv3[row]
            tfLv4.text = ""
            tfLv5.text = ""
            ddlLv4()
        case 4:
            tfLv4.text = self.dataLv4[row]
            self.name_lv4 = self.dataLv4[row]
            tfLv5.text = ""
            ddlLv5()
        case 5:
            tfLv5.text = self.dataLv5[row]
        default:
            tfPriority_name.text = dataSystemClose[row]
        }
        
        self.view.endEditing(true)
    }
    //----- PickerView Requried -----//
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        setDataDetail()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    //----- Event Button -----//
    
    //----- Reuse -----//
    func initView() {
        view_content.layer.cornerRadius = 10
        
        tfSapmsg.delegate = self
        tfSapmsg.returnKeyType = .done
        
        tfLink.delegate = self
        tfLink.returnKeyType = .done
        
        tfCommentAMDoing.delegate = self
        tfCommentAMDoing.returnKeyType = .done
        
        // done button on number pad
        self.addDoneButtonOnKeyboard()
        
        // hide keyboard when tab outside
        self.hideKeyboardWhenTappedAround()
        
        // set picker view
        pvLv1.delegate = self
        pvLv1.dataSource = self
        pvLv1.tag = 1
        tfLv1.inputView = pvLv1
        
        pvLv2.delegate = self
        pvLv2.dataSource = self
        pvLv2.tag = 2
        tfLv2.inputView = pvLv2
        
        pvLv3.delegate = self
        pvLv3.dataSource = self
        pvLv3.tag = 3
        tfLv3.inputView = pvLv3
        
        pvLv4.delegate = self
        pvLv4.dataSource = self
        pvLv4.tag = 4
        tfLv4.inputView = pvLv4
        
        pvLv5.delegate = self
        pvLv5.dataSource = self
        pvLv5.tag = 5
        tfLv5.inputView = pvLv5
        // set picker view
        
        // set picker view
        pvClose.delegate = self
        pvClose.dataSource = self
        pvClose.tag = 6
        tfPriority_name.inputView = pvClose
        
        ddlLv1()
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bp_CloseJobViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_Bp_CloseJobViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // setDataDetail
    func setDataDetail() {
        if(tfLv1.text! == "" || tfLv2.text! == "" || tfLv3.text! == "" || tfLv4.text! == "" || tfLv5.text! == "" || tfPriority_name.text! == "" || tfManHours.text! == "" || tfSapmsg.text! == "" || tfLink.text! == "" || tfCommentAMDoing.text! == ""){
            alertError(return_msg: "กรุณากรอกข้อมุลให้ครบถ้วนค่ะ")
        }
        else{
            urlUpdate()
            urlCloseJob_Admin()
            goBack()
        }
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 2 && acidx == 1){
            if(approve == 5){
                staidx = 24
            }
            else{
                staidx = 23
            }
        }
        
        if(tfPriority_name.text! == "ด่วน"){
            PIDX_Add = 1
        }
        else if(tfPriority_name.text! == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else if(tfPriority_name.text! == "ปานกลาง"){
            PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                        "PIDX_Add" : PIDX_Add,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_SapGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get urlCloseJob_Admin
    func urlCloseJob_Admin() {
        
        if(unidx == 4 && acidx == 3){
            staidx = 22
        }
        
        if(tfPriority_name.text! == "ด่วน"){
            PIDX_Add = 1
        }
        else if(tfPriority_name.text! == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else{
            PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "MS1IDX" : 0,
                        "MS2IDX" : 0,
                        "MS3IDX" : 0,
                        "MS4IDX" : 0,
                        "MS5IDX" : 0,
                        "Name_Code1" : tfLv1.text!,
                        "Name_Code2" : tfLv2.text!,
                        "Name_Code3" : tfLv3.text!,
                        "Name_Code4" : tfLv4.text!,
                        "Name_Code5" : tfLv5.text!,
                        "PIDX_Add" : PIDX_Add,
                        "ManHours" : tfManHours.text!,
                        "Link" : tfLink.text!,
                        "SapMsg" : tfSapmsg.text!,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "CommentAMDoing" : tfCommentAMDoing.text!,
                        "CCAIDX" : 0,
                        "RecieveIDX" : 0,
                        "EmpIDX_add" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "StaIDX" : 1,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSap_CloseJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // get ddlLv1
    func ddlLv1() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 0,
                        "SysIDX_add" : 23
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV1 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        // print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS1IDX") {
                                self.Lv_detail.MS1IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code1") {
                                self.Lv_detail.Name_Code1 = item.stringValue
                            }
                        }
                        self.arr_Lv_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS1IDX = Int(item["MS1IDX"].stringValue)!
                            self.Lv_detail.Name_Code1 = item["Name_Code1"].stringValue
                            
                            self.arr_Lv_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_detail.count {
                        if(self.arr_Lv_detail[i].Name_Code1 != ""){
                            self.dataLv1 += [self.arr_Lv_detail[i].Name_Code1]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv2
    func ddlLv2() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "SysIDX_add" : 23,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV2 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_2_detail.removeAll()
        self.dataLv2.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS2IDX") {
                                self.Lv_detail.MS2IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code2") {
                                self.Lv_detail.Name_Code2 = item.stringValue
                            }
                        }
                        self.arr_Lv_2_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS2IDX = Int(item["MS2IDX"].stringValue)!
                            self.Lv_detail.Name_Code2 = item["Name_Code2"].stringValue
                            
                            self.arr_Lv_2_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_2_detail.count {
                        if(self.arr_Lv_2_detail[i].Name_Code2 != ""){
                            self.dataLv2 += [self.arr_Lv_2_detail[i].Name_Code2]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv3
    func ddlLv3() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "SysIDX_add" : 23,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV3 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_3_detail.removeAll()
        self.dataLv3.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS3IDX") {
                                self.Lv_detail.MS3IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code3") {
                                self.Lv_detail.Name_Code3 = item.stringValue
                            }
                        }
                        self.arr_Lv_3_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS3IDX = Int(item["MS3IDX"].stringValue)!
                            self.Lv_detail.Name_Code3 = item["Name_Code3"].stringValue
                            
                            self.arr_Lv_3_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_3_detail.count {
                        if(self.arr_Lv_3_detail[i].Name_Code3 != ""){
                            self.dataLv3 += [self.arr_Lv_3_detail[i].Name_Code3]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv4
    func ddlLv4() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "SysIDX_add" : 23,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV4 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        
        self.arr_Lv_4_detail.removeAll()
        self.dataLv4.removeAll()
        
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS4IDX") {
                                self.Lv_detail.MS4IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code4") {
                                self.Lv_detail.Name_Code4 = item.stringValue
                            }
                        }
                        self.arr_Lv_4_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS4IDX = Int(item["MS4IDX"].stringValue)!
                            self.Lv_detail.Name_Code4 = item["Name_Code4"].stringValue
                            
                            self.arr_Lv_4_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_4_detail.count {
                        if(self.arr_Lv_4_detail[i].Name_Code4 != ""){
                            self.dataLv4 += [self.arr_Lv_4_detail[i].Name_Code4]
                        }
                    }
                    
                }
        }
    }
    
    // get ddlLv5
    func ddlLv5() {
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "MS1IDX" : 1,
                        "SysIDX_add" : 23,
                        "ChkAdaccept" : self.name_lv1, //ddlCloseJobSap
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlSelect_ddlLV5 + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    return_msg = swiftyJsonVar["DataSupportIT"]["ReturnMsg"].stringValue
                    // check return_code
                    //if (return_code == 0) {
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "MS5IDX") {
                                self.Lv_detail.MS5IDX = Int(item.stringValue)!
                            }
                            if(key == "Name_Code5") {
                                self.Lv_detail.Name_Code5 = item.stringValue
                            }
                        }
                        self.arr_Lv_detail += [self.Lv_detail]
                    }
                    else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.Lv_detail.MS5IDX = Int(item["MS5IDX"].stringValue)!
                            self.Lv_detail.Name_Code5 = item["Name_Code5"].stringValue
                            
                            self.arr_Lv_detail += [self.Lv_detail]
                            
                        }
                    }
                    
                    for i in 0 ..< self.arr_Lv_detail.count {
                        if(self.arr_Lv_detail[i].Name_Code5 != ""){
                            self.dataLv5 += [self.arr_Lv_detail[i].Name_Code5]
                        }
                    }
                    
                }
        }
    }
    
    // alertError
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpTabBarController
        itBpTabBarController.selectedIndex = 0 //
    }
    
    // TextField resign first responder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // done button on number pad
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(It_Bi_CloseJobViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.tfManHours.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.tfManHours.resignFirstResponder()
    }
    // done button on number pad
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    // function move textfield up when keyboard appears
    //----- Reuse -----//
}
