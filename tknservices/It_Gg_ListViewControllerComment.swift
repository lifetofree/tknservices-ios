//
//  It_Gg_ListViewControllerComment.swift
//  tknservices
//
//  Created by lifetofree on 7/25/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Gg_ListViewControllerComment: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet weak var tvcItGgComment: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tfComment: UITextField!
    
    var refreshControl = UIRefreshControl()
    
    var u0_userre = UserRequest_detail()
    var arr_u0_userre = [UserRequest_detail]()
    
    var u0_userre_selected = UserRequest_detail()
    
    let textCellIdentifier = "tvcItGgComment"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set table view w/ transparent background
        self.tvcItGgComment.backgroundColor = UIColor.clear
        
        tvcItGgComment.delegate = self
        tvcItGgComment.dataSource = self
        
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(It_It_ListViewControllerComment.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tvcItGgComment.addSubview(refreshControl)
        
        
        // create tool bar for date picker
        let toolBarComment = UIToolbar()
        toolBarComment.barStyle = .default
        toolBarComment.isTranslucent = true
        toolBarComment.sizeToFit()
        
        
        let doneButtonComment = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerComment))
        let spaceButtonComment = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonComment = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPickerComment))
        
        toolBarComment.setItems([cancelButtonComment,spaceButtonComment, doneButtonComment], animated: false)
        toolBarComment.isUserInteractionEnabled = true
        
        tfComment.inputAccessoryView = toolBarComment
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton
            , borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
        
        self.getAllList()
        
        // move textfield up when keyboard appears
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(It_It_CreateViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // click done button
    func donePickerComment()
    {
        tfComment.resignFirstResponder()
    }
    func CancelPickerComment()
    {
        tfComment.text = ""
        tfComment.resignFirstResponder()
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_userre.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: It_Gg_CustomCellList = tvcItGgComment.dequeueReusableCell(withIdentifier: textCellIdentifier) as! It_Gg_CustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _FullNameTH = arr_u0_userre[row].FullNameTH
        let _CreateDateUser = arr_u0_userre[row].CDate + " " + arr_u0_userre[row].CTime
        let _CommentAuto = arr_u0_userre[row].CommentAuto
        
        // load item
        cell.loadItem_Comment(FullNameTH: _FullNameTH, CommentAuto: _CommentAuto, CDate: _CreateDateUser)
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        u0_userre_selected = arr_u0_userre[row]
    }
    
    func getAllList() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        // print(sEmpIDX)
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "SysIDX_add" : 1,
                        "URQIDX" : URQIDX,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        
        let tempUrl = String(urlSelect_GvComment_List + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    //return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            //if(key == "CMIDX") {
                            //self.u0_userre.CMIDX = Int(item.stringValue)!
                            //}
                            if(key == "FullNameTH") {
                                self.u0_userre.FullNameTH = item.stringValue
                            }
                            if(key == "CommentAuto") {
                                self.u0_userre.CommentAuto = item.stringValue
                            }
                            if(key == "CDate") {
                                self.u0_userre.CDate = item.stringValue
                            }
                            if(key == "CTime") {
                                self.u0_userre.CTime = item.stringValue
                            }
                            
                        }
                        self.arr_u0_userre += [self.u0_userre]
                    } else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            //self.u0_userre.CMIDX = Int(item["CMIDX"].stringValue)!
                            self.u0_userre.FullNameTH = item["FullNameTH"].stringValue
                            self.u0_userre.CommentAuto = item["CommentAuto"].stringValue
                            self.u0_userre.CDate = item["CDate"].stringValue
                            self.u0_userre.CTime = item["CTime"].stringValue
                            
                            self.arr_u0_userre += [self.u0_userre]
                        }
                    }
                    
                    // count data and reload
                    if (self.arr_u0_userre.count > 0) {
                        // set back to label view
                        self.tvcItGgComment.backgroundView = nil;
                        
                        self.tvcItGgComment.reloadData()
                    }
                    else {
                        self.tvcItGgComment.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tvcItGgComment.bounds.size.width, height: self.tvcItGgComment.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        lblEmpty.font = UIFont(name: "kanit-regular", size: 15.0)
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray
                        
                        // set back to label view
                        self.tvcItGgComment.backgroundView = lblEmpty;
                        // no separator
                        self.tvcItGgComment.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    // get urlUpdate
    func urlUpdateComment() {
        
        if(tfComment.text! != ""){
            
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "EmpIDX_add" : Int(sEmpIDX)!,
                        "CommentAuto" : tfComment.text!,
                        "CStatus" : 1,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlInsertCommentGM + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    // function move textfield up when keyboard appears
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    
    @IBAction func btnSave(_ sender: UIButton) {
        
        urlUpdateComment()
        
        /*let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         let ItItListViewControllerSelected = storyBoard.instantiateViewController(withIdentifier: "It_It_ListViewControllerSelected")
         
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         
         appDelegate.window?.rootViewController = ItItListViewControllerSelected*/
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
        
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
    }
}
