//
//  It_Sap_ReceiveJobViewController.swift
//  tknservices
//
//  Created by lifetofree on 8/22/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Sap_ReceiveJobViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblCloseReceive: UITextField!
    
    var dataSystemClose = ["ด่วน","ไม่ด่วน","ปานกลาง"]
    var pvClose = UIPickerView()
    
    var PIDX_Add: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        // set picker view
        pvClose.delegate = self
        pvClose.dataSource = self
        pvClose.tag = 1
        lblCloseReceive.text = dataSystemClose[0]
        lblCloseReceive.inputView = pvClose
        
        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get urlUpdate
    func urlUpdate() {
        
        if(unidx == 2 && acidx == 1){
            if(approve == 5){
                staidx = 24
            }
            else{
                staidx = 23
            }
        }
        
        if(lblCloseReceive.text! == "ด่วน"){
            PIDX_Add = 1
        }
        else if(lblCloseReceive.text! == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else if(lblCloseReceive.text! == "ปานกลาง"){
            PIDX_Add = 3
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "unidx" : unidx,
                        "acidx" : acidx,
                        "StaIDX" : staidx,
                        "PIDX_Add" : PIDX_Add,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_SapGetJob + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //if(pickerView.tag == 1) {
        return dataSystemClose.count
        //}
    }
    
    // From the UIPickerViewDataSource protocol.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //if(pickerView.tag == 1) {
        return dataSystemClose[row]
        //}
    }
    
    // From the UIPickerViewDelegate protocol.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //if(pickerView.tag == 1) {
        lblCloseReceive.text = dataSystemClose[row]
        //TempCloseReceive = lblCloseReceive.text!
        //}
        self.view.endEditing(true)
    }
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        
        urlUpdate()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 // It_Sap_ListViewController
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 0
        
    }
    
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnBack(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itSapTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Sap_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itSapTabBarController
        itSapTabBarController.selectedIndex = 0 //
        
    }
    //----- Navigation Bar Button -----//
}
