//
//  It_Bp_ListViewControllerDetail.swift
//  TKNServices
//
//  Created by lifetofree on 7/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bp_ListViewControllerDetail: UIViewController {
    
    @IBOutlet weak var btnReceiveJob: UIButton!
    @IBOutlet weak var btnTransferJob: UIButton!
    @IBOutlet weak var btnCloseJob: UIButton!
    @IBOutlet weak var btnUserCloseJob: UIButton!
    
    @IBOutlet weak var lblDocCode: UILabel!
    @IBOutlet weak var lblCreateDateTime: UILabel!
    @IBOutlet weak var lblEmpName_Create: UILabel!
    @IBOutlet weak var lblTel: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblUserLogon: UILabel!
    @IBOutlet weak var lblTcode: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    var rdept_idx: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- Event Button -----//
    @IBAction func btnReceiveJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpReceiveJobViewController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_ReceiveJobViewController") as! It_Bp_ReceiveJobViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpReceiveJobViewController
    }
    
    @IBAction func btnTransferJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpJobTransferViewController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_JobTransferViewController") as! It_Bp_JobTransferViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpJobTransferViewController
    }
    
    @IBAction func btnCloseJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpCloseJobViewController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_CloseJobViewController") as! It_Bp_CloseJobViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpCloseJobViewController
    }
    
    @IBAction func btnUserCloseJob(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBpCloseJobUViewController = storyBoard.instantiateViewController(withIdentifier: "It_Bp_CloseJobUViewController") as! It_Bp_CloseJobUViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBpCloseJobUViewController
    }
    //----- Event Button -----//
    
    //----- Reuse -----//
    func initView() {
        if let ud_rdept_idx = defaults.string(forKey: "rdept_idx") {
            rdept_idx = Int(ud_rdept_idx)!
        }
        // print("rdept_idx : " + String(rdept_idx))
        
        // set button
        btnReceiveJob.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnTransferJob.roundButton(rad: 5.0, buttonColor: colorOrangeButton, borderColor: colorOrangeBorder, borderWidth: 1.0)
        btnCloseJob.roundButton(rad: 5.0, buttonColor: colorBlueButton, borderColor: colorBlueBorder, borderWidth: 1.0)
        btnUserCloseJob.roundButton(rad: 5.0, buttonColor: colorBlueButton, borderColor: colorBlueBorder, borderWidth: 1.0)
        // set button
        
        setDetail()
    }
    
    // set detail
    func setDetail() {
        lblDocCode.text = DocCode
        lblCreateDateTime.text = CreateDateUser + " " + TimeCreateJob
        lblEmpName_Create.text = EmpName
        lblTel.text = TelETC
        lblLocation.text = LocName
        lblUserLogon.text = UserLogonName
        lblTcode.text = TransactionCode
        lblPriority.text = Priority_name
        lblComment.text = Comment
        
        if(acidx == 1){ //ผู้สร้าง
            if(unidx == 2 && Int(sEmpIDX)! == EmpIDX_Create) {
                btnReceiveJob.isHidden = true
                btnTransferJob.isHidden = true
                btnCloseJob.isHidden = true
                btnUserCloseJob.isHidden = false
            }
            else{
                btnReceiveJob.isHidden = true
                btnTransferJob.isHidden = true
                btnCloseJob.isHidden = true
                btnUserCloseJob.isHidden = true
            }
        }
        else if(acidx == 2){ //เจ้าหน้าที่ IT
            if(unidx == 6 && (rdept_idx == 20 || rdept_idx == 21)) {
                btnReceiveJob.isHidden = false
                btnTransferJob.isHidden = false
                btnCloseJob.isHidden = true
                btnUserCloseJob.isHidden = true
            } else if(unidx == 3 && (rdept_idx == 20 || rdept_idx == 21)) {
                btnReceiveJob.isHidden = true
                btnTransferJob.isHidden = true
                btnCloseJob.isHidden = false
                btnUserCloseJob.isHidden = true
            } else {
                btnReceiveJob.isHidden = true
                btnTransferJob.isHidden = true
                btnCloseJob.isHidden = true
                btnUserCloseJob.isHidden = true
            }
        } else {
            btnReceiveJob.isHidden = true
            btnTransferJob.isHidden = true
            btnCloseJob.isHidden = true
            btnUserCloseJob.isHidden = true
        }
    }
    //----- Reuse -----//
}
