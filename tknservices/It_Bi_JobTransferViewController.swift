//
//  It_Bi_JobTransferViewController.swift
//  TKNServices
//
//  Created by lifetofree on 7/10/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bi_JobTransferViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
        @IBOutlet weak var view_content: UIView!
        @IBOutlet weak var btnSave: UIButton!
        @IBOutlet weak var btnCancel: UIButton!
        @IBOutlet weak var tfStatus: UITextField!
    
        var dataSystemTransfer = ["Google Apps","IT","BP"]
        var pvSystem = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- PickerView Requried -----//
        // returns the number of 'columns' to display.
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
    
        // returns the # of rows in each component.
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return dataSystemTransfer.count
        }
    
        // From the UIPickerViewDataSource protocol.
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return dataSystemTransfer[row]
        }
    
        // From the UIPickerViewDelegate protocol.
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            tfStatus.text = dataSystemTransfer[row]
            self.view.endEditing(true)
        }
    //----- PickerView Requried -----//
    
    //----- Event Button -----//
    @IBAction func btnSave(_ sender: UIButton) {
        setDataDetail()
        goBack()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        goBack()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        goBack()
    }
    //----- Event Button -----//
    
    //----- Reuse -----//
    func initView() {
        view_content.layer.cornerRadius = 10

        // set picker view
        pvSystem.delegate = self
        pvSystem.dataSource = self
        pvSystem.tag = 1
        tfStatus.text = dataSystemTransfer[0]
        tfStatus.inputView = pvSystem

        // set button
        btnSave.roundButton(rad: 5.0, buttonColor: colorGreenButton, borderColor: colorGreenBorder, borderWidth: 1.0)
        btnCancel.roundButton(rad: 5.0, buttonColor: colorRedButton, borderColor: colorRedBorder, borderWidth: 1.0)
        // set button
    }
    
    // setDataDetail
    func setDataDetail() {
        switch (tfStatus.text!) {
        case "Google Apps":
            Temp_transfer = 1
        case "SAP":
            Temp_transfer = 2
        case "IT":
            Temp_transfer = 3
        default:
            Temp_transfer = 23 //BP
        }
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "URQIDX" : URQIDX,
                        "AdminIDX" : Int(sEmpIDX)!,
                        "AdminDoingIDX" : Int(sEmpIDX)!,
                        "StaIDX" : 0,
                        "SysIDX_add" : Temp_transfer,
                        "OrgIDX" : OrgIDX,
                        "CommentAMDoing" : "โอนย้ายไประบบ" + tfStatus.text!,
                        "acidx" : acidx,
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlUpdate_ChangeSystem + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        // set data and url
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                }
        }
    }
    
    func goBack() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itBiTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Bi_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itBiTabBarController
        itBiTabBarController.selectedIndex = 0 //
    }
    //----- Reuse -----//
}
