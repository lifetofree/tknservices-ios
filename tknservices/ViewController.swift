//
//  ViewController.swift
//  tknservices
//
//  Created by lifetofree on 3/31/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var refreshControl = UIRefreshControl()
    
    var return_code: Int = 999
    var return_msg: String = ""
    
    var u0_news = news_u0_news_detail()
    var arr_u0_news = [news_u0_news_detail]()
    
    @IBOutlet weak var tbvNews: UITableView!
    let textCellIdentifier = "tvcNews"
    
    
    var data_selected = news_u0_news_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // custom background
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg_blue")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        // set table view w/ transparent background
        self.tbvNews.backgroundColor = UIColor.clear
//
        tbvNews.delegate = self
        tbvNews.dataSource = self
//        
        let attr = [NSForegroundColorAttributeName:UIColor.white]
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.white
        self.tbvNews.addSubview(refreshControl)
//        
        self.getAllNews()
        
        //-- temp --//
        /*let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginController*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_news.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: News_CustomCellList = tbvNews.dequeueReusableCell(withIdentifier: textCellIdentifier) as! News_CustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _news_uidx = arr_u0_news[row].uidx
        let _news_title = arr_u0_news[row].news_title
        let _news_author = arr_u0_news[row].news_author
        let _update_date = arr_u0_news[row].update_date
        
        // load item
        cell.loadItem(news_uidx: _news_uidx, news_title: _news_title, news_author: _news_author, update_date: _update_date)
        
        // set cell background
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: Int(cell.frame.width), height: Int(cell.frame.height)))
        let image = UIImage(named: "bg_row_list")
        imageView.image = image
        cell.backgroundView = UIView()
        cell.backgroundView!.addSubview(imageView)
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        data_selected = arr_u0_news[row]
    }
    
    //--- connected to other view ---//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        let iIndexPath : IndexPath = self.tbvNews.indexPathForSelectedRow! as IndexPath
    
        let detailView = segue.destination as! News_ListViewControllerSelected
    
        data_selected = arr_u0_news[iIndexPath.row]
        detailView.val_selected = data_selected
    }
    
    // function for event
    // get all approve
    func getAllNews() {
        // set data and url
        let value =
            [
                "data_news" : []
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        let tempUrl = String(urlGetNewsList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    self.return_code = Int(swiftyJsonVar["data_news"]["return_code"].stringValue)!
                    self.return_msg = swiftyJsonVar["data_news"]["return_msg"].stringValue
                    
                    // check return_code
                    if (self.return_code == 0) {
                        self.arr_u0_news.removeAll()
                        // get value
                        if swiftyJsonVar["data_news"]["u0_news_list"].dictionaryObject != nil {
                            for (key, item) in swiftyJsonVar["data_news"]["u0_news_list"].dictionaryValue {
                                if(key == "uidx") {
                                    self.u0_news.uidx = Int(item.stringValue)!
                                }
                                if(key == "news_title") {
                                    self.u0_news.news_title = item.stringValue
                                }
                                if(key == "news_detail") {
                                    self.u0_news.news_detail = item.stringValue
                                }
                                if(key == "news_author") {
                                    self.u0_news.news_author = item.stringValue
                                }
                                if(key == "update_date") {
                                    self.u0_news.update_date = item.stringValue
                                }
                            }
                            self.arr_u0_news += [self.u0_news]
                        } else if swiftyJsonVar["data_news"]["u0_news_list"].arrayObject != nil {
                            for (item) in swiftyJsonVar["data_news"]["u0_news_list"].arrayValue {
                                self.u0_news.uidx = Int(item["uidx"].stringValue)!
                                self.u0_news.news_title = item["news_title"].stringValue
                                self.u0_news.news_detail = item["news_detail"].stringValue
                                self.u0_news.news_author = item["news_author"].stringValue
                                self.u0_news.update_date = item["update_date"].stringValue
                                
                                self.arr_u0_news += [self.u0_news]
                            }
                        }
                    } else {
                        self.alertError(return_msg: self.return_msg)
                    }
                    // print(responseData.result.value!)
                    
                    // count data and reload
                    if (self.arr_u0_news.count > 0) {
                        self.tbvNews.reloadData()
                    }
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllNews()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            //            print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    // function for event
    
    // go to guest central
    @IBAction func btnGuestHome(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "GuestCentralViewController") as! GuestCentralViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginController
    }
    // go to guest central
}

