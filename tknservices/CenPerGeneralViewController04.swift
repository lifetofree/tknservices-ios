//
//  CenPerGeneralViewController04.swift
//  tknservices
//
//  Created by lifetofree on 3/16/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class CenPerGeneralViewController04: UIViewController {
    
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblBGName: UILabel!
    @IBOutlet weak var lblScar: UILabel!
    @IBOutlet weak var lblSocHosName: UILabel!
    @IBOutlet weak var lblSocNo: UILabel!
    @IBOutlet weak var lblSocDateExpired: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        getMyProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // get user data
    func getMyProfile() {
        if let ud_height = defaults.string(forKey: "Height") {
            lblHeight.text = ud_height
        }
        if let ud_weight = defaults.string(forKey: "Weight") {
            lblWeight.text = ud_weight
        }
        if let ud_bg_name = defaults.string(forKey: "BGName") {
            lblBGName.text = ud_bg_name
        }
        if let ud_scar = defaults.string(forKey: "Scar") {
            lblScar.text = ud_scar
        }
        if let ud_soc_hos_name = defaults.string(forKey: "SocHosName") {
            lblSocHosName.text = ud_soc_hos_name
        }
        if let ud_soc_no = defaults.string(forKey: "SocNo") {
            lblSocNo.text = ud_soc_no
        }
        if let ud_soc_date_expired = defaults.string(forKey: "SocDateExpired") {
            lblSocDateExpired.text = ud_soc_date_expired
        }
    }
}
