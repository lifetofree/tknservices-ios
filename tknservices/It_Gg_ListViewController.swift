//
//  It_Gg_ListViewController.swift
//  tknservices
//
//  Created by lifetofree on 7/21/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Gg_ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var view_content: UIView!
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tbvList: UITableView!
    let textCellIdentifier = "tvcList"
    
    var u0_userre = UserRequest_detail()
    var arr_u0_userre = [UserRequest_detail]()
    
    var u0_userre_selected = UserRequest_detail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_content.layer.cornerRadius = 10
        
        self.tbvList.backgroundColor = UIColor.clear
        
        tbvList.delegate = self
        tbvList.dataSource = self
        
        let attr = [NSForegroundColorAttributeName:UIColor.darkGray]
        refreshControl.addTarget(self, action: #selector(It_Gg_ListViewController.handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attr)
        refreshControl.tintColor = UIColor.darkGray
        self.tbvList.addSubview(refreshControl)
        
        self.getAllList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_u0_userre.count
    }
    
    //--- required --//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: It_Gg_CustomCellList = tbvList.dequeueReusableCell(withIdentifier: textCellIdentifier) as! It_Gg_CustomCellList
        
        let row = (indexPath as NSIndexPath).row
        
        // set value
        let _DocCode = arr_u0_userre[row].DocCode
        let _CreateDateUser = arr_u0_userre[row].CreateDateUser + " " + arr_u0_userre[row].TimeCreateJob
        let _EmpName = arr_u0_userre[row].EmpName
        let _RDeptName = arr_u0_userre[row].RDeptName
        let _LocName = arr_u0_userre[row].LocName
        let _detailUser = arr_u0_userre[row].detailUser
        let _Status = arr_u0_userre[row].StaName
        
        // load item
        cell.loadItem(DocCode: _DocCode, CreateDateUser: _CreateDateUser, EmpName: _EmpName, RDeptName: _RDeptName, LocName: _LocName, detailUser: _detailUser, Status: _Status, ImageStatus: _Status )
        
        // set cell selection style
        cell.selectionStyle = .none
        
        return cell
    }
    
    //--- optional for clickable ---//
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        u0_userre_selected = arr_u0_userre[row]
    }
    
    //--- connected to other view ---//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let iIndexPath : IndexPath = self.tbvList.indexPathForSelectedRow! as IndexPath
        
        let detailView = segue.destination as! It_Gg_ListViewControllerSelected
        
        u0_userre_selected = arr_u0_userre[iIndexPath.row]
        detailView.user_selected = u0_userre_selected
    }
    
    //--- Select to view It_Repair ---//
    func getAllList() {
        if let ud_emp_idx = defaults.value(forKey: "emp_idx") {
            sEmpIDX = ud_emp_idx as! String
        }
        // print(sEmpIDX)
        
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        "EmpIDX_add" : sEmpIDX,
                        "SysIDX_add" : 1, //GM
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        
        let tempUrl = String(urlSelect_list_mobile + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    //return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "DocCode") {
                                self.u0_userre.DocCode = item.stringValue
                            }
                            if(key == "CreateDateUser") {
                                self.u0_userre.CreateDateUser = item.stringValue
                            }
                            if(key == "TimeCreateJob") {
                                self.u0_userre.TimeCreateJob = item.stringValue
                            }
                            if(key == "EmpName") {
                                self.u0_userre.EmpName = item.stringValue
                            }
                            if(key == "RDeptName") {
                                self.u0_userre.RDeptName = item.stringValue
                            }
                            if(key == "LocName") {
                                self.u0_userre.LocName = item.stringValue
                            }
                            if(key == "detailUser") {
                                self.u0_userre.detailUser = item.stringValue
                            }
                            if(key == "MobileNo") {
                                self.u0_userre.MobileNo = item.stringValue
                            }
                            if(key == "TelETC") {
                                self.u0_userre.TelETC = item.stringValue
                            }
                            if(key == "RemoteName") {
                                self.u0_userre.RemoteName = item.stringValue
                            }
                            if(key == "UserIDRemote") {
                                self.u0_userre.UserIDRemote = item.stringValue
                            }
                            if(key == "PasswordRemote") {
                                self.u0_userre.PasswordRemote = item.stringValue
                            }
                            if(key == "URQIDX") {
                                self.u0_userre.URQIDX = Int(item.stringValue)!
                            }
                            if(key == "unidx") {
                                self.u0_userre.unidx = Int(item.stringValue)!
                            }
                            if(key == "acidx") {
                                self.u0_userre.acidx = Int(item.stringValue)!
                            }
                            if(key == "RDeptIDX") {
                                self.u0_userre.RDeptIDX = Int(item.stringValue)!
                            }
                            if(key == "EmpIDX") {
                                self.u0_userre.EmpIDX = Int(item.stringValue)!
                            }
                            if(key == "OrgIDX") {
                                self.u0_userre.OrgIDX = Int(item.stringValue)!
                            }
                            if(key == "StaName") {
                                self.u0_userre.StaName = item.stringValue
                            }
                        }
                        self.arr_u0_userre += [self.u0_userre]
                    } else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.u0_userre.DocCode = item["DocCode"].stringValue
                            self.u0_userre.CreateDateUser = item["CreateDateUser"].stringValue
                            self.u0_userre.TimeCreateJob = item["TimeCreateJob"].stringValue
                            self.u0_userre.EmpName = item["EmpName"].stringValue
                            self.u0_userre.RDeptName = item["RDeptName"].stringValue
                            self.u0_userre.LocName = item["LocName"].stringValue
                            self.u0_userre.detailUser = item["detailUser"].stringValue
                            self.u0_userre.MobileNo = item["MobileNo"].stringValue
                            self.u0_userre.TelETC = item["TelETC"].stringValue
                            self.u0_userre.RemoteName = item["RemoteName"].stringValue
                            self.u0_userre.UserIDRemote = item["UserIDRemote"].stringValue
                            self.u0_userre.PasswordRemote = item["PasswordRemote"].stringValue
                            self.u0_userre.URQIDX = Int(item["URQIDX"].stringValue)!
                            self.u0_userre.unidx = Int(item["unidx"].stringValue)!
                            self.u0_userre.acidx = Int(item["acidx"].stringValue)!
                            self.u0_userre.RDeptIDX = Int(item["RDeptIDX"].stringValue)!
                            self.u0_userre.SysIDX = 3 // IT
                            self.u0_userre.EmpIDX = Int(item["EmpIDX"].stringValue)!
                            self.u0_userre.OrgIDX = Int(item["OrgIDX"].stringValue)!
                            self.u0_userre.StaName = item["StaName"].stringValue
                            
                            self.arr_u0_userre += [self.u0_userre]
                        }
                    }
                    
                    // count data and reload
                    if (self.arr_u0_userre.count > 0) {
                        // set back to label view
                        self.tbvList.backgroundView = nil;
                        
                        self.tbvList.reloadData()
                    }
                    else {
                        self.tbvList.reloadData()
                        
                        // set label size
                        let lblEmpty = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbvList.bounds.size.width, height: self.tbvList.bounds.size.height))
                        // set the message
                        lblEmpty.text = "No data is available";
                        // center the text
                        lblEmpty.textAlignment = .center;
                        lblEmpty.textColor = UIColor.darkGray
                        
                        // set back to label view
                        self.tbvList.backgroundView = lblEmpty;
                        // no separator
                        self.tbvList.separatorStyle = UITableViewCellSeparatorStyle.none;
                    }
                }
        }
    }
    
    // handle refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        self.getAllList()
        // stop refreshing
        self.refreshControl.endRefreshing()
    }
    
    func alertError(return_msg: String) {
        let failAlert = UIAlertController(title: "เกิดข้อผิดพลาด", message: return_msg, preferredStyle: UIAlertControllerStyle.alert); // "ไม่สามารถเชื่อมต่อ Server ได้ค่ะ"
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){(ACTION) in
            // print("OK button tapped")
        }
        
        failAlert.addAction(okAction)
        
        self.present(failAlert, animated: true, completion: nil)
    }
    
    //----- Event Button -----//
    @IBAction func btnDetail(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgListViewControllerSelected = storyBoard.instantiateViewController(withIdentifier: "It_Gg_ListViewControllerSelected")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgListViewControllerSelected

    }
    //----- Event Button -----//
    
    //----- Navigation Bar Button -----//
    @IBAction func btnGoToHome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let centralTabBarController = storyBoard.instantiateViewController(withIdentifier: "CentralTabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = centralTabBarController
        centralTabBarController.selectedIndex = 0 // CentralTabBarController
    }
    
    @IBAction func btnGoToCreate(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let itGgTabBarController = storyBoard.instantiateViewController(withIdentifier: "It_Gg_TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = itGgTabBarController
        itGgTabBarController.selectedIndex = 1 // It_Gg_CreateViewController
    }
    //----- Navigation Bar Button -----//
}
