//
//  It_Bp_ListViewControllerAdmin.swift
//  TKNServices
//
//  Created by lifetofree on 7/11/18.
//  Copyright © 2018 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class It_Bp_ListViewControllerAdmin: UIViewController {
    
    @IBOutlet weak var lblDateReciveJobFirst: UILabel!
    @IBOutlet weak var lblLv1: UILabel!
    @IBOutlet weak var lblLv2: UILabel!
    @IBOutlet weak var lblLv3: UILabel!
    @IBOutlet weak var lblLv4: UILabel!
    @IBOutlet weak var lblLv5: UILabel!
    @IBOutlet weak var Priority_name: UILabel!
    @IBOutlet weak var ManHours: UILabel!
    @IBOutlet weak var lblsumtime: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblSapmsg: UILabel!
    @IBOutlet weak var lblDateCloseJob: UILabel!
    @IBOutlet weak var lblStaName: UILabel!
    @IBOutlet weak var lblAdminName: UILabel!
    @IBOutlet weak var lblAdminDoingName: UILabel!
    @IBOutlet weak var lblCommentAMDoing: UILabel!
    
    var u0_userre = UserRequest_detail()
    var arr_u0_userre = [UserRequest_detail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //----- Reuse -----//
    func initView() {
        getAllList()
    }
    
    // getAllList
    func getAllList() {
        let value =
            [
                "DataSupportIT" : [
                    "BoxUserRequest" : [
                        "FileUser" : 0,
                        "URQIDX" : URQIDX
                    ]
                ]
        ]
        
        let dataIn: String = convertDataToSendForm(value as [String : AnyObject])
        
        let tempUrl = String(urlSelect_DetailCloseJobList + dataIn)
        let sendUrl = URL(string: tempUrl!)!
        
        //print(sendUrl)
        // call web service
        Alamofire.request(
            sendUrl,
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers: nil)
            .responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    // get return_code
                    //return_code = Int(swiftyJsonVar["data_vacation"]["return_code"].stringValue)!
                    //return_msg = swiftyJsonVar["data_vacation"]["return_msg"].stringValue
                    
                    if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryObject != nil {
                        for (key, item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].dictionaryValue {
                            if(key == "DateReciveJobFirst") {
                                self.u0_userre.DateReciveJobFirst = item.stringValue
                            }
                            if(key == "TimeReciveJobFirst") {
                                self.u0_userre.TimeReciveJobFirst = item.stringValue
                            }
                            if(key == "Name1") {
                                self.u0_userre.Name1 = item.stringValue
                            }
                            if(key == "Name2") {
                                self.u0_userre.Name2 = item.stringValue
                            }
                            if(key == "Name3") {
                                self.u0_userre.Name3 = item.stringValue
                            }
                            if(key == "Name4") {
                                self.u0_userre.Name4 = item.stringValue
                            }
                            if(key == "Name5") {
                                self.u0_userre.Name5 = item.stringValue
                            }
                            if(key == "sumtime") {
                                self.u0_userre.sumtime = item.stringValue
                            }
                            
                            if(key == "StaName") {
                                self.u0_userre.StaName = item.stringValue
                            }
                            if(key == "Link_IT") {
                                self.u0_userre.Link_IT = item.stringValue
                            }
                            if(key == "AdminName") {
                                self.u0_userre.AdminName = item.stringValue
                            }
                            if(key == "AdminDoingName") {
                                self.u0_userre.AdminDoingName = item.stringValue
                            }
                            if(key == "CommentAMDoing") {
                                self.u0_userre.CommentAMDoing = item.stringValue
                            }
                            if(key == "DatecloseJob") {
                                self.u0_userre.DatecloseJob = item.stringValue
                            }
                            if(key == "TimecloseJob") {
                                self.u0_userre.TimecloseJob = item.stringValue
                            }
                            if(key == "Priority_name") {
                                self.u0_userre.Priority_name = item.stringValue
                            }
                            if(key == "ManHours") {
                                self.u0_userre.ManHours = Int(item.stringValue)!
                            }
                            if(key == "SapMsg") {
                                self.u0_userre.SapMsg = item.stringValue
                            }
                        }
                        self.arr_u0_userre += [self.u0_userre]
                    } else if swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayObject != nil {
                        for (item) in swiftyJsonVar["DataSupportIT"]["BoxUserRequest"].arrayValue {
                            self.u0_userre.DateReciveJobFirst = item["DateReciveJobFirst"].stringValue
                            self.u0_userre.TimeReciveJobFirst = item["TimeReciveJobFirst"].stringValue
                            self.u0_userre.Name1 = item["Name1"].stringValue
                            self.u0_userre.Name2 = item["Name2"].stringValue
                            self.u0_userre.Name3 = item["Name3"].stringValue
                            self.u0_userre.Name4 = item["Name4"].stringValue
                            self.u0_userre.Name5 = item["Name5"].stringValue
                            self.u0_userre.sumtime = item["sumtime"].stringValue
                            self.u0_userre.StaName = item["StaName"].stringValue
                            self.u0_userre.Link_IT = item["Link_IT"].stringValue
                            self.u0_userre.AdminName = item["AdminName"].stringValue
                            self.u0_userre.AdminDoingName = item["AdminDoingName"].stringValue
                            self.u0_userre.CommentAMDoing = item["CommentAMDoing"].stringValue
                            self.u0_userre.DatecloseJob = item["DatecloseJob"].stringValue
                            self.u0_userre.TimecloseJob = item["TimecloseJob"].stringValue
                            
                            self.u0_userre.Priority_name = item["Priority_name"].stringValue
                            self.u0_userre.ManHours = Int(item["ManHours"].stringValue)!
                            self.u0_userre.SapMsg = item["SapMsg"].stringValue
                            
                            self.arr_u0_userre += [self.u0_userre]
                        }
                    }
                    
                    self.lblDateReciveJobFirst.text = self.arr_u0_userre[0].DateReciveJobFirst + " " + self.arr_u0_userre[0].TimeReciveJobFirst
                    self.lblLv1.text = self.arr_u0_userre[0].Name1
                    self.lblLv2.text = self.arr_u0_userre[0].Name2
                    self.lblLv3.text = self.arr_u0_userre[0].Name3
                    self.lblLv4.text = self.arr_u0_userre[0].Name4
                    self.lblLv5.text = self.arr_u0_userre[0].Name5
                    self.lblsumtime.text = self.arr_u0_userre[0].sumtime
                    self.lblStaName.text = self.arr_u0_userre[0].StaName
                    self.lblLink.text = self.arr_u0_userre[0].Link_IT
                    self.lblAdminName.text = self.arr_u0_userre[0].AdminName
                    self.lblAdminDoingName.text = self.arr_u0_userre[0].AdminDoingName
                    self.lblCommentAMDoing.text = self.arr_u0_userre[0].CommentAMDoing
                    self.lblDateCloseJob.text = self.arr_u0_userre[0].DatecloseJob + " " + self.arr_u0_userre[0].TimecloseJob
                    
                    self.Priority_name.text = self.arr_u0_userre[0].Priority_name
                    self.ManHours.text = String(self.arr_u0_userre[0].ManHours)
                    self.lblSapmsg.text = self.arr_u0_userre[0].SapMsg
                }
        }
    }
    //----- Reuse -----//
}
