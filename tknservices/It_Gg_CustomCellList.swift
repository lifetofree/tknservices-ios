//
//  It_Gg_CustomCellList.swift
//  tknservices
//
//  Created by lifetofree on 8/7/17.
//  Copyright © 2017 Taokaenoi Food & Marketing Public Company Limited. All rights reserved.
//

import UIKit

class It_Gg_CustomCellList: UITableViewCell {
    
    @IBOutlet weak var lblDocCode: UILabel!
    @IBOutlet weak var lblCreateDateUser: UILabel!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblRDeptName: UILabel!
    @IBOutlet weak var lblLocName: UILabel!
    @IBOutlet weak var lbldetailUser: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var lblNameComment: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblDateComment: UILabel!
    @IBOutlet weak var myImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadItem(DocCode: String, CreateDateUser: String, EmpName: String, RDeptName: String, LocName: String, detailUser: String, Status: String, ImageStatus: String) {
        
        lblDocCode.text = DocCode
        lblCreateDateUser.text = CreateDateUser
        lblEmpName.text = EmpName
        lblRDeptName.text = RDeptName
        lblLocName.text = LocName
        lbldetailUser.text = detailUser
        lblStatus.text = Status
        
        switch ImageStatus {
        case "สมบูรณ์":
            myImage.image = UIImage(named: "icon_status_completed")
        case "ยังไม่รับงาน":
            myImage.image = UIImage(named: "icon_status_warning")
        case "กำลังดำเนินการ", "กำลังตรวจสอบ", "กำลังรอผู้แจ้งทำการตรวจสอบ", "รอ User Comment ตอบกลับ":
            myImage.image = UIImage(named: "icon_status_waiting")
        case "ส่งเครม", "รออะไหล่", "ติดต่อซัพภายนอก":
            myImage.image = UIImage(named: "icon_status_repair")
        default:
            myImage.image = UIImage(named: "icon_status_completed")
        }
    }
    
    func loadItem_Comment(FullNameTH: String, CommentAuto: String, CDate: String) {
        
        lblNameComment.text = FullNameTH
        lblComment.text = CommentAuto
        lblDateComment.text = CDate
    }
    
}
